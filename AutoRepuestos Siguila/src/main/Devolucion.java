/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import clases.TextPrompt;
import clases.Conexion_postgres;
import clases.Activacion;
import clases.Conexion_mysql;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;

/**
 *
 * @author Leo
 */
public class Devolucion extends javax.swing.JFrame {
    int devolucion1;
      
    int cantTabla,Cant_Prod_Tot,Cant_Estatico;
    public int id_prod,id_vent,cant;
    public float precio,descuento,disminucion=0,total_venta,desc_en;
    float tot = 0; //variable para almacenar operacion para calcular el descuento
    //verificacion de pagos para asi eliminarlos
    int pag;
     DefaultTableModel modelo2=new DefaultTableModel();
      String product, descrip,marca;
    public Devolucion() {
        initComponents();
                //Primera tabla
tabladatos.setOpaque(false);
jScrollPane3.setOpaque(false);
jScrollPane3.getViewport().setOpaque(false);


        loading.setVisible(false);
        loading.setVisible(false);
        this.setLocationRelativeTo(null);

         TextPrompt q=new TextPrompt("Ingrese No de Venta",id_venta1);
            modelo2.addColumn("Id Detalle Venta");
            modelo2.addColumn("Id Producto");
             modelo2.addColumn("Producto");
            modelo2.addColumn("Descripción");
            modelo2.addColumn("Marca"); 
             modelo2.addColumn("Precio"); 
             modelo2.addColumn("Cantidad");
            tabladatos.setModel(modelo2);
            //dis.setText(disminucion);
  //Activacion
      r_pr.setEnabled(false);
        r_pro.setEnabled(false);
        r_em.setEnabled(false);
        r_fa.setEnabled(false);
        r_cli.setEnabled(false);
        b_pro.setEnabled(false);
        b_emp.setEnabled(false);
        b_fac.setEnabled(false);
        b_cli.setEnabled(false);
      ven.setEnabled(false);
        venE.setEnabled(false);
   rep.setEnabled(false);
      dev.setEnabled(false);
        m_pro.setEnabled(false);
  m_emp.setEnabled(false);
    m_cli.setEnabled(false);
       m_prov.setEnabled(false);
        b_ventas.setEnabled(false);
       r_pago.setEnabled(false);
        Activacion b=new Activacion();
        if(b.ver==1){
            b.entrante(r_pr,r_pro,r_em,r_fa,r_cli,b_pro,b_emp,b_fac,b_cli,modificacion,devolucion,ven,ea,dev,m_pro,
                    m_emp,m_cli,m_prov,rep,venE, b_ventas,r_pago);
        }
           if(b.ver==2){
            b.en(ven,venE,r_fa,b_pro,r_pr,r_pro,r_cli,r_pago,dev,b_cli,b_fac);
        }
                mostrar_detalle_venta();
    }
    
   
    public void borrar(DefaultTableModel tabla){
     while(tabla.getRowCount()>0){
     tabla.removeRow(0);
     }
        }
    //Procedimiento
     public void Escribir_En_Bitacora(){
       Conexion_mysql con;
  con=new Conexion_mysql(loading);
  java.sql.Connection cn=con.getConnection();
    String sql="call PRC_Registro_Bitacora(?,?,?,?,?,?,?)";
    try {
    
        CallableStatement cs=cn.prepareCall(sql);
        cs.setInt(1,Integer.parseInt(id_venta1.getText()));
        cs.setInt(2,id_prod);
        cs.setString(3,product);
        cs.setString(4,descrip);
        cs.setString(5,marca);
        cs.setFloat(6,precio);
        cs.setInt(7,cant);
                 if(cs.execute()){
           JOptionPane.showMessageDialog(null, "Registro correcto");
       }
                 cs.close();
    } catch (Exception ex) {
     Logger.getLogger(R_PROD.class.getName()).log(Level.SEVERE, null, ex);
    }
}



    
      public void mostrar_detalle_venta(){
      borrar(modelo2);
        
            String[] datos=new String[9];
            
           Conexion_mysql con;
  con=new Conexion_mysql(loading);
  java.sql.Connection cn=con.getConnection();
  
    String readMetaData = "CALL  PRC_detalle_venta_devolver(?) ";
     try {
         cn.setAutoCommit(false); // This line must be written just after the 

         CallableStatement cs= cn.prepareCall(readMetaData);
        cs.setInt(1,Integer.parseInt(id_venta1.getText()));
      
         //cs.registerOutParameter(2, Types.REF_CURSOR);
//cs.execute();
         ResultSet rs = (ResultSet) cs.executeQuery();
         
if (rs != null) {
    while (rs.next()) {       
     datos[0]=rs.getString(1).toUpperCase();
      datos[1]=rs.getString(2).toUpperCase();
      datos[2]=rs.getString(3).toUpperCase();
      datos[3]=rs.getString(4).toUpperCase();
      datos[4]=rs.getString(5).toUpperCase();
      datos[5]=rs.getString(6);
       datos[6]=rs.getString(7);
       Cant_Prod_Tot= Cant_Prod_Tot+rs.getInt(7);
         descuento=rs.getFloat(8);
         total_venta=rs.getFloat(9);
       modelo2.addRow(datos);
                }//end while
            }//end if
System.out.println(descuento);
cs.close();
rs.close();
     } catch (SQLException ex) {
        
     }
     to1.setText(Float.toString(total_venta-descuento));
     
    
}
      ///
       public void Ver_pagos(){

          int id=Integer.parseInt(id_venta1.getText());
          
           Conexion_mysql con;
   con=new Conexion_mysql(loading);
  java.sql.Connection cn=con.getConnection();
  
    String readMetaData = "CALL  PRC_Ver_pag(?) ";
     try {
         cn.setAutoCommit(false); // This line must be written just after the 

         CallableStatement cs= cn.prepareCall(readMetaData);
         cs.setInt(1,id);
      
         //cs.registerOutParameter(2, Types.REF_CURSOR);
//cs.execute();
ResultSet rs = (ResultSet) cs.executeQuery();
if (rs != null) {
    while (rs.next()) {       
      pag=rs.getInt(1);
                }//end while
            }//end if
cs.close();
rs.close();
     } catch (SQLException ex) {
        
     }
  
    
}
      
      ///
      
      public void decuento_producto(float can){
          
          if(total_venta==precio){//si total de Venta no es igual que el precio unitario entonces solo hace el descuetno existente
       
              tot=can-descuento;
             disminucion=disminucion+tot;
              System.out.print(" Tot4= "+tot);
                 dis.setText(Float.toString(disminucion));
                 to1.setText("0");
             }else{
                                  System.out.print(" Total V= "+total_venta+" descuento= "+descuento);
           tot=total_venta/descuento;
             System.out.print(" Tot1= "+tot);
          tot=100/tot;
                    System.out.print(" Tot2= "+tot);
           tot=tot/100;   System.out.print(" Tot3= "+tot);
          tot=tot*can; 
                              System.out.print(" Multiplicado * "+can);

             desc_en=tot;
                    System.out.print(" Descuento_Final= "+desc_en);
              tot=can-tot;
              disminucion=disminucion+tot;
              System.out.print(" Tot4= "+tot);
                 dis.setText(Float.toString(disminucion));
             }
  
      }
      
      

      
    public void Eliminar_Producto_DE_Detalle(){
    try {
           Conexion_mysql con;
  con=new Conexion_mysql(loading);
  java.sql.Connection cn=con.getConnection();
        String sql="call PRC_eliminar_Productos_En_Detalle_V(?,?)";

        CallableStatement cs=cn.prepareCall(sql);
        cs.setInt(1,Integer.parseInt(id_venta1.getText()));
        cs.setInt(2,id_prod);
                 if(cs.execute()){
           JOptionPane.showMessageDialog(null, "Devolucion Correcta");
       }
                 cs.close();
                
    } catch (Exception ex) {
     Logger.getLogger(R_PROD.class.getName()).log(Level.SEVERE, null, ex);
    }
}
    //Eliminacion de las ventas
     
    public void Eliminar_Ventas(){
    try {
            Conexion_mysql con;
  con=new Conexion_mysql(loading);
  java.sql.Connection cn=con.getConnection();
        String sql="call PRC_eliminar_venta(?)";

        CallableStatement cs=cn.prepareCall(sql);
        cs.setInt(1,Integer.parseInt(id_venta1.getText()));
                 if(cs.execute()){
           JOptionPane.showMessageDialog(null, "Devolucion Correcta");
       }
                 cs.close();
    } catch (Exception ex) {
     Logger.getLogger(R_PROD.class.getName()).log(Level.SEVERE, null, ex);
    }
}
        public void Eliminar_Pago(){
    try {
         Conexion_mysql con;
  con=new Conexion_mysql(loading);
  java.sql.Connection cn=con.getConnection();
        String sql="call PRC_eliminar_pagos(?)";

        CallableStatement cs=cn.prepareCall(sql);
        cs.setInt(1,Integer.parseInt(id_venta1.getText()));
                 if(cs.execute()){
           JOptionPane.showMessageDialog(null, "Devolucion Correcta");
       }
                 cs.close();
    } catch (Exception ex) {
     Logger.getLogger(R_PROD.class.getName()).log(Level.SEVERE, null, ex);
    }
}
        public void Eliminar_detalle(){
    try {
           Conexion_mysql con;
  con=new Conexion_mysql(loading);
  java.sql.Connection cn=con.getConnection();
        String sql="call PRC_eliminar_detalle(?)";

        CallableStatement cs=cn.prepareCall(sql);
        cs.setInt(1,Integer.parseInt(id_venta1.getText()));
                 if(cs.execute()){
           JOptionPane.showMessageDialog(null, "Devolucion Correcta");
       }
                 cs.close();
    } catch (Exception ex) {
     Logger.getLogger(R_PROD.class.getName()).log(Level.SEVERE, null, ex);
    }
}
    
   
    
       public void Disminuir_Monto_Vent(int a){
       Conexion_mysql con;
  con=new Conexion_mysql(loading);
  java.sql.Connection cn=con.getConnection();
    String sql="call PRC_Disminuir_Valor_Venta(?,?,?,?)";
    try {
    
        CallableStatement cs=cn.prepareCall(sql);
        cs.setInt(1,Integer.parseInt(id_venta1.getText()));
       // cs.setInt(2,id_prod);
        cs.setInt(2,a);
        cs.setFloat(3,precio);
            cs.setFloat(4,desc_en);
                 if(cs.execute()){
           JOptionPane.showMessageDialog(null, "Devolucion Correcta");
       }
                   JOptionPane.showMessageDialog(null, "<html><h1>ENTRE<h1></html>");
                 cs.close();
    } catch (Exception ex) {
     Logger.getLogger(R_PROD.class.getName()).log(Level.SEVERE, null, ex);
    }
}
        
    
    
        
    public void Manda_Parametros_para_Aumentar_exis(int cantEntrante){
      Conexion_mysql con;
  con=new Conexion_mysql(loading);
  java.sql.Connection cn=con.getConnection();
    String sql="call PRC_Aument_exis_Despues_dev(?,?)";
    try {
    
        CallableStatement cs=cn.prepareCall(sql);
        cs.setInt(1,id_prod);
        cs.setInt(2, cantEntrante);
                 if(cs.execute()){
           JOptionPane.showMessageDialog(null, "Devolucion Correcta");
       }
                 cs.close();
    } catch (Exception ex) {
     Logger.getLogger(R_PROD.class.getName()).log(Level.SEVERE, null, ex);
    }
}
    
       
    
    
     public void Actualizar_Producto_DE_Detalle(int cantt){
      Conexion_mysql con;
  con=new Conexion_mysql(loading);
  java.sql.Connection cn=con.getConnection();
    String sql="call PRC_Actualizar_Productos_En_Detalle_V(?,?,?)";
    try {
    
        CallableStatement cs=cn.prepareCall(sql);
        cs.setInt(1,Integer.parseInt(id_venta1.getText()));
        cs.setInt(2,id_prod);
        cs.setInt(3,cantt);
                 if(cs.execute()){
           JOptionPane.showMessageDialog(null, "");
       }
                 cs.close();
    } catch (Exception ex) {
     Logger.getLogger(R_PROD.class.getName()).log(Level.SEVERE, null, ex);
    }
}
             public void Consulta_Imagen(){
     //Variables para la imagen
InputStream is;  
      ImageIcon foto;
      
           Conexion_mysql con;
  con=new Conexion_mysql(loading);
  java.sql.Connection cn=con.getConnection();
  
    String readMetaData = "CALL PRC_Con_Imag_Re_Fac(?) ";
     try {
         cn.setAutoCommit(false); // This line must be written just after the 

         CallableStatement cs= cn.prepareCall(readMetaData);
         cs.setInt(1,id_prod);
  
         //cs.registerOutParameter(2, Types.REF_CURSOR);
//cs.execute();
ResultSet rs = (ResultSet) cs.executeQuery();
if (rs != null) {
    while (rs.next()) {       
    
//PROCESO PARA CONSULTAR IMAGEN
 is=rs.getBinaryStream(1);
 try {
            BufferedImage bi=ImageIO.read(is);
            foto=new ImageIcon(bi);
            Image img=foto.getImage();
            Image newimg=img.getScaledInstance(190,190, 
java.awt.Image.SCALE_SMOOTH);
            ImageIcon newicon=new ImageIcon(newimg);
  rec_img.setIcon(newicon);//enviarlo a un jlabel    
 } catch (IOException ex) {
            Logger.getLogger(R_FACTURA.class.getName()).log
(Level.SEVERE, null, ex);
        }
     
//TERMINACION DE PROCESO PARA CONSULTAR IMAGEN

                }//end while
            }//end if
     } catch (SQLException ex) {
         Logger.getLogger(R_FACTURA.class.getName()).log(Level.SEVERE, null, ex);
     }

 }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPopupMenu1 = new javax.swing.JPopupMenu();
        jMenuItem1 = new javax.swing.JMenuItem();
        jMenuItem2 = new javax.swing.JMenuItem();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane3 = new javax.swing.JScrollPane();
        tabladatos = new javax.swing.JTable();
        id_venta1 = new javax.swing.JTextField();
        rec_img = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        to1 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jButton7 = new javax.swing.JButton();
        jLabel5 = new javax.swing.JLabel();
        loading = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        dis = new javax.swing.JLabel();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jMenu3 = new javax.swing.JMenu();
        jMenu2 = new javax.swing.JMenu();
        jMenu4 = new javax.swing.JMenu();
        r_pr = new javax.swing.JMenuItem();
        r_em = new javax.swing.JMenuItem();
        r_fa = new javax.swing.JMenuItem();
        r_pro = new javax.swing.JMenuItem();
        r_cli = new javax.swing.JMenuItem();
        jMenu6 = new javax.swing.JMenu();
        b_pro = new javax.swing.JMenuItem();
        b_emp = new javax.swing.JMenuItem();
        b_fac = new javax.swing.JMenuItem();
        b_cli = new javax.swing.JMenuItem();
        b_ventas = new javax.swing.JMenuItem();
        modificacion = new javax.swing.JMenu();
        m_pro = new javax.swing.JMenuItem();
        m_emp = new javax.swing.JMenuItem();
        m_cli = new javax.swing.JMenuItem();
        m_prov = new javax.swing.JMenuItem();
        devolucion = new javax.swing.JMenu();
        dev = new javax.swing.JMenuItem();
        jMenu7 = new javax.swing.JMenu();
        ven = new javax.swing.JMenuItem();
        venE = new javax.swing.JMenuItem();
        ea = new javax.swing.JMenu();
        rep = new javax.swing.JMenuItem();
        jMenu5 = new javax.swing.JMenu();
        r_pago = new javax.swing.JMenuItem();

        jMenuItem1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/add (1).png"))); // NOI18N
        jMenuItem1.setText("Devolucion Normal");
        jMenuItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem1ActionPerformed(evt);
            }
        });
        jPopupMenu1.add(jMenuItem1);

        jMenuItem2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Bote de basura.png"))); // NOI18N
        jMenuItem2.setText("Devolucion Defectuosa");
        jMenuItem2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem2ActionPerformed(evt);
            }
        });
        jPopupMenu1.add(jMenuItem2);

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setUndecorated(true);

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));
        jPanel2.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        tabladatos.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        tabladatos.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Id Producto", "Nombre Producto", "Fecha"
            }
        ));
        tabladatos.setComponentPopupMenu(jPopupMenu1);
        tabladatos.setSelectionBackground(new java.awt.Color(51, 204, 255));
        tabladatos.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                tabladatosMousePressed(evt);
            }
        });
        jScrollPane3.setViewportView(tabladatos);

        jPanel2.add(jScrollPane3, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 280, 1010, 190));

        id_venta1.setText("0");
        id_venta1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                id_venta1ActionPerformed(evt);
            }
        });
        id_venta1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                id_venta1KeyReleased(evt);
            }
        });
        jPanel2.add(id_venta1, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 240, 340, 30));

        rec_img.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanel2.add(rec_img, new org.netbeans.lib.awtextra.AbsoluteConstraints(1020, 280, 190, 190));

        jLabel15.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        jLabel15.setText("Producto");
        jPanel2.add(jLabel15, new org.netbeans.lib.awtextra.AbsoluteConstraints(900, 180, 110, 20));

        jLabel3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/NUEVA LUPA.png"))); // NOI18N
        jPanel2.add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 240, 280, -1));

        jLabel1.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        jLabel1.setText("Total Venta");
        jPanel2.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(900, 70, 120, 30));

        jLabel4.setFont(new java.awt.Font("Times New Roman", 1, 36)); // NOI18N
        jLabel4.setText("DEVOLUCION");
        jPanel2.add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 30, 270, 40));

        jSeparator1.setForeground(new java.awt.Color(255, 0, 51));
        jSeparator1.setFont(new java.awt.Font("Tahoma", 3, 11)); // NOI18N
        jSeparator1.setMaximumSize(new java.awt.Dimension(42767, 32767));
        jPanel2.add(jSeparator1, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 80, 200, 10));

        to1.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        to1.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel2.add(to1, new org.netbeans.lib.awtextra.AbsoluteConstraints(890, 100, 120, 30));

        jLabel6.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        jLabel6.setText("No.Venta");
        jPanel2.add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 210, 350, 30));

        jButton7.setFont(new java.awt.Font("Tahoma", 3, 24)); // NOI18N
        jButton7.setForeground(new java.awt.Color(255, 255, 255));
        jButton7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/salir.png"))); // NOI18N
        jButton7.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jButton7.setIconTextGap(1);
        jButton7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton7ActionPerformed(evt);
            }
        });
        jPanel2.add(jButton7, new org.netbeans.lib.awtextra.AbsoluteConstraints(1030, 10, 50, 40));

        jLabel5.setBackground(new java.awt.Color(255, 255, 255));
        jLabel5.setFont(new java.awt.Font("Tahoma", 3, 14)); // NOI18N
        jLabel5.setText("SALIR");
        jPanel2.add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(1030, 50, -1, -1));

        loading.setIcon(new javax.swing.ImageIcon(getClass().getResource("/main/loading_1.gif"))); // NOI18N
        jPanel2.add(loading, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 60, 340, 100));

        jButton1.setText("Buscar");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        jPanel2.add(jButton1, new org.netbeans.lib.awtextra.AbsoluteConstraints(400, 240, 330, 30));

        jLabel2.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        jLabel2.setText("Cantidad Devolver:");
        jPanel2.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(570, 70, 180, 29));

        dis.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        dis.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel2.add(dis, new org.netbeans.lib.awtextra.AbsoluteConstraints(570, 100, 170, 30));

        jMenuBar1.setBorder(null);
        jMenuBar1.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jMenuBar1.setFont(new java.awt.Font("Segoe UI", 2, 12)); // NOI18N

        jMenu1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/can.png"))); // NOI18N
        jMenu1.setText("LOGIN");

        jMenu3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Fondo.png"))); // NOI18N
        jMenu3.setText("LOGIN");
        jMenu3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jMenu3MouseClicked(evt);
            }
        });
        jMenu1.add(jMenu3);

        jMenuBar1.add(jMenu1);
        jMenuBar1.add(jMenu2);

        jMenu4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/add (1).png"))); // NOI18N
        jMenu4.setText(" REGISTRO");

        r_pr.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_R, java.awt.event.InputEvent.CTRL_DOWN_MASK));
        r_pr.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/ingreso.png"))); // NOI18N
        r_pr.setText("PRODUCTO");
        r_pr.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                r_prActionPerformed(evt);
            }
        });
        jMenu4.add(r_pr);

        r_em.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_E, java.awt.event.InputEvent.CTRL_DOWN_MASK));
        r_em.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/emple.png"))); // NOI18N
        r_em.setText("EMPLEADOS");
        r_em.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                r_emActionPerformed(evt);
            }
        });
        jMenu4.add(r_em);

        r_fa.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F, java.awt.event.InputEvent.CTRL_DOWN_MASK));
        r_fa.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/REGISTRO FACTURA.png"))); // NOI18N
        r_fa.setText("FACTURA");
        r_fa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                r_faActionPerformed(evt);
            }
        });
        jMenu4.add(r_fa);

        r_pro.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_P, java.awt.event.InputEvent.CTRL_DOWN_MASK));
        r_pro.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/proveNue.png"))); // NOI18N
        r_pro.setText("PROVEEDOR");
        r_pro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                r_proActionPerformed(evt);
            }
        });
        jMenu4.add(r_pro);

        r_cli.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_C, java.awt.event.InputEvent.CTRL_DOWN_MASK));
        r_cli.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/cliente.png"))); // NOI18N
        r_cli.setText("CLIENTE");
        r_cli.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                r_cliActionPerformed(evt);
            }
        });
        jMenu4.add(r_cli);

        jMenuBar1.add(jMenu4);

        jMenu6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Busqueda (1).png"))); // NOI18N
        jMenu6.setText("BUSQUEDA");

        b_pro.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_P, java.awt.event.InputEvent.ALT_DOWN_MASK));
        b_pro.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/prodddd.png"))); // NOI18N
        b_pro.setText("PRODUCTOS");
        b_pro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                b_proActionPerformed(evt);
            }
        });
        jMenu6.add(b_pro);

        b_emp.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_E, java.awt.event.InputEvent.ALT_DOWN_MASK));
        b_emp.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/busPer .png"))); // NOI18N
        b_emp.setText("EMPLEADOS");
        b_emp.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                b_empActionPerformed(evt);
            }
        });
        jMenu6.add(b_emp);

        b_fac.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F, java.awt.event.InputEvent.ALT_DOWN_MASK));
        b_fac.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/busqueda_factura.png"))); // NOI18N
        b_fac.setText("FACTURA");
        b_fac.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                b_facActionPerformed(evt);
            }
        });
        jMenu6.add(b_fac);

        b_cli.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_C, java.awt.event.InputEvent.ALT_DOWN_MASK));
        b_cli.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/bus_em.png"))); // NOI18N
        b_cli.setText("CLIENTE");
        b_cli.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                b_cliActionPerformed(evt);
            }
        });
        jMenu6.add(b_cli);

        b_ventas.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_Z, java.awt.event.InputEvent.ALT_DOWN_MASK));
        b_ventas.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Busqueda1.png"))); // NOI18N
        b_ventas.setText("VENTAS");
        b_ventas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                b_ventasActionPerformed(evt);
            }
        });
        jMenu6.add(b_ventas);

        jMenuBar1.add(jMenu6);

        modificacion.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/modificacion.png"))); // NOI18N
        modificacion.setText("MODIFICAR");

        m_pro.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_K, java.awt.event.InputEvent.ALT_DOWN_MASK));
        m_pro.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/PRODUCTO.png"))); // NOI18N
        m_pro.setText("PRODUCTO");
        m_pro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                m_proActionPerformed(evt);
            }
        });
        modificacion.add(m_pro);

        m_emp.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_E, java.awt.event.InputEvent.SHIFT_DOWN_MASK));
        m_emp.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/empleado.png"))); // NOI18N
        m_emp.setText("EMPLEADO");
        m_emp.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                m_empActionPerformed(evt);
            }
        });
        modificacion.add(m_emp);

        m_cli.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_C, java.awt.event.InputEvent.SHIFT_DOWN_MASK));
        m_cli.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/clientee.png"))); // NOI18N
        m_cli.setText("CLIENTE");
        m_cli.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                m_cliActionPerformed(evt);
            }
        });
        modificacion.add(m_cli);

        m_prov.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_P, java.awt.event.InputEvent.SHIFT_DOWN_MASK));
        m_prov.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/proveedores.png"))); // NOI18N
        m_prov.setText("PROVEEDORES");
        m_prov.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                m_provActionPerformed(evt);
            }
        });
        modificacion.add(m_prov);

        jMenuBar1.add(modificacion);

        devolucion.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/DEV.png"))); // NOI18N
        devolucion.setText("DEVOULUCION");

        dev.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_D, java.awt.event.InputEvent.CTRL_DOWN_MASK));
        dev.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/dev1.png"))); // NOI18N
        dev.setText("DEVOLUCION");
        dev.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                devActionPerformed(evt);
            }
        });
        devolucion.add(dev);

        jMenuBar1.add(devolucion);

        jMenu7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/png-transparent-shopping-cart-graphy-cart-supermarket-vehicle-shopping-bags-trolleys (1) (1).png"))); // NOI18N
        jMenu7.setText("VENTA");

        ven.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_V, java.awt.event.InputEvent.CTRL_DOWN_MASK));
        ven.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/venta a credito.png"))); // NOI18N
        ven.setText("VENTA  A CREDITO");
        ven.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                venActionPerformed(evt);
            }
        });
        jMenu7.add(ven);

        venE.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_B, java.awt.event.InputEvent.CTRL_DOWN_MASK));
        venE.setIcon(new javax.swing.ImageIcon(getClass().getResource("/main/venta efectivo.png"))); // NOI18N
        venE.setText("VENTA EN EFECTIVO");
        venE.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                venEActionPerformed(evt);
            }
        });
        jMenu7.add(venE);

        jMenuBar1.add(jMenu7);

        ea.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/REPORTE.png"))); // NOI18N
        ea.setText("REPORTE");

        rep.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_R, java.awt.event.InputEvent.ALT_DOWN_MASK));
        rep.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/reportess.png"))); // NOI18N
        rep.setText(" REPORTES");
        rep.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                repActionPerformed(evt);
            }
        });
        ea.add(rep);

        jMenuBar1.add(ea);

        jMenu5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Pago_C_P.png"))); // NOI18N
        jMenu5.setText(" PAGOS");

        r_pago.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Pago_C_P (2).png"))); // NOI18N
        r_pago.setText("REALIZAR PAGOS");
        r_pago.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                r_pagoActionPerformed(evt);
            }
        });
        jMenu5.add(r_pago);

        jMenuBar1.add(jMenu5);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, 556, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jMenuItem1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem1ActionPerformed
String a=JOptionPane.showInputDialog(null,"Ingrese la cantidad a devolver");
 int cuentaFilasSeleccionadas = tabladatos.getSelectedRowCount();
     if(cuentaFilasSeleccionadas ==0){
            JOptionPane.showMessageDialog(null,"No hay filas seleccinada");
  }else{
if(a==null){
    System.out.println("Operacion Cancelada");
}else{
    if(a.equals("")){
            JOptionPane.showMessageDialog(null, "NO HA INGRESADO LA CANTIDAD A DEVOLVER","ERROR",JOptionPane.ERROR_MESSAGE);

    }else{
    if(Integer.parseInt(a)>cant||Integer.parseInt(a)<1){
        JOptionPane.showMessageDialog(null, "CANTIDAD MAYOR O MENOR QUE CANTIDAD COMPRADA COMPRO","ERROR",JOptionPane.ERROR_MESSAGE);
    }else{
        
          if(Integer.parseInt(a)==cant && tabladatos.getRowCount()>=1){
        Eliminar_Producto_DE_Detalle();       //pupup DEVOLUCION NORMAL
       Manda_Parametros_para_Aumentar_exis(Integer.parseInt(a));
   decuento_producto(Integer.parseInt(a)*precio);//Operacion para calcular el descuento para aplicar a cada venta 
             Disminuir_Monto_Vent(Integer.parseInt(a));
borrar(modelo2);          
        }
          if(cant>1 && tabladatos.getRowCount()>=1 && Integer.parseInt(a)<cant){
 Actualizar_Producto_DE_Detalle(Integer.parseInt(a));
        Manda_Parametros_para_Aumentar_exis(Integer.parseInt(a));
        decuento_producto(Integer.parseInt(a)*precio);//Operacion para calcular el descuento para aplicar a cada venta 
             Disminuir_Monto_Vent(Integer.parseInt(a));
              mostrar_detalle_venta();
       
}
          if(tabladatos.getRowCount()==1 && Integer.parseInt(a)==cant){
     
               Actualizar_Producto_DE_Detalle(Integer.parseInt(a));
                Ver_pagos();

                decuento_producto(Integer.parseInt(a)*precio);//Funcion para hacer calculo del nuevo dinero a devolver y para disminucion de descuento en venta
         Disminuir_Monto_Vent(Integer.parseInt(a));

               if(pag>0){//elimino pagos primero si la consulta es existente depues elimino venta por su enlace
                   Eliminar_Pago();
                     Eliminar_detalle();
                    Eliminar_Ventas();
                        to1.setText("0");
              }else{
           Eliminar_Ventas();
            to1.setText("0");
               }
          }
          
          
    }
    }
     
}
}
 try {
            
               Cant_Estatico=+Cant_Estatico+Integer.parseInt(a);
  
        } catch (Exception e) {
        }

    }//GEN-LAST:event_jMenuItem1ActionPerformed

    private void id_venta1KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_id_venta1KeyReleased
  
    }//GEN-LAST:event_id_venta1KeyReleased

    private void jMenuItem2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem2ActionPerformed
        String a=JOptionPane.showInputDialog(null,"Ingrese la cantidad a devolver");
      
 int cuentaFilasSeleccionadas = tabladatos.getSelectedRowCount();
     if(cuentaFilasSeleccionadas ==0){
            JOptionPane.showMessageDialog(null,"No hay filas seleccinada");
  }else{
if(a==null){
    System.out.println("Operacion Cancelada");
}else{
    if(a.equals("")){
            JOptionPane.showMessageDialog(null, "NO HA INGRESADO LA CANTIDAD A DEVOLVER","ERROR",JOptionPane.ERROR_MESSAGE);

    }else{
    if(Integer.parseInt(a)>cant||Integer.parseInt(a)<1){
        JOptionPane.showMessageDialog(null, "CANTIDAD MAYOR O MENOR QUE CANTIDAD COMPRADA COMPRO","ERROR",JOptionPane.ERROR_MESSAGE);
 
    }else{
        if(Integer.parseInt(a)==cant && tabladatos.getRowCount()>=1){
            Escribir_En_Bitacora();
            
    Eliminar_Producto_DE_Detalle();
            decuento_producto(Integer.parseInt(a)*precio);//Funcion para hacer calculo del nuevo dinero a devolver y para disminucion de descuento en venta
         Disminuir_Monto_Vent(Integer.parseInt(a));
              borrar(modelo2);

} if(cant>1 && tabladatos.getRowCount()>=1 && Integer.parseInt(a)<cant){
     Escribir_En_Bitacora();
Actualizar_Producto_DE_Detalle(Integer.parseInt(a));
      decuento_producto(Integer.parseInt(a)*precio);//Funcion para hacer calculo del nuevo dinero a devolver y para disminucion de descuento en venta
         Disminuir_Monto_Vent(Integer.parseInt(a));
  borrar(modelo2);
   mostrar_detalle_venta();
}if(tabladatos.getRowCount()==1 && Integer.parseInt(a)==cant){
    Escribir_En_Bitacora();
       Ver_pagos();
        JOptionPane.showMessageDialog(null,"Eliminacion por completo1");
   if(pag>0){//elimino pagos primero si la consulta es existente depues elimino venta por su enlace
                   Eliminar_Pago();
                     Eliminar_Producto_DE_Detalle();
                   Eliminar_detalle();
                    Eliminar_Ventas();
                           to1.setText("0");
              }else{
           JOptionPane.showMessageDialog(null,"Eliminacion por completo2");
            Eliminar_Producto_DE_Detalle(); 
            Eliminar_detalle();
           Eliminar_Ventas();
           
                   to1.setText("0");
               }
      
            borrar(modelo2);

}
    }
    }                        
    decuento_producto(Integer.parseInt(a)*precio);//Funcion para hacer calculo del nuevo dinero a devolver y para disminucion de descuento en venta
         Disminuir_Monto_Vent(Integer.parseInt(a));
         
    }
}
      try {
       Cant_Estatico=+Cant_Estatico+Integer.parseInt(a);
        
     
        } catch (Exception e) {
        }
            
    }//GEN-LAST:event_jMenuItem2ActionPerformed

    private void tabladatosMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tabladatosMousePressed
         Thread thread = new Thread() {
            public void run() {

        int seleccion=tabladatos.rowAtPoint(evt.getPoint());
    //Extraxion de datos en la tabla      
         id_prod=Integer.parseInt(String.valueOf(tabladatos.getValueAt(seleccion,1)));
         product=String.valueOf(tabladatos.getValueAt(seleccion,2));
         descrip=String.valueOf(tabladatos.getValueAt(seleccion,3));
         marca=String.valueOf(tabladatos.getValueAt(seleccion,4));
         precio=Float.parseFloat(String.valueOf(tabladatos.getValueAt(seleccion,5)));
         cant=Integer.parseInt(String.valueOf(tabladatos.getValueAt(seleccion,6)));
             
    Consulta_Imagen();
            }
        };

        thread.start();
    }//GEN-LAST:event_tabladatosMousePressed

    private void jMenu3MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jMenu3MouseClicked
        Login l=new Login();
        l.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_jMenu3MouseClicked

    private void r_prActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_r_prActionPerformed
        R_PROD a=new    R_PROD();
        a.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_r_prActionPerformed

    private void r_emActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_r_emActionPerformed
        R_EMPL a=new   R_EMPL();
        a.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_r_emActionPerformed

    private void r_faActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_r_faActionPerformed
        R_FACTURA a=new R_FACTURA();
        a.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_r_faActionPerformed

    private void r_proActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_r_proActionPerformed
        R_PROVEE a=new R_PROVEE();
        a.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_r_proActionPerformed

    private void r_cliActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_r_cliActionPerformed
        Activacion a =new Activacion();
        a.ver=4;

        R_CLI A=new R_CLI();
        A.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_r_cliActionPerformed

    private void b_proActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_b_proActionPerformed
        Busqueda_Productos a=new Busqueda_Productos();
        a.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_b_proActionPerformed

    private void b_empActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_b_empActionPerformed
        Busqueda_Empleados a=new Busqueda_Empleados();
        a.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_b_empActionPerformed

    private void b_facActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_b_facActionPerformed
        Busqueda_Facturas a=new Busqueda_Facturas();
        a.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_b_facActionPerformed

    private void b_cliActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_b_cliActionPerformed
        Busqueda_Clientes a=new Busqueda_Clientes ();
        a.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_b_cliActionPerformed

    private void b_ventasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_b_ventasActionPerformed
        Busqueda_Venta P=new  Busqueda_Venta();
        this.setVisible(false);
        P.setVisible(true);
    }//GEN-LAST:event_b_ventasActionPerformed

    private void m_proActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_m_proActionPerformed
        m_prod a=new  m_prod();
        a.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_m_proActionPerformed

    private void m_empActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_m_empActionPerformed
        m_empl a=new  m_empl ();
        a.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_m_empActionPerformed

    private void m_cliActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_m_cliActionPerformed
        m_cliente a=new  m_cliente();
        a.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_m_cliActionPerformed

    private void m_provActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_m_provActionPerformed
        m_prov a=new  m_prov ();
        a.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_m_provActionPerformed

    private void devActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_devActionPerformed
        Devolucion l=new Devolucion();
        l.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_devActionPerformed

    private void venActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_venActionPerformed
        Ventas_Cred a=new Ventas_Cred();
        a.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_venActionPerformed

    private void venEActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_venEActionPerformed
        Ventas a=new Ventas();
        a.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_venEActionPerformed

    private void repActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_repActionPerformed
  Reportes r =new Reportes();
  r.setVisible(true);
  this.setVisible(false);
    }//GEN-LAST:event_repActionPerformed

    private void r_pagoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_r_pagoActionPerformed
        PAGO P=new PAGO();
        this.setVisible(false);
        P.setVisible(true);
    }//GEN-LAST:event_r_pagoActionPerformed

    private void jButton7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton7ActionPerformed
        System.exit(0);
    }//GEN-LAST:event_jButton7ActionPerformed

    private void id_venta1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_id_venta1ActionPerformed
    
    }//GEN-LAST:event_id_venta1ActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
     Thread thread = new Thread() {
            public void run() {
        mostrar_detalle_venta();
            }
        };

        thread.start();
    }//GEN-LAST:event_jButton1ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Devolucion.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Devolucion.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Devolucion.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Devolucion.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Devolucion().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenuItem b_cli;
    private javax.swing.JMenuItem b_emp;
    private javax.swing.JMenuItem b_fac;
    private javax.swing.JMenuItem b_pro;
    private javax.swing.JMenuItem b_ventas;
    private javax.swing.JMenuItem dev;
    private javax.swing.JMenu devolucion;
    private javax.swing.JLabel dis;
    private javax.swing.JMenu ea;
    private javax.swing.JTextField id_venta1;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton7;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenu jMenu3;
    private javax.swing.JMenu jMenu4;
    private javax.swing.JMenu jMenu5;
    private javax.swing.JMenu jMenu6;
    private javax.swing.JMenu jMenu7;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPopupMenu jPopupMenu1;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JLabel loading;
    private javax.swing.JMenuItem m_cli;
    private javax.swing.JMenuItem m_emp;
    private javax.swing.JMenuItem m_pro;
    private javax.swing.JMenuItem m_prov;
    private javax.swing.JMenu modificacion;
    private javax.swing.JMenuItem r_cli;
    private javax.swing.JMenuItem r_em;
    private javax.swing.JMenuItem r_fa;
    private javax.swing.JMenuItem r_pago;
    private javax.swing.JMenuItem r_pr;
    private javax.swing.JMenuItem r_pro;
    private javax.swing.JLabel rec_img;
    private javax.swing.JMenuItem rep;
    private javax.swing.JTable tabladatos;
    private javax.swing.JLabel to1;
    private javax.swing.JMenuItem ven;
    private javax.swing.JMenuItem venE;
    // End of variables declaration//GEN-END:variables
}
