package main;

import clases.Imp_fac;
import clases.TextPrompt;
import clases.Conexion_postgres;
import clases.Activacion;
import clases.Conexion_mysql;
import java.awt.Color;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Types;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;
import javax.swing.RowFilter;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;

public class Ventas extends javax.swing.JFrame {
   private TableRowSorter filtro;
    float sum, resta;
    Imp_fac f = new Imp_fac();
    int cant, existenciaval;
    public int id, codigo, existencia;
    public float precio, total = 0;
    String nombre, descripcion, marca;
    Activacion ac = new Activacion();
    int id_domicilio, Id_venta;
    DefaultTableModel model = new DefaultTableModel();
    Conexion_postgres cn = new Conexion_postgres();

    public Ventas() {
        initComponents();
        //primera tabla
tabladatos.setOpaque(false);
jScrollPane4.setOpaque(false);
jScrollPane4.getViewport().setOpaque(false);
//segunda tabla
tabladatos1.setOpaque(false);
jScrollPane2.setOpaque(false);
jScrollPane2.getViewport().setOpaque(false);
        loading.setVisible(false);
        //no modificar filas tabla
        tabladatos1.setDefaultEditor(Object.class, null);
        tabladatos.setDefaultEditor(Object.class, null);
        Activacion ac = new Activacion();
        vendedor.setText(ac.nom_log);
        dire.setEnabled(false);
        combustible.setEnabled(false);
        Productos_Entrantes();
        modelo_tabla2_mostrar_prod();

        desc.setEnabled(false);
        TextPrompt p = new TextPrompt("Ingrese Nombre", v_nom);
        TextPrompt q = new TextPrompt("Ingrese Descripcion", v_desc);
        TextPrompt r = new TextPrompt("Ingrese Marca", v_marc);
        this.setLocationRelativeTo(null);
        //ACTIVACION
        r_pr.setEnabled(false);
        r_pro.setEnabled(false);
        r_em.setEnabled(false);
        r_fa.setEnabled(false);
        r_cli.setEnabled(false);
        b_pro.setEnabled(false);
        b_emp.setEnabled(false);
        b_fac.setEnabled(false);
        b_cli.setEnabled(false);
        ven.setEnabled(false);
        venE.setEnabled(false);
        rep.setEnabled(false);
        dev.setEnabled(false);
        m_pro.setEnabled(false);
        m_emp.setEnabled(false);
        m_cli.setEnabled(false);
        m_prov.setEnabled(false);
        b_ventas.setEnabled(false);
        r_pago.setEnabled(false);
        Activacion b = new Activacion();
        if (b.ver == 1) {
            b.entrante(r_pr, r_pro, r_em, r_fa, r_cli, b_pro, b_emp, b_fac, b_cli, modificacion, devolucion, ven, ea, dev, m_pro,
                    m_emp, m_cli, m_prov, rep, venE, b_ventas, r_pago);
        }
        if (b.ver == 2) {
            b.en(ven, venE, r_fa, b_pro, r_pr, r_pro, r_cli, r_pago, dev, b_cli, b_fac);
        }
        Venta_efec_Consulta_productos();
    }

    //borrar tabla
    public void borrar(DefaultTableModel tabla) {
        while (tabla.getRowCount() > 0) {
            tabla.removeRow(0);
        }
    }

    public void Insertar_Domicilio() {

        Conexion_mysql con;
        con = new Conexion_mysql(loading);
        java.sql.Connection cn = con.getConnection();

        String sql = "call PRC_vent_a_domicilio(?,?)";
        try {

            CallableStatement cs = cn.prepareCall(sql);
            cs.setFloat(1, Float.parseFloat(combustible.getText()));
            cs.setString(2, dire.getText().toUpperCase());
            if (cs.execute()) {
                JOptionPane.showMessageDialog(null, "Registro Completado");
            }
            cs.close();
        } catch (Exception ex) {
            Logger.getLogger(R_PROD.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void Insertar_Venta_Efectivo(int id_dom) {
        Conexion_mysql con;
        con = new Conexion_mysql(loading);
        java.sql.Connection cn = con.getConnection();

        String sql = "call PRC_reg_venta(?,?,?,?,?,?,?)";
        try {

            CallableStatement cs = cn.prepareCall(sql);
            cs.setInt(1, ac.codigo_log);//usuario
            cs.setInt(2, 1);//tipo venta
            cs.setInt(3, 1);//cliente
            cs.setInt(4, id_dom);//ID DOMICILIO
            cs.setFloat(5, Float.parseFloat(desc.getText()));
            cs.setFloat(6, Float.parseFloat(t_pag.getText()));
            cs.setString(7, "CANCELADO");
            if (cs.execute()) {
                JOptionPane.showMessageDialog(null, "Registro Completado");
            }
            cs.close();
        } catch (Exception ex) {
            Logger.getLogger(R_PROD.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    //Menu para seleccionar Productos a comprar
    public void modelo_tabla2_mostrar_prod() {

        model.addColumn("Codigo");
        model.addColumn("Nombre");
        model.addColumn("Descripcion");
        model.addColumn("Marca");
        model.addColumn("Precio/U");
        model.addColumn("Cantidad");
        model.addColumn("Subtotal");
        tabladatos1.setModel(model);
    }

    public void Consulta_Imagen() {
        //Variables para la imagen
        InputStream is;
        ImageIcon foto;
        Conexion_mysql con;
        con = new Conexion_mysql(loading);
        java.sql.Connection cn = con.getConnection();

        String readMetaData = "CALL PRC_Con_Imag_Re_Fac(?) ";
        try {
            cn.setAutoCommit(false); // This line must be written just after the 

            CallableStatement cs = cn.prepareCall(readMetaData);
            cs.setInt(1, codigo);
            //cs.registerOutParameter(2, Types.REF_CURSOR);
//cs.execute();
            ResultSet rs = (ResultSet) cs.executeQuery();
            if (rs != null) {
                while (rs.next()) {

//PROCESO PARA CONSULTAR IMAGEN
                    is = rs.getBinaryStream(1);
                    try {
                        BufferedImage bi = ImageIO.read(is);
                        foto = new ImageIcon(bi);
                        Image img = foto.getImage();
                        Image newimg = img.getScaledInstance(150, 140,
                                java.awt.Image.SCALE_SMOOTH);
                        ImageIcon newicon = new ImageIcon(newimg);
                        r_img.setIcon(newicon);//enviarlo a un jlabel    
                    } catch (IOException ex) {
                        Logger.getLogger(R_FACTURA.class.getName()).log(Level.SEVERE, null, ex);
                    }

//TERMINACION DE PROCESO PARA CONSULTAR IMAGEN
                }//end while
            }//end if
        } catch (SQLException ex) {
            Logger.getLogger(R_FACTURA.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void Venta_efec_Consulta_productos() {
        Conexion_mysql con;
        con = new Conexion_mysql(loading);
        java.sql.Connection cn = con.getConnection();

        DefaultTableModel modelo = new DefaultTableModel();

        String readMetaData = "CALL PRC_prod_vent_efec() ";
        try {
            this.tabladatos.setModel(modelo);
            cn.setAutoCommit(false); // This line must be written just after the 

            CallableStatement cs = cn.prepareCall(readMetaData);

            //cs.registerOutParameter(4, Types.REF_CURSOR);//registro que va a ver un cursor de salida
            //cs.execute();
            ResultSet rs = (ResultSet) cs.executeQuery();
            ResultSetMetaData rsMd = rs.getMetaData();
            //La cantidad de columnas que tiene la consulta
            int cantidadColumnas = rsMd.getColumnCount();
            //Establecer como cabezeras el nombre de las columnas
            for (int i = 1; i <= cantidadColumnas; i++) {
                modelo.addColumn(rsMd.getColumnLabel(i));

            }
            //Creando las filas para el JTable
            while (rs.next()) {
                Object[] fila = new Object[cantidadColumnas];
                for (int i = 0; i < cantidadColumnas; i++) {
                    fila[i] = rs.getObject(i + 1);
                }
                modelo.addRow(fila);
            }
            rs.close();
            cn.close();
        } catch (SQLException ex) {
            Logger.getLogger(m_prov.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void Productos_Entrantes() {
        //Sección 1
        // DefaultTableModel model = (DefaultTableModel) tabladatos1.getModel(); 
        JPopupMenu popupMenu = new JPopupMenu();
        JMenuItem menuItem1 = new JMenuItem("Eliminar", new ImageIcon(getClass().getResource("/img/registro.png")));
        menuItem1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                int a = tabladatos1.getSelectedRow();
                int cuentaFilasSeleccionadas = tabladatos1.getSelectedRowCount();
                if (cuentaFilasSeleccionadas == 0) {
                    JOptionPane.showMessageDialog(null, "No hay filas seleccinada");
                } else {

                    model.removeRow(a);
                    suma();
                    t_pag1.setText(t_pag.getText());
                }
            }
        });
        popupMenu.add(menuItem1);
        tabladatos1.setComponentPopupMenu(popupMenu);
    }

    public void Consulta_ID_Domicilio() {
        Conexion_mysql con;
        con = new Conexion_mysql(loading);
        java.sql.Connection cn = con.getConnection();

        String readMetaData = "CALL  PRC_mostrar_id_domicilio()";
        try {
            cn.setAutoCommit(false); // This line must be written just after the 

            CallableStatement cs = cn.prepareCall(readMetaData);

            //cs.registerOutParameter(1, Types.REF_CURSOR);
//cs.execute();
            ResultSet rs = (ResultSet) cs.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    id_domicilio = rs.getInt(1);
                    System.out.print("\n ID ADOMICILIO"+id_domicilio+"\n");
                }//end while
            }//end if
        } catch (SQLException ex) {

        }
    }//end if

    public void Consulta_ID_Venta() {
        Conexion_mysql con;
        con = new Conexion_mysql(loading);
        java.sql.Connection cn = con.getConnection();

        String readMetaData = "CALL PRC_Consulta_Id_Venta()";
        try {
            cn.setAutoCommit(false); // This line must be written just after the 

            CallableStatement cs = cn.prepareCall(readMetaData);

            //cs.registerOutParameter(1, Types.REF_CURSOR);
//cs.execute();
            ResultSet rs = (ResultSet) cs.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    Id_venta = rs.getInt(1);
                    System.out.print(Id_venta);
                }//end while
            }//end if
        } catch (SQLException ex) {

        }
    }//end if

//REGISTRO DETALLE VENTA
    public void registro_detalle_Venta_Efectivo() {
        Conexion_mysql con;
        con = new Conexion_mysql(loading);
        java.sql.Connection cn = con.getConnection();

        if (tabladatos1.getRowCount() > 0) {
            for (int i = 0; i < tabladatos1.getRowCount(); i++) {
                String Sql = "call PRC_reg_detalle_venta(?,?,?,?)";
                try {
                    CallableStatement cs = cn.prepareCall(Sql);
                    cs.setInt(1, Integer.parseInt(tabladatos1.getValueAt(i, 0).toString()));
                    cs.setInt(2, Id_venta);
                    cs.setFloat(3, Float.parseFloat(tabladatos1.getValueAt(i, 4).toString()));
                    cs.setInt(4, Integer.parseInt(tabladatos1.getValueAt(i, 5).toString()));
                    if (cs.execute()) {
                        JOptionPane.showMessageDialog(null, "Registro Completado");
                    }
                    cs.close();
                } catch (Exception e) {
                    JOptionPane.showMessageDialog(this, e);
                }
            }
        } else {
            JOptionPane.showMessageDialog(this, "La tabla se encuentra vacio");
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPopupMenu1 = new javax.swing.JPopupMenu();
        agregar = new javax.swing.JMenuItem();
        jPopupMenu3 = new javax.swing.JPopupMenu();
        eliminar = new javax.swing.JMenuItem();
        jRadioButtonMenuItem1 = new javax.swing.JRadioButtonMenuItem();
        jPanel1 = new javax.swing.JPanel();
        fecha = new javax.swing.JLabel();
        v_marc = new javax.swing.JTextField();
        jScrollPane2 = new javax.swing.JScrollPane();
        tabladatos = new javax.swing.JTable();
        r_img = new javax.swing.JLabel();
        loading = new javax.swing.JLabel();
        v_desc = new javax.swing.JTextField();
        v_nom = new javax.swing.JTextField();
        jLabel15 = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        check = new javax.swing.JCheckBox();
        jLabel17 = new javax.swing.JLabel();
        vendedor = new javax.swing.JLabel();
        rSLabelHora1 = new rojeru_san.componentes.RSLabelHora();
        rSLabelFecha1 = new rojeru_san.componentes.RSLabelFecha();
        jLabel14 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        dire = new javax.swing.JTextField();
        combustible = new javax.swing.JTextField();
        jButton7 = new javax.swing.JButton();
        jLabel6 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jLabel7 = new javax.swing.JLabel();
        jButton2 = new javax.swing.JButton();
        jScrollPane4 = new javax.swing.JScrollPane();
        tabladatos1 = new javax.swing.JTable();
        jLabel10 = new javax.swing.JLabel();
        t_pag = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        t_pag1 = new javax.swing.JLabel();
        jLabel27 = new javax.swing.JLabel();
        desc = new javax.swing.JTextField();
        sel1 = new javax.swing.JCheckBox();
        CanP = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        vueltos = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jMenu3 = new javax.swing.JMenu();
        jMenu2 = new javax.swing.JMenu();
        jMenu4 = new javax.swing.JMenu();
        r_pr = new javax.swing.JMenuItem();
        r_em = new javax.swing.JMenuItem();
        r_fa = new javax.swing.JMenuItem();
        r_pro = new javax.swing.JMenuItem();
        r_cli = new javax.swing.JMenuItem();
        jMenu6 = new javax.swing.JMenu();
        b_pro = new javax.swing.JMenuItem();
        b_emp = new javax.swing.JMenuItem();
        b_fac = new javax.swing.JMenuItem();
        b_cli = new javax.swing.JMenuItem();
        b_ventas = new javax.swing.JMenuItem();
        modificacion = new javax.swing.JMenu();
        m_pro = new javax.swing.JMenuItem();
        m_emp = new javax.swing.JMenuItem();
        m_cli = new javax.swing.JMenuItem();
        m_prov = new javax.swing.JMenuItem();
        devolucion = new javax.swing.JMenu();
        dev = new javax.swing.JMenuItem();
        jMenu7 = new javax.swing.JMenu();
        ven = new javax.swing.JMenuItem();
        venE = new javax.swing.JMenuItem();
        ea = new javax.swing.JMenu();
        rep = new javax.swing.JMenuItem();
        jMenu5 = new javax.swing.JMenu();
        r_pago = new javax.swing.JMenuItem();

        agregar.setText("agregar");
        agregar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                agregarActionPerformed(evt);
            }
        });
        jPopupMenu1.add(agregar);

        eliminar.setText("eliminar");
        eliminar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                eliminarActionPerformed(evt);
            }
        });
        jPopupMenu3.add(eliminar);

        jRadioButtonMenuItem1.setSelected(true);
        jRadioButtonMenuItem1.setText("jRadioButtonMenuItem1");

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setMinimumSize(new java.awt.Dimension(1025, 501));
        setUndecorated(true);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setBorder(javax.swing.BorderFactory.createMatteBorder(1, 1, 1, 1, new java.awt.Color(255, 204, 51)));
        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());
        jPanel1.add(fecha, new org.netbeans.lib.awtextra.AbsoluteConstraints(550, 10, 40, 30));

        v_marc.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                v_marcActionPerformed(evt);
            }
        });
        v_marc.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                v_marcKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                v_marcKeyTyped(evt);
            }
        });
        jPanel1.add(v_marc, new org.netbeans.lib.awtextra.AbsoluteConstraints(240, 110, 210, 30));

        tabladatos.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        tabladatos.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null}
            },
            new String [] {
                "Id Producto", "Nombre Producto", "Existencia"
            }
        ));
        tabladatos.setComponentPopupMenu(jPopupMenu1);
        tabladatos.setName("agregar"); // NOI18N
        tabladatos.setSelectionBackground(new java.awt.Color(51, 204, 255));
        tabladatos.setSelectionForeground(new java.awt.Color(0, 0, 51));
        tabladatos.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tabladatosMouseClicked(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                tabladatosMousePressed(evt);
            }
        });
        jScrollPane2.setViewportView(tabladatos);

        jPanel1.add(jScrollPane2, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 150, 1050, 160));

        r_img.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 0)));
        jPanel1.add(r_img, new org.netbeans.lib.awtextra.AbsoluteConstraints(1060, 150, 160, 160));

        loading.setIcon(new javax.swing.ImageIcon(getClass().getResource("/main/loading_1.gif"))); // NOI18N
        jPanel1.add(loading, new org.netbeans.lib.awtextra.AbsoluteConstraints(860, -10, 260, 160));

        v_desc.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                v_descActionPerformed(evt);
            }
        });
        v_desc.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                v_descKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                v_descKeyTyped(evt);
            }
        });
        jPanel1.add(v_desc, new org.netbeans.lib.awtextra.AbsoluteConstraints(470, 110, 210, 30));

        v_nom.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                v_nomActionPerformed(evt);
            }
        });
        v_nom.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                v_nomKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                v_nomKeyTyped(evt);
            }
        });
        jPanel1.add(v_nom, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 110, 210, 30));

        jLabel15.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        jLabel15.setText("Selección de Producto");
        jPanel1.add(jLabel15, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 80, 250, 30));
        jPanel1.add(jSeparator1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 70, 730, 10));

        check.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        check.setText("A Domicilio");
        check.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                checkMouseClicked(evt);
            }
        });
        jPanel1.add(check, new org.netbeans.lib.awtextra.AbsoluteConstraints(290, 30, 130, -1));

        jLabel17.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        jLabel17.setText("Vendedor");
        jPanel1.add(jLabel17, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, 110, -1));

        vendedor.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        vendedor.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanel1.add(vendedor, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 30, 250, 20));
        jPanel1.add(rSLabelHora1, new org.netbeans.lib.awtextra.AbsoluteConstraints(1010, 30, 180, 20));
        jPanel1.add(rSLabelFecha1, new org.netbeans.lib.awtextra.AbsoluteConstraints(1010, 10, 180, 20));

        jLabel14.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/MOTO.png"))); // NOI18N
        jPanel1.add(jLabel14, new org.netbeans.lib.awtextra.AbsoluteConstraints(790, 0, 80, 70));

        jLabel13.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        jLabel13.setText("Dirección");
        jPanel1.add(jLabel13, new org.netbeans.lib.awtextra.AbsoluteConstraints(450, 0, 100, 30));

        jLabel16.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        jLabel16.setText("  Combustible Q ");
        jPanel1.add(jLabel16, new org.netbeans.lib.awtextra.AbsoluteConstraints(590, 0, 150, 30));

        dire.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        dire.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        dire.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                direActionPerformed(evt);
            }
        });
        jPanel1.add(dire, new org.netbeans.lib.awtextra.AbsoluteConstraints(450, 30, 140, 20));

        combustible.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        combustible.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        combustible.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                combustibleActionPerformed(evt);
            }
        });
        combustible.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                combustibleKeyTyped(evt);
            }
        });
        jPanel1.add(combustible, new org.netbeans.lib.awtextra.AbsoluteConstraints(600, 30, 140, 20));

        jButton7.setFont(new java.awt.Font("Tahoma", 3, 24)); // NOI18N
        jButton7.setForeground(new java.awt.Color(255, 255, 255));
        jButton7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/salir.png"))); // NOI18N
        jButton7.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jButton7.setIconTextGap(1);
        jButton7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton7ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton7, new org.netbeans.lib.awtextra.AbsoluteConstraints(1160, 0, 50, 40));

        jLabel6.setBackground(new java.awt.Color(255, 255, 255));
        jLabel6.setFont(new java.awt.Font("Tahoma", 3, 14)); // NOI18N
        jLabel6.setText("SALIR");
        jPanel1.add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(1160, 40, -1, -1));

        getContentPane().add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 1230, 310));

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));
        jPanel2.setLayout(null);

        jLabel7.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        jLabel7.setText("Detalle de Venta");
        jPanel2.add(jLabel7);
        jLabel7.setBounds(10, 0, 250, 30);

        jButton2.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        jButton2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/save.png"))); // NOI18N
        jButton2.setText("REALIZAR VENTA");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });
        jPanel2.add(jButton2);
        jButton2.setBounds(940, 250, 270, 30);

        tabladatos1.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        tabladatos1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null}
            },
            new String [] {
                "Id Producto", "Nombre Producto", "Existencia"
            }
        ));
        tabladatos1.setGridColor(new java.awt.Color(255, 255, 255));
        tabladatos1.setName("agregar"); // NOI18N
        tabladatos1.setOpaque(false);
        tabladatos1.setSelectionBackground(new java.awt.Color(51, 204, 255));
        tabladatos1.setSelectionForeground(new java.awt.Color(0, 0, 51));
        tabladatos1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tabladatos1MouseClicked(evt);
            }
        });
        jScrollPane4.setViewportView(tabladatos1);

        jPanel2.add(jScrollPane4);
        jScrollPane4.setBounds(10, 30, 1210, 140);

        jLabel10.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        jLabel10.setText("Subtotal");
        jPanel2.add(jLabel10);
        jLabel10.setBounds(10, 210, 90, 20);

        t_pag.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        t_pag.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(102, 102, 0)));
        jPanel2.add(t_pag);
        t_pag.setBounds(100, 210, 120, 20);

        jLabel9.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        jLabel9.setText("Total Final");
        jPanel2.add(jLabel9);
        jLabel9.setBounds(480, 210, 110, 20);

        t_pag1.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        t_pag1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanel2.add(t_pag1);
        t_pag1.setBounds(570, 210, 120, 20);

        jLabel27.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        jLabel27.setText("Descuento");
        jPanel2.add(jLabel27);
        jLabel27.setBounds(260, 210, 80, 20);

        desc.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        desc.setText("0");
        desc.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        desc.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                descActionPerformed(evt);
            }
        });
        desc.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                descKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                descKeyTyped(evt);
            }
        });
        jPanel2.add(desc);
        desc.setBounds(350, 210, 120, 20);

        sel1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                sel1ActionPerformed(evt);
            }
        });
        jPanel2.add(sel1);
        sel1.setBounds(230, 210, 19, 19);

        CanP.setFont(new java.awt.Font("Times New Roman", 3, 18)); // NOI18N
        CanP.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        CanP.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CanPActionPerformed(evt);
            }
        });
        CanP.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                CanPKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                CanPKeyTyped(evt);
            }
        });
        jPanel2.add(CanP);
        CanP.setBounds(820, 210, 130, 20);

        jLabel2.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        jLabel2.setText("Efectivo");
        jPanel2.add(jLabel2);
        jLabel2.setBounds(730, 210, 80, 20);

        vueltos.setFont(new java.awt.Font("Times New Roman", 3, 18)); // NOI18N
        vueltos.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanel2.add(vueltos);
        vueltos.setBounds(1060, 210, 130, 20);

        jLabel5.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        jLabel5.setText("Cambio");
        jPanel2.add(jLabel5);
        jLabel5.setBounds(970, 210, 110, 20);

        getContentPane().add(jPanel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 310, 1230, 290));

        jMenuBar1.setBorder(null);
        jMenuBar1.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jMenuBar1.setFont(new java.awt.Font("Segoe UI", 2, 12)); // NOI18N

        jMenu1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/can.png"))); // NOI18N
        jMenu1.setText("LOGIN");

        jMenu3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Fondo.png"))); // NOI18N
        jMenu3.setText("LOGIN");
        jMenu3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jMenu3MouseClicked(evt);
            }
        });
        jMenu1.add(jMenu3);

        jMenuBar1.add(jMenu1);
        jMenuBar1.add(jMenu2);

        jMenu4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/add (1).png"))); // NOI18N
        jMenu4.setText(" REGISTRO");

        r_pr.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_R, java.awt.event.InputEvent.CTRL_DOWN_MASK));
        r_pr.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/ingreso.png"))); // NOI18N
        r_pr.setText("PRODUCTO");
        r_pr.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                r_prActionPerformed(evt);
            }
        });
        jMenu4.add(r_pr);

        r_em.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_E, java.awt.event.InputEvent.CTRL_DOWN_MASK));
        r_em.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/emple.png"))); // NOI18N
        r_em.setText("EMPLEADOS");
        r_em.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                r_emActionPerformed(evt);
            }
        });
        jMenu4.add(r_em);

        r_fa.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F, java.awt.event.InputEvent.CTRL_DOWN_MASK));
        r_fa.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/REGISTRO FACTURA.png"))); // NOI18N
        r_fa.setText("FACTURA");
        r_fa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                r_faActionPerformed(evt);
            }
        });
        jMenu4.add(r_fa);

        r_pro.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_P, java.awt.event.InputEvent.CTRL_DOWN_MASK));
        r_pro.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/proveNue.png"))); // NOI18N
        r_pro.setText("PROVEEDOR");
        r_pro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                r_proActionPerformed(evt);
            }
        });
        jMenu4.add(r_pro);

        r_cli.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_C, java.awt.event.InputEvent.CTRL_DOWN_MASK));
        r_cli.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/cliente.png"))); // NOI18N
        r_cli.setText("CLIENTE");
        r_cli.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                r_cliActionPerformed(evt);
            }
        });
        jMenu4.add(r_cli);

        jMenuBar1.add(jMenu4);

        jMenu6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Busqueda (1).png"))); // NOI18N
        jMenu6.setText("BUSQUEDA");

        b_pro.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_P, java.awt.event.InputEvent.ALT_DOWN_MASK));
        b_pro.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/prodddd.png"))); // NOI18N
        b_pro.setText("PRODUCTOS");
        b_pro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                b_proActionPerformed(evt);
            }
        });
        jMenu6.add(b_pro);

        b_emp.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_E, java.awt.event.InputEvent.ALT_DOWN_MASK));
        b_emp.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/busPer .png"))); // NOI18N
        b_emp.setText("EMPLEADOS");
        b_emp.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                b_empActionPerformed(evt);
            }
        });
        jMenu6.add(b_emp);

        b_fac.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F, java.awt.event.InputEvent.ALT_DOWN_MASK));
        b_fac.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/busqueda_factura.png"))); // NOI18N
        b_fac.setText("FACTURA");
        b_fac.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                b_facActionPerformed(evt);
            }
        });
        jMenu6.add(b_fac);

        b_cli.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_C, java.awt.event.InputEvent.ALT_DOWN_MASK));
        b_cli.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/bus_em.png"))); // NOI18N
        b_cli.setText("CLIENTE");
        b_cli.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                b_cliActionPerformed(evt);
            }
        });
        jMenu6.add(b_cli);

        b_ventas.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_Z, java.awt.event.InputEvent.ALT_DOWN_MASK));
        b_ventas.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Busqueda1.png"))); // NOI18N
        b_ventas.setText("VENTAS");
        b_ventas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                b_ventasActionPerformed(evt);
            }
        });
        jMenu6.add(b_ventas);

        jMenuBar1.add(jMenu6);

        modificacion.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/modificacion.png"))); // NOI18N
        modificacion.setText("MODIFICAR");

        m_pro.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_K, java.awt.event.InputEvent.ALT_DOWN_MASK));
        m_pro.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/PRODUCTO.png"))); // NOI18N
        m_pro.setText("PRODUCTO");
        m_pro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                m_proActionPerformed(evt);
            }
        });
        modificacion.add(m_pro);

        m_emp.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_E, java.awt.event.InputEvent.SHIFT_DOWN_MASK));
        m_emp.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/empleado.png"))); // NOI18N
        m_emp.setText("EMPLEADO");
        m_emp.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                m_empActionPerformed(evt);
            }
        });
        modificacion.add(m_emp);

        m_cli.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_C, java.awt.event.InputEvent.SHIFT_DOWN_MASK));
        m_cli.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/clientee.png"))); // NOI18N
        m_cli.setText("CLIENTE");
        m_cli.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                m_cliActionPerformed(evt);
            }
        });
        modificacion.add(m_cli);

        m_prov.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_P, java.awt.event.InputEvent.SHIFT_DOWN_MASK));
        m_prov.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/proveedores.png"))); // NOI18N
        m_prov.setText("PROVEEDORES");
        m_prov.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                m_provActionPerformed(evt);
            }
        });
        modificacion.add(m_prov);

        jMenuBar1.add(modificacion);

        devolucion.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/DEV.png"))); // NOI18N
        devolucion.setText("DEVOULUCION");

        dev.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_D, java.awt.event.InputEvent.CTRL_DOWN_MASK));
        dev.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/dev1.png"))); // NOI18N
        dev.setText("DEVOLUCION");
        dev.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                devActionPerformed(evt);
            }
        });
        devolucion.add(dev);

        jMenuBar1.add(devolucion);

        jMenu7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/png-transparent-shopping-cart-graphy-cart-supermarket-vehicle-shopping-bags-trolleys (1) (1).png"))); // NOI18N
        jMenu7.setText("VENTA");

        ven.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_V, java.awt.event.InputEvent.CTRL_DOWN_MASK));
        ven.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/venta a credito.png"))); // NOI18N
        ven.setText("VENTA  A CREDITO");
        ven.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                venActionPerformed(evt);
            }
        });
        jMenu7.add(ven);

        venE.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_B, java.awt.event.InputEvent.CTRL_DOWN_MASK));
        venE.setIcon(new javax.swing.ImageIcon(getClass().getResource("/main/venta efectivo.png"))); // NOI18N
        venE.setText("VENTA EN EFECTIVO");
        venE.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                venEActionPerformed(evt);
            }
        });
        jMenu7.add(venE);

        jMenuBar1.add(jMenu7);

        ea.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/REPORTE.png"))); // NOI18N
        ea.setText("REPORTE");

        rep.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_R, java.awt.event.InputEvent.ALT_DOWN_MASK));
        rep.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/reportess.png"))); // NOI18N
        rep.setText(" REPORTES");
        rep.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                repActionPerformed(evt);
            }
        });
        ea.add(rep);

        jMenuBar1.add(ea);

        jMenu5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Pago_C_P.png"))); // NOI18N
        jMenu5.setText("PAGOS");

        r_pago.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Pago_C_P (2).png"))); // NOI18N
        r_pago.setText("REALIZAR PAGOS");
        r_pago.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                r_pagoActionPerformed(evt);
            }
        });
        jMenu5.add(r_pago);

        jMenuBar1.add(jMenu5);

        setJMenuBar(jMenuBar1);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void v_marcActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_v_marcActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_v_marcActionPerformed

    private void v_descActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_v_descActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_v_descActionPerformed

    private void v_nomActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_v_nomActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_v_nomActionPerformed

    private void tabladatosMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tabladatosMouseClicked


    }//GEN-LAST:event_tabladatosMouseClicked

    private void eliminarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_eliminarActionPerformed
        int a = tabladatos1.getSelectedRow();
        model.removeRow(a);

    }//GEN-LAST:event_eliminarActionPerformed

    private void v_nomKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_v_nomKeyReleased

    }//GEN-LAST:event_v_nomKeyReleased

    private void v_descKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_v_descKeyReleased

    }//GEN-LAST:event_v_descKeyReleased

    private void v_marcKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_v_marcKeyReleased


    }//GEN-LAST:event_v_marcKeyReleased
    public void suma() {
        double suma = 0;
        double suma1 = 0;

        for (int i = 0; i < tabladatos1.getRowCount(); i++) {

            suma = Double.parseDouble(tabladatos1.getValueAt(i, 6).toString());
            suma1 += suma;

        }
        t_pag.setText(Double.toString(suma1));
    }
    private void agregarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_agregarActionPerformed
        try {

            String[] datos = new String[9];
            String cantidad = JOptionPane.showInputDialog("<html><h1>Escriba la cantidad<h1></html>");

            if (null == cantidad) {
                System.out.println("La Operacion ha sido cancelada");
            } else {
                if (cantidad.equals("")) {
                    JOptionPane.showMessageDialog(null, "<html><h1>Vuelva a ingresar el Cantidad<h1></html>", "ERROR", JOptionPane.ERROR_MESSAGE);
                    String cantidad1 = JOptionPane.showInputDialog("<html><h1>Escriba la cantidad<h1></html>");
                    ///
                    //Eliminar filas repetidas
                    int cuentaFilasSeleccionadas = tabladatos1.getSelectedRowCount();
                    float subtotal = 0;
                    boolean ver = false;
                    if (tabladatos1.getRowCount() >= 0) {

                        for (int i = 0; i < model.getRowCount(); i++) {
                            if (Integer.parseInt((String) tabladatos1.getValueAt(i, 0)) == codigo) {
                                model.removeRow(i);
                                JOptionPane.showMessageDialog(null, "<html><h1>Producto repitido !!!!!!<br>Vuelva a ingresar el Cantidad<h1></html>", "ERROR", JOptionPane.ERROR_MESSAGE);
                                suma();
                                ver = true;//si el producto es repetido se activa true
                            } else {

                            }
                        }

                    }
                    if (ver == false) {//

                        if (!(Integer.parseInt(cantidad) <= 0) && !(existenciaval <= 0)) {
                            try {
                                cant = Integer.parseInt(cantidad1);
                                subtotal = cant * precio;
                                datos[0] = Integer.toString(codigo);
                                datos[1] = nombre;
                                datos[2] = descripcion;
                                datos[3] = marca;
                                datos[4] = Float.toString(precio);
                                datos[5] = cantidad1;
                                datos[6] = Float.toString(subtotal);

                                model.addRow(datos);
                            } catch (Exception e) {
                                JOptionPane.showMessageDialog(null, e);
                            }
                            suma();
                        }
                    } else {

                    }
                    ///

                } else {//si se ingresa cantidad de un solo
                    if (!(Integer.parseInt(cantidad) <= 0) && !(existenciaval <= 0)) {
                        int cuentaFilasSeleccionadas = tabladatos1.getSelectedRowCount();
                        float subtotal = 0;
                        boolean ver = false;
                        cant = Integer.parseInt(cantidad);
                        if (tabladatos1.getRowCount() >= 0) {

                            for (int i = 0; i < model.getRowCount(); i++) {
                                if (Integer.parseInt((String) tabladatos1.getValueAt(i, 0)) == codigo) {
                                    model.removeRow(i);
                                    JOptionPane.showMessageDialog(null, "<html><h1>Producto repitiendo !!!!!!<br>Vuelva a ingresar el Cantidad<h1></html>", "ERROR", JOptionPane.ERROR_MESSAGE);
                                    suma();
                                    ver = true;//si el producto es repetido se activa true
                                } else {

                                }
                            }

                        }

                        if (ver == false) {//
                            try {
                                subtotal = cant * precio;
                                datos[0] = Integer.toString(codigo);
                                datos[1] = nombre;
                                datos[2] = descripcion;
                                datos[3] = marca;
                                datos[4] = Float.toString(precio);
                                datos[5] = cantidad;
                                datos[6] = Float.toString(subtotal);
                                model.addRow(datos);
                                suma();
                            } catch (Exception e) {
                                JOptionPane.showMessageDialog(null, e);
                            }
                        } else {

                        }

                    } else {
                        JOptionPane.showMessageDialog(null, "<html><h1>La cantidad no puede ser menor o igual a 0<h1></html>", "ERROR", JOptionPane.ERROR_MESSAGE);

                    }
                }

            }
            t_pag1.setText(t_pag.getText());
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "<html><h1>!!!!!!<br>Debe ingresar solo numeros<h1></html>", "ERROR", JOptionPane.ERROR_MESSAGE);

        }
    }//GEN-LAST:event_agregarActionPerformed
    //filtro por nombre de producto

    void filtro_nombre_Producto() {
        int ColumntaTabla = 1;
        filtro.setRowFilter(RowFilter.regexFilter(v_nom.getText(), ColumntaTabla));
    }
    //filtro por nombre de producto

    void filtro_Descripción_Producto() {
        int ColumntaTabla = 2;
        filtro.setRowFilter(RowFilter.regexFilter(v_desc.getText(), ColumntaTabla));
    }
    //filtro por nombre de producto

    void filtro_Marca_Producto() {
        int ColumntaTabla = 3;
        filtro.setRowFilter(RowFilter.regexFilter(v_marc.getText(), ColumntaTabla));
    }
    private void tabladatos1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tabladatos1MouseClicked
        int seleccion = tabladatos1.rowAtPoint(evt.getPoint());
        resta = Float.parseFloat(String.valueOf(tabladatos1.getValueAt(seleccion, 6)));
    }//GEN-LAST:event_tabladatos1MouseClicked

    private void tabladatosMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tabladatosMousePressed
         Thread thread = new Thread() {
            public void run() {
  int seleccion = tabladatos.rowAtPoint(evt.getPoint());
        codigo = Integer.parseInt(String.valueOf(tabladatos.getValueAt(seleccion, 0)));
        nombre = String.valueOf(tabladatos.getValueAt(seleccion, 1));
        descripcion = String.valueOf(tabladatos.getValueAt(seleccion, 2));
        marca = String.valueOf(tabladatos.getValueAt(seleccion, 3));
        precio = Float.parseFloat(String.valueOf(tabladatos.getValueAt(seleccion, 4)));
        existenciaval = Integer.parseInt(String.valueOf(tabladatos.getValueAt(seleccion, 5)));
        Consulta_Imagen();
        desc.setText("0");
        CanP.setText("0");
        vueltos.setText("0");
            }
        };

        thread.start();
      
    }//GEN-LAST:event_tabladatosMousePressed

    private void combustibleActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_combustibleActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_combustibleActionPerformed

    private void direActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_direActionPerformed
        // combustible.setEditable(false);
    }//GEN-LAST:event_direActionPerformed

    private void CanPKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_CanPKeyReleased

        if (desc.getText().equals("")) {
            JOptionPane.showMessageDialog(null, "DESELECCIONAR DESCUENTO");
        } else {

            try {
                Float a;
                if (desc.getText().isEmpty()) {
                    a = Float.parseFloat(t_pag.getText());
                    vueltos.setText("");
                } else {
                    a = Float.parseFloat(t_pag1.getText());

                    vueltos.setText("");

                }

                //demuestro los vueltos
                if (Float.parseFloat(CanP.getText()) - a < 0) {
                } else {
                    vueltos.setText(Float.toString(Float.parseFloat(CanP.getText()) - a));

                }
            } catch (Exception e) {
            }

        }


    }//GEN-LAST:event_CanPKeyReleased

    private void CanPActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CanPActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_CanPActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        Thread thread = new Thread() {
            public void run() {
        if (check.isSelected()) {
            Insertar_Domicilio();
            Consulta_ID_Domicilio();
            Insertar_Venta_Efectivo(id_domicilio);
            Consulta_ID_Venta();
            registro_detalle_Venta_Efectivo();
            borrar(model);
            Venta_efec_Consulta_productos();
        } else {
            Insertar_Venta_Efectivo(1);
            Consulta_ID_Venta();
            registro_detalle_Venta_Efectivo();
            borrar(model);
            Venta_efec_Consulta_productos();
        }
        f.setEmpleado(vendedor.getText());
        f.setNombre("C/F");
        f.setId_venta(Id_venta);
        f.setTotal(t_pag.getText());
        f.setDescuento(desc.getText());
        f.setTot_con_desc(t_pag1.getText());
        Comprobante c = new Comprobante();
        c.setVisible(true);
       
        }
        };
        thread.start();
         
    }//GEN-LAST:event_jButton2ActionPerformed

    private void descActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_descActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_descActionPerformed

    private void descKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_descKeyReleased
        CanP.setText("");
        vueltos.setText("");
        if (sel1.isSelected()) {

            try {
                if (Float.parseFloat(t_pag.getText()) - Float.parseFloat(desc.getText()) < 0) {
                    JOptionPane.showMessageDialog(null, "<html><h1>DESCUENTO NO ACEPTADO<h1></html>");
                    desc.setText("");
                    t_pag1.setText(Float.toString(Float.parseFloat(t_pag.getText())));
                } else {
                    t_pag1.setText(Float.toString(Float.parseFloat(t_pag.getText()) - Float.parseFloat(desc.getText())));
                }
            } catch (Exception e) {
                System.out.println(e);

                t_pag1.setText("");

                t_pag1.setText(Float.toString(Float.parseFloat(t_pag.getText())));

            }

        }
    }//GEN-LAST:event_descKeyReleased

    private void sel1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_sel1ActionPerformed

        try {

            if (sel1.isSelected()) {
                desc.setText("");
                desc.setEnabled(true);
            } else {
                desc.setEnabled(false);
                desc.setText("0");

                t_pag1.setText("");
                t_pag1.setText(Float.toString(Float.parseFloat(t_pag.getText())));

            }
        } catch (Exception e) {
        }
    }//GEN-LAST:event_sel1ActionPerformed

    private void jMenu3MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jMenu3MouseClicked
        Login l = new Login();
        l.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_jMenu3MouseClicked

    private void r_prActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_r_prActionPerformed
        R_PROD a = new R_PROD();
        a.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_r_prActionPerformed

    private void r_emActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_r_emActionPerformed
        R_EMPL a = new R_EMPL();
        a.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_r_emActionPerformed

    private void r_faActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_r_faActionPerformed
        R_FACTURA a = new R_FACTURA();
        a.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_r_faActionPerformed

    private void r_proActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_r_proActionPerformed
        R_PROVEE a = new R_PROVEE();
        a.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_r_proActionPerformed

    private void r_cliActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_r_cliActionPerformed

        R_CLI A = new R_CLI();
        A.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_r_cliActionPerformed

    private void b_proActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_b_proActionPerformed
        Busqueda_Productos a = new Busqueda_Productos();
        a.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_b_proActionPerformed

    private void b_empActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_b_empActionPerformed
        Busqueda_Empleados a = new Busqueda_Empleados();
        a.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_b_empActionPerformed

    private void b_facActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_b_facActionPerformed
        Busqueda_Facturas a = new Busqueda_Facturas();
        a.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_b_facActionPerformed

    private void b_cliActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_b_cliActionPerformed
        Busqueda_Clientes a = new Busqueda_Clientes();
        a.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_b_cliActionPerformed

    private void b_ventasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_b_ventasActionPerformed
        Busqueda_Venta P = new Busqueda_Venta();
        this.setVisible(false);
        P.setVisible(true);
    }//GEN-LAST:event_b_ventasActionPerformed

    private void m_proActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_m_proActionPerformed
        m_prod a = new m_prod();
        a.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_m_proActionPerformed

    private void m_empActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_m_empActionPerformed
        m_empl a = new m_empl();
        a.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_m_empActionPerformed

    private void m_cliActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_m_cliActionPerformed
        m_cliente a = new m_cliente();
        a.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_m_cliActionPerformed

    private void m_provActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_m_provActionPerformed
        m_prov a = new m_prov();
        a.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_m_provActionPerformed

    private void devActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_devActionPerformed
        Devolucion l = new Devolucion();
        l.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_devActionPerformed

    private void venActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_venActionPerformed
        Ventas_Cred a = new Ventas_Cred();
        a.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_venActionPerformed

    private void venEActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_venEActionPerformed
        Ventas a = new Ventas();
        a.setVisible(true);
        this.setVisible(false);

    }//GEN-LAST:event_venEActionPerformed

    private void repActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_repActionPerformed
        Reportes r = new Reportes();
        r.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_repActionPerformed

    private void r_pagoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_r_pagoActionPerformed
        PAGO P = new PAGO();
        this.setVisible(false);
        P.setVisible(true);
    }//GEN-LAST:event_r_pagoActionPerformed

    private void jButton7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton7ActionPerformed
        System.exit(0);
    }//GEN-LAST:event_jButton7ActionPerformed

    private void checkMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_checkMouseClicked

        if (check.isSelected()) {
            dire.setEnabled(true);
            combustible.setEnabled(true);
        } else {
            dire.setEnabled(false);
            combustible.setEnabled(false);
        }
    }//GEN-LAST:event_checkMouseClicked

    private void v_nomKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_v_nomKeyTyped

        char c=evt.getKeyChar();
     if(Character.isDigit(c)){
       
          evt.consume();
      }
        v_nom.addKeyListener(new KeyAdapter() {
            public void keyReleased(final KeyEvent e) {
                String cadena = (v_nom.getText().toUpperCase());
                v_nom.setText(cadena);
                filtro_nombre_Producto();
            }

        });
        filtro = new TableRowSorter(tabladatos.getModel());
        tabladatos.setRowSorter(filtro);
    }//GEN-LAST:event_v_nomKeyTyped

    private void v_descKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_v_descKeyTyped
          v_desc.addKeyListener(new KeyAdapter() {
            public void keyReleased(final KeyEvent e) {
                String cadena = (v_desc.getText().toUpperCase());
                v_desc.setText(cadena);
                filtro_Descripción_Producto();
            }

        });
        filtro = new TableRowSorter(tabladatos.getModel());
        tabladatos.setRowSorter(filtro);
    }//GEN-LAST:event_v_descKeyTyped

    private void v_marcKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_v_marcKeyTyped
        v_marc.addKeyListener(new KeyAdapter() {
            public void keyReleased(final KeyEvent e) {
                String cadena = (v_marc.getText().toUpperCase());
                v_marc.setText(cadena);
                filtro_Marca_Producto();
            }

        });
        filtro = new TableRowSorter(tabladatos.getModel());
        tabladatos.setRowSorter(filtro);
    }//GEN-LAST:event_v_marcKeyTyped

    private void combustibleKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_combustibleKeyTyped
        char c=evt.getKeyChar();
     if(!Character.isDigit(c)&c!='.'){
       
          evt.consume();
      }
    if(c=='.'&&combustible.getText().contains(".")){
          evt.consume();
      }
      
        if(combustible.getText().length() >= 8)
    {
        evt.consume();
    }
    }//GEN-LAST:event_combustibleKeyTyped

    private void CanPKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_CanPKeyTyped
       char c=evt.getKeyChar();
     if(!Character.isDigit(c)&c!='.'){
       
          evt.consume();
      }
    if(c=='.'&&CanP.getText().contains(".")){
          evt.consume();
      }
      
        if(CanP.getText().length() >= 8)
    {
        evt.consume();
    }
    }//GEN-LAST:event_CanPKeyTyped

    private void descKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_descKeyTyped
   char c=evt.getKeyChar();
     if(!Character.isDigit(c)&c!='.'){
       
          evt.consume();
      }
    if(c=='.'&&desc.getText().contains(".")){
          evt.consume();
      }
      
        if(desc.getText().length() >= 8)
    {
        evt.consume();
    }    }//GEN-LAST:event_descKeyTyped

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Ventas.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Ventas.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Ventas.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Ventas.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Ventas().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField CanP;
    private javax.swing.JMenuItem agregar;
    private javax.swing.JMenuItem b_cli;
    private javax.swing.JMenuItem b_emp;
    private javax.swing.JMenuItem b_fac;
    private javax.swing.JMenuItem b_pro;
    private javax.swing.JMenuItem b_ventas;
    private javax.swing.JCheckBox check;
    private javax.swing.JTextField combustible;
    private javax.swing.JTextField desc;
    private javax.swing.JMenuItem dev;
    private javax.swing.JMenu devolucion;
    private javax.swing.JTextField dire;
    private javax.swing.JMenu ea;
    private javax.swing.JMenuItem eliminar;
    private javax.swing.JLabel fecha;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton7;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenu jMenu3;
    private javax.swing.JMenu jMenu4;
    private javax.swing.JMenu jMenu5;
    private javax.swing.JMenu jMenu6;
    private javax.swing.JMenu jMenu7;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPopupMenu jPopupMenu1;
    private javax.swing.JPopupMenu jPopupMenu3;
    private javax.swing.JRadioButtonMenuItem jRadioButtonMenuItem1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JLabel loading;
    private javax.swing.JMenuItem m_cli;
    private javax.swing.JMenuItem m_emp;
    private javax.swing.JMenuItem m_pro;
    private javax.swing.JMenuItem m_prov;
    private javax.swing.JMenu modificacion;
    private rojeru_san.componentes.RSLabelFecha rSLabelFecha1;
    private rojeru_san.componentes.RSLabelHora rSLabelHora1;
    private javax.swing.JMenuItem r_cli;
    private javax.swing.JMenuItem r_em;
    private javax.swing.JMenuItem r_fa;
    private javax.swing.JLabel r_img;
    private javax.swing.JMenuItem r_pago;
    private javax.swing.JMenuItem r_pr;
    private javax.swing.JMenuItem r_pro;
    private javax.swing.JMenuItem rep;
    private javax.swing.JCheckBox sel1;
    private javax.swing.JLabel t_pag;
    private javax.swing.JLabel t_pag1;
    private javax.swing.JTable tabladatos;
    private javax.swing.JTable tabladatos1;
    private javax.swing.JTextField v_desc;
    private javax.swing.JTextField v_marc;
    private javax.swing.JTextField v_nom;
    private javax.swing.JMenuItem ven;
    private javax.swing.JMenuItem venE;
    private javax.swing.JLabel vendedor;
    private javax.swing.JLabel vueltos;
    // End of variables declaration//GEN-END:variables
}
