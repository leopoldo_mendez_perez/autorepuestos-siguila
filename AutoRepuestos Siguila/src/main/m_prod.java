/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import clases.Conexion_postgres;
import clases.Conexion_mysql;
import clases.TextPrompt;
import java.awt.Image;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Types;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.RowFilter;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;
import org.postgresql.util.PSQLException;

/**
 *
 * @author Leo
 */
public class m_prod extends javax.swing.JFrame {
              private TableRowSorter filtro;

//DECLARACION DE VARIABLE GLOBAL
public int id;
 Conexion_postgres cn=new Conexion_postgres();
 String Nombre_E,Descripcion_E,Marca_E;
  DefaultTableModel modelo=new DefaultTableModel();
 

    String ruta = null;
    public m_prod() {
        initComponents();
            TextPrompt a = new TextPrompt("Ingrese Nombre",busqueda);
            TextPrompt b = new TextPrompt("Ingrese Descripción", busqueda1);
            TextPrompt c = new TextPrompt("Ingrese Marca", busqueda2);

                      //Primera tabla
jTable1.setOpaque(false);
jScrollPane1.setOpaque(false);
jScrollPane1.getViewport().setOpaque(false);
        loading.setVisible(false);
        loading.setVisible(false);
        m_est.setText("");
        m_est.disable();
                this.setLocationRelativeTo(null);
     modelo.addColumn("Id_Prod");
modelo.addColumn("Nombre_Prod");
        modelo.addColumn("Descripcion_Prod");
         modelo.addColumn("Marca");
         modelo.addColumn("Precio_pro");
          modelo.addColumn("Precio_Ven");
          modelo.addColumn("Estado_Prod");
           modelo.addColumn("Existencia");
            jTable1.setModel(modelo);
     Mostrar_Producto();
    }
        public void borrar(DefaultTableModel tabla){
     while(tabla.getRowCount()>0){
     tabla.removeRow(0);
     }
        }
//MOSTRAR PRODUCTOS EN LA TABLA
         public void Mostrar_Producto(){
    
            String[] datos=new String[9];
           Conexion_mysql con;
  con=new Conexion_mysql(loading);
  java.sql.Connection cn=con.getConnection();
  
    String readMetaData = "CALL PRC_Cons_Prod_m_prod() ";
     try {
         cn.setAutoCommit(false); // This line must be written just after the 

         CallableStatement cs= cn.prepareCall(readMetaData);

         //cs.registerOutParameter(4, Types.REF_CURSOR);
//cs.execute();
ResultSet rs = (ResultSet) cs.executeQuery();
if (rs != null) {
    while (rs.next()) {       
     datos[0]=rs.getString(1).toUpperCase();
      datos[1]=rs.getString(2).toUpperCase();
      datos[2]=rs.getString(3).toUpperCase();
      datos[3]=rs.getString(4).toUpperCase();
      datos[4]=rs.getString(5).toUpperCase();
      datos[5]=rs.getString(6).toUpperCase();
       datos[6]=rs.getString(7).toUpperCase();
        datos[7]=rs.getString(8).toUpperCase();
//PROCESO PARA CONSULTAR IMAGEN
//TERMINACION DE PROCESO PARA CONSULTAR IMAGEN
       modelo.addRow(datos);
        

                }//end while
            }//end if
     } catch (SQLException ex) {
         Logger.getLogger(R_FACTURA.class.getName()).log(Level.SEVERE, null, ex);
     }


}
        //MODIFICAR PRODUCTO_123
         public void modificacion_Poducto(){
     Conexion_mysql con;
  con=new Conexion_mysql(loading);
  java.sql.Connection cn=con.getConnection();
  
    String sql="call PRC_modif_Producto(?,?,?,?,?,?,?)";
    try {
    
        CallableStatement cs=cn.prepareCall(sql);
        cs.setInt(1,id);
        cs.setString(2,m_produc.getText().toUpperCase());
        cs.setString(3,m_des.getText().toUpperCase());
        cs.setFloat(4, Float.parseFloat(m_pre_com.getText()));
        cs.setFloat(5, Float.parseFloat(m_pre_ven.getText()));
        cs.setString(6,m_est.getText().toUpperCase());
        cs.setString(7,m_mar.getText().toUpperCase());
                 if(cs.execute()){
           JOptionPane.showMessageDialog(null, "Registro Completado");
       }
                 cs.close();
    } catch (Exception ex) {
     Logger.getLogger(R_PROD.class.getName()).log(Level.SEVERE, null, ex);
    }
}
         //Consulta Imagen
         public void Consulta_Imagen(){
     //Variables para la imagen
InputStream is;  
      ImageIcon foto;
           Conexion_mysql con;
  con=new Conexion_mysql(loading);
  java.sql.Connection cn=con.getConnection();
  
    String readMetaData = "CALL PRC_Con_Imag_Re_Fac(?) ";
     try {
         cn.setAutoCommit(false); // This line must be written just after the 

         CallableStatement cs= cn.prepareCall(readMetaData);
         cs.setInt(1,id);
  
         //cs.registerOutParameter(2, Types.REF_CURSOR);
//cs.execute();
ResultSet rs = (ResultSet) cs.executeQuery();
if (rs != null) {
    while (rs.next()) {       
    
//PROCESO PARA CONSULTAR IMAGEN
 is=rs.getBinaryStream(1);
 try {
            BufferedImage bi=ImageIO.read(is);
            foto=new ImageIcon(bi);
            Image img=foto.getImage();
            Image newimg=img.getScaledInstance(280,280, 
java.awt.Image.SCALE_SMOOTH);
            ImageIcon newicon=new ImageIcon(newimg);
  imagenRec.setIcon(newicon);//enviarlo a un jlabel    
 } catch (IOException ex) {
            Logger.getLogger(R_FACTURA.class.getName()).log
(Level.SEVERE, null, ex);
        }
     
//TERMINACION DE PROCESO PARA CONSULTAR IMAGEN

                }//end while
            }//end if
     } catch (SQLException ex) {
         Logger.getLogger(R_FACTURA.class.getName()).log(Level.SEVERE, null, ex);
     }

 }
         //ELIMINACION  PRODUCTOS
         public void Eliminacion_Productos(){
     Conexion_mysql con;
  con=new Conexion_mysql(loading);
  java.sql.Connection cn=con.getConnection();
  
    String sql="call PRC_eliminar_Productos(?)";
    try {
    
        CallableStatement cs=cn.prepareCall(sql);
        cs.setInt(1,id);
                 if(cs.execute()){
           JOptionPane.showMessageDialog(null, "Registro Eliminado");
       }
                 cs.close();
    } catch (Exception ex) {
     Logger.getLogger(R_PROD.class.getName()).log(Level.SEVERE, null, ex);
    }
}
            //filtro por nombre de producto
   void filtro_nombre_Producto(){
          int ColumntaTabla=1;
     filtro.setRowFilter(RowFilter.regexFilter(busqueda.getText(),ColumntaTabla));
   }
     //filtro por nombre de producto
   void filtro_Descripción_Producto(){
          int ColumntaTabla=2;
     filtro.setRowFilter(RowFilter.regexFilter(busqueda1.getText(),ColumntaTabla));
   }
     //filtro por nombre de producto
   void filtro_Marca_Producto(){
          int ColumntaTabla=3;
     filtro.setRowFilter(RowFilter.regexFilter(busqueda2.getText(),ColumntaTabla));
   }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPopupMenu1 = new javax.swing.JPopupMenu();
        Modificacion = new javax.swing.JMenuItem();
        Eliminacion = new javax.swing.JMenuItem();
        buttonGroup1 = new javax.swing.ButtonGroup();
        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        busqueda = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jLabel9 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        m_produc = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        m_des = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        m_pre_com = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        loading = new javax.swing.JLabel();
        m_pre_ven = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        m_est = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        m_mar = new javax.swing.JTextField();
        imagenRec = new javax.swing.JLabel();
        lblurl = new javax.swing.JLabel();
        busqueda1 = new javax.swing.JTextField();
        busqueda2 = new javax.swing.JTextField();
        jLabel18 = new javax.swing.JLabel();
        jLabel19 = new javax.swing.JLabel();
        jButton7 = new javax.swing.JButton();
        jLabel8 = new javax.swing.JLabel();
        activo = new javax.swing.JRadioButton();
        inactivo = new javax.swing.JRadioButton();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jMenu3 = new javax.swing.JMenu();
        jMenu2 = new javax.swing.JMenu();
        jMenu4 = new javax.swing.JMenu();
        r_pr = new javax.swing.JMenuItem();
        r_em = new javax.swing.JMenuItem();
        r_fa = new javax.swing.JMenuItem();
        r_pro = new javax.swing.JMenuItem();
        r_cli = new javax.swing.JMenuItem();
        jMenu6 = new javax.swing.JMenu();
        b_pro = new javax.swing.JMenuItem();
        b_emp = new javax.swing.JMenuItem();
        b_fac = new javax.swing.JMenuItem();
        b_cli = new javax.swing.JMenuItem();
        b_ventas = new javax.swing.JMenuItem();
        modificacion = new javax.swing.JMenu();
        m_pro = new javax.swing.JMenuItem();
        m_emp = new javax.swing.JMenuItem();
        m_cli = new javax.swing.JMenuItem();
        m_prov = new javax.swing.JMenuItem();
        devolucion = new javax.swing.JMenu();
        dev = new javax.swing.JMenuItem();
        jMenu7 = new javax.swing.JMenu();
        ven = new javax.swing.JMenuItem();
        venE = new javax.swing.JMenuItem();
        ea = new javax.swing.JMenu();
        rep = new javax.swing.JMenuItem();
        jMenu5 = new javax.swing.JMenu();
        r_pago = new javax.swing.JMenuItem();

        Modificacion.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Modificar.png"))); // NOI18N
        Modificacion.setText("MODIFICACION");
        Modificacion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ModificacionActionPerformed(evt);
            }
        });
        jPopupMenu1.add(Modificacion);

        Eliminacion.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Bote de basura.png"))); // NOI18N
        Eliminacion.setText("ELIMINACIÓN");
        Eliminacion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                EliminacionActionPerformed(evt);
            }
        });
        jPopupMenu1.add(Eliminacion);

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setUndecorated(true);

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setForeground(new java.awt.Color(255, 255, 255));

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));
        jPanel2.setForeground(new java.awt.Color(255, 255, 255));
        jPanel2.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/NUEVA LUPA.png"))); // NOI18N
        jPanel2.add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(340, 150, -1, -1));

        busqueda.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        busqueda.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                busquedaKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                busquedaKeyTyped(evt);
            }
        });
        jPanel2.add(busqueda, new org.netbeans.lib.awtextra.AbsoluteConstraints(370, 150, 180, 30));

        jLabel1.setFont(new java.awt.Font("Times New Roman", 1, 48)); // NOI18N
        jLabel1.setText("Modificación de Producto");
        jPanel2.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 40, 550, 40));

        jTable1.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Codigo", "Nombre_Prod", "Descripción", " Precio_compra", "Precio_Venta", "Estado", "Cantidad"
            }
        ));
        jTable1.setComponentPopupMenu(jPopupMenu1);
        jTable1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTable1MouseClicked(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jTable1MousePressed(evt);
            }
        });
        jScrollPane1.setViewportView(jTable1);

        jPanel2.add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(340, 190, 600, 250));

        jLabel9.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        jLabel9.setText("Nombre");
        jPanel2.add(jLabel9, new org.netbeans.lib.awtextra.AbsoluteConstraints(370, 120, 120, 20));

        jLabel2.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        jLabel2.setText("NOMBRE PRODUCTO");
        jPanel2.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(950, 110, 190, 20));

        m_produc.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        m_produc.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                m_producKeyPressed(evt);
            }
        });
        jPanel2.add(m_produc, new org.netbeans.lib.awtextra.AbsoluteConstraints(950, 130, 180, 30));

        jLabel6.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        jLabel6.setText(" DESCRIPCIÓN");
        jPanel2.add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(950, 170, 140, 20));

        m_des.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        m_des.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                m_desKeyPressed(evt);
            }
        });
        jPanel2.add(m_des, new org.netbeans.lib.awtextra.AbsoluteConstraints(950, 190, 180, 30));

        jLabel4.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        jLabel4.setText("PRECIO_COMPRA");
        jPanel2.add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(950, 230, 170, 20));

        m_pre_com.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        m_pre_com.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                m_pre_comKeyPressed(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                m_pre_comKeyTyped(evt);
            }
        });
        jPanel2.add(m_pre_com, new org.netbeans.lib.awtextra.AbsoluteConstraints(950, 250, 180, 30));

        jLabel5.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        jLabel5.setText("PRECIO_VENTA");
        jPanel2.add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(950, 290, 160, 20));

        loading.setIcon(new javax.swing.ImageIcon(getClass().getResource("/main/loading_1.gif"))); // NOI18N
        jPanel2.add(loading, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 160, 310, 280));

        m_pre_ven.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        m_pre_ven.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                m_pre_venKeyPressed(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                m_pre_venKeyTyped(evt);
            }
        });
        jPanel2.add(m_pre_ven, new org.netbeans.lib.awtextra.AbsoluteConstraints(950, 310, 180, 30));

        jLabel7.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        jLabel7.setText("ESTADO");
        jPanel2.add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(950, 350, 80, 20));

        m_est.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        m_est.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                m_estKeyPressed(evt);
            }
        });
        jPanel2.add(m_est, new org.netbeans.lib.awtextra.AbsoluteConstraints(950, 370, 180, 30));

        jLabel10.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        jLabel10.setText("MARCA");
        jPanel2.add(jLabel10, new org.netbeans.lib.awtextra.AbsoluteConstraints(950, 430, 120, 20));

        m_mar.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        m_mar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                m_marKeyPressed(evt);
            }
        });
        jPanel2.add(m_mar, new org.netbeans.lib.awtextra.AbsoluteConstraints(950, 450, 180, 30));

        imagenRec.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 255, 255), 2));
        jPanel2.add(imagenRec, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 160, 290, 280));
        jPanel2.add(lblurl, new org.netbeans.lib.awtextra.AbsoluteConstraints(860, 380, 200, 20));

        busqueda1.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        busqueda1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                busqueda1KeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                busqueda1KeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                busqueda1KeyTyped(evt);
            }
        });
        jPanel2.add(busqueda1, new org.netbeans.lib.awtextra.AbsoluteConstraints(570, 150, 170, 30));

        busqueda2.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        busqueda2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                busqueda2KeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                busqueda2KeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                busqueda2KeyTyped(evt);
            }
        });
        jPanel2.add(busqueda2, new org.netbeans.lib.awtextra.AbsoluteConstraints(760, 150, 170, 30));

        jLabel18.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        jLabel18.setText("Descipcion");
        jPanel2.add(jLabel18, new org.netbeans.lib.awtextra.AbsoluteConstraints(600, 120, 110, -1));

        jLabel19.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        jLabel19.setText("Marca");
        jPanel2.add(jLabel19, new org.netbeans.lib.awtextra.AbsoluteConstraints(810, 120, 80, -1));

        jButton7.setFont(new java.awt.Font("Tahoma", 3, 24)); // NOI18N
        jButton7.setForeground(new java.awt.Color(255, 255, 255));
        jButton7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/salir.png"))); // NOI18N
        jButton7.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jButton7.setIconTextGap(1);
        jButton7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton7ActionPerformed(evt);
            }
        });
        jPanel2.add(jButton7, new org.netbeans.lib.awtextra.AbsoluteConstraints(1130, 0, 50, 40));

        jLabel8.setBackground(new java.awt.Color(255, 255, 255));
        jLabel8.setFont(new java.awt.Font("Tahoma", 3, 14)); // NOI18N
        jLabel8.setText("SALIR");
        jPanel2.add(jLabel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(1130, 40, -1, -1));

        buttonGroup1.add(activo);
        activo.setText("ACTIVO");
        activo.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                activoMouseClicked(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                activoMousePressed(evt);
            }
        });
        activo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                activoActionPerformed(evt);
            }
        });
        jPanel2.add(activo, new org.netbeans.lib.awtextra.AbsoluteConstraints(960, 400, -1, -1));

        buttonGroup1.add(inactivo);
        inactivo.setText("INACTIVO");
        inactivo.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                inactivoMouseClicked(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                inactivoMousePressed(evt);
            }
        });
        jPanel2.add(inactivo, new org.netbeans.lib.awtextra.AbsoluteConstraints(1050, 400, -1, -1));

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, 490, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        jMenuBar1.setBorder(null);
        jMenuBar1.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jMenuBar1.setFont(new java.awt.Font("Segoe UI", 2, 12)); // NOI18N

        jMenu1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/can.png"))); // NOI18N
        jMenu1.setText("LOGIN");

        jMenu3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Fondo.png"))); // NOI18N
        jMenu3.setText("LOGIN");
        jMenu3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jMenu3MouseClicked(evt);
            }
        });
        jMenu1.add(jMenu3);

        jMenuBar1.add(jMenu1);
        jMenuBar1.add(jMenu2);

        jMenu4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/add (1).png"))); // NOI18N
        jMenu4.setText(" REGISTRO");

        r_pr.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_R, java.awt.event.InputEvent.CTRL_DOWN_MASK));
        r_pr.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/ingreso.png"))); // NOI18N
        r_pr.setText("PRODUCTO");
        r_pr.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                r_prActionPerformed(evt);
            }
        });
        jMenu4.add(r_pr);

        r_em.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_E, java.awt.event.InputEvent.CTRL_DOWN_MASK));
        r_em.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/emple.png"))); // NOI18N
        r_em.setText("EMPLEADOS");
        r_em.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                r_emActionPerformed(evt);
            }
        });
        jMenu4.add(r_em);

        r_fa.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F, java.awt.event.InputEvent.CTRL_DOWN_MASK));
        r_fa.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/REGISTRO FACTURA.png"))); // NOI18N
        r_fa.setText("FACTURA");
        r_fa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                r_faActionPerformed(evt);
            }
        });
        jMenu4.add(r_fa);

        r_pro.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_P, java.awt.event.InputEvent.CTRL_DOWN_MASK));
        r_pro.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/proveNue.png"))); // NOI18N
        r_pro.setText("PROVEEDOR");
        r_pro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                r_proActionPerformed(evt);
            }
        });
        jMenu4.add(r_pro);

        r_cli.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_C, java.awt.event.InputEvent.CTRL_DOWN_MASK));
        r_cli.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/cliente.png"))); // NOI18N
        r_cli.setText("CLIENTE");
        r_cli.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                r_cliActionPerformed(evt);
            }
        });
        jMenu4.add(r_cli);

        jMenuBar1.add(jMenu4);

        jMenu6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Busqueda (1).png"))); // NOI18N
        jMenu6.setText("BUSQUEDA");

        b_pro.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_P, java.awt.event.InputEvent.ALT_DOWN_MASK));
        b_pro.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/prodddd.png"))); // NOI18N
        b_pro.setText("PRODUCTOS");
        b_pro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                b_proActionPerformed(evt);
            }
        });
        jMenu6.add(b_pro);

        b_emp.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_E, java.awt.event.InputEvent.ALT_DOWN_MASK));
        b_emp.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/busPer .png"))); // NOI18N
        b_emp.setText("EMPLEADOS");
        b_emp.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                b_empActionPerformed(evt);
            }
        });
        jMenu6.add(b_emp);

        b_fac.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F, java.awt.event.InputEvent.ALT_DOWN_MASK));
        b_fac.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/busqueda_factura.png"))); // NOI18N
        b_fac.setText("FACTURA");
        b_fac.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                b_facActionPerformed(evt);
            }
        });
        jMenu6.add(b_fac);

        b_cli.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_C, java.awt.event.InputEvent.ALT_DOWN_MASK));
        b_cli.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/bus_em.png"))); // NOI18N
        b_cli.setText("CLIENTE");
        b_cli.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                b_cliActionPerformed(evt);
            }
        });
        jMenu6.add(b_cli);

        b_ventas.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_Z, java.awt.event.InputEvent.ALT_DOWN_MASK));
        b_ventas.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Busqueda1.png"))); // NOI18N
        b_ventas.setText("VENTAS");
        b_ventas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                b_ventasActionPerformed(evt);
            }
        });
        jMenu6.add(b_ventas);

        jMenuBar1.add(jMenu6);

        modificacion.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/modificacion.png"))); // NOI18N
        modificacion.setText("MODIFICAR");

        m_pro.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_K, java.awt.event.InputEvent.ALT_DOWN_MASK));
        m_pro.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/PRODUCTO.png"))); // NOI18N
        m_pro.setText("PRODUCTO");
        m_pro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                m_proActionPerformed(evt);
            }
        });
        modificacion.add(m_pro);

        m_emp.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_E, java.awt.event.InputEvent.SHIFT_DOWN_MASK));
        m_emp.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/empleado.png"))); // NOI18N
        m_emp.setText("EMPLEADO");
        m_emp.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                m_empActionPerformed(evt);
            }
        });
        modificacion.add(m_emp);

        m_cli.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_C, java.awt.event.InputEvent.SHIFT_DOWN_MASK));
        m_cli.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/clientee.png"))); // NOI18N
        m_cli.setText("CLIENTE");
        m_cli.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                m_cliActionPerformed(evt);
            }
        });
        modificacion.add(m_cli);

        m_prov.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_P, java.awt.event.InputEvent.SHIFT_DOWN_MASK));
        m_prov.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/proveedores.png"))); // NOI18N
        m_prov.setText("PROVEEDORES");
        m_prov.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                m_provActionPerformed(evt);
            }
        });
        modificacion.add(m_prov);

        jMenuBar1.add(modificacion);

        devolucion.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/DEV.png"))); // NOI18N
        devolucion.setText("DEVOULUCION");

        dev.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_D, java.awt.event.InputEvent.CTRL_DOWN_MASK));
        dev.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/dev1.png"))); // NOI18N
        dev.setText("DEVOLUCION");
        dev.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                devActionPerformed(evt);
            }
        });
        devolucion.add(dev);

        jMenuBar1.add(devolucion);

        jMenu7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/png-transparent-shopping-cart-graphy-cart-supermarket-vehicle-shopping-bags-trolleys (1) (1).png"))); // NOI18N
        jMenu7.setText("VENTA");

        ven.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_V, java.awt.event.InputEvent.CTRL_DOWN_MASK));
        ven.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/venta a credito.png"))); // NOI18N
        ven.setText("VENTA  A CREDITO");
        ven.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                venActionPerformed(evt);
            }
        });
        jMenu7.add(ven);

        venE.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_B, java.awt.event.InputEvent.CTRL_DOWN_MASK));
        venE.setIcon(new javax.swing.ImageIcon(getClass().getResource("/main/venta efectivo.png"))); // NOI18N
        venE.setText("VENTA EN EFECTIVO");
        venE.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                venEActionPerformed(evt);
            }
        });
        jMenu7.add(venE);

        jMenuBar1.add(jMenu7);

        ea.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/REPORTE.png"))); // NOI18N
        ea.setText("REPORTE");

        rep.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_R, java.awt.event.InputEvent.ALT_DOWN_MASK));
        rep.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/reportess.png"))); // NOI18N
        rep.setText(" REPORTES");
        rep.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                repActionPerformed(evt);
            }
        });
        ea.add(rep);

        jMenuBar1.add(ea);

        jMenu5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Pago_C_P.png"))); // NOI18N
        jMenu5.setText("PAGOS");

        r_pago.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Pago_C_P (2).png"))); // NOI18N
        r_pago.setText("REALIZAR PAGOS");
        r_pago.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                r_pagoActionPerformed(evt);
            }
        });
        jMenu5.add(r_pago);

        jMenuBar1.add(jMenu5);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void m_producKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_m_producKeyPressed
if(evt.getKeyCode() == KeyEvent.VK_ENTER){
    m_des.requestFocus();
    }  
    }//GEN-LAST:event_m_producKeyPressed

    private void m_desKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_m_desKeyPressed
if(evt.getKeyCode() == KeyEvent.VK_ENTER){
   m_pre_com.requestFocus();
    }  
    }//GEN-LAST:event_m_desKeyPressed

    private void m_pre_comKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_m_pre_comKeyPressed
      if(evt.getKeyCode() == KeyEvent.VK_ENTER){
    m_pre_ven.requestFocus();
    }  
    }//GEN-LAST:event_m_pre_comKeyPressed

    private void m_pre_venKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_m_pre_venKeyPressed
    if(evt.getKeyCode() == KeyEvent.VK_ENTER){
    m_est.requestFocus();
    }  
    }//GEN-LAST:event_m_pre_venKeyPressed

    private void m_estKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_m_estKeyPressed
if(evt.getKeyCode() == KeyEvent.VK_ENTER){
    m_mar.requestFocus();
    }  
    }//GEN-LAST:event_m_estKeyPressed

    private void m_marKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_m_marKeyPressed
 
    }//GEN-LAST:event_m_marKeyPressed

    private void ModificacionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ModificacionActionPerformed
       Thread thread = new Thread() {
            public void run() {
   modificacion_Poducto();
     
 m_produc.setText("");
         m_des.setText("");
           m_pre_com.setText("");
           m_pre_ven.setText("");
           m_est.setText("");
           m_mar.setText("");
           borrar(modelo);
           Mostrar_Producto();
            }
        };

        thread.start();
     
           
    }//GEN-LAST:event_ModificacionActionPerformed

    private void EliminacionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_EliminacionActionPerformed
         Thread thread = new Thread() {
            public void run() {
  Eliminacion_Productos();
              
     /* m_produc.setText("");
         m_des.setText("");
           m_pre_com.setText("");
           m_pre_ven.setText("");
           m_est.setText("");
           m_mar.setText("");*/
      borrar(modelo);
           Mostrar_Producto();
            }
        };

        thread.start();
            
       
    }//GEN-LAST:event_EliminacionActionPerformed

    private void busquedaKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_busquedaKeyReleased
     
    }//GEN-LAST:event_busquedaKeyReleased

    private void busqueda1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_busqueda1KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_busqueda1KeyPressed

    private void busqueda1KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_busqueda1KeyReleased
        
    }//GEN-LAST:event_busqueda1KeyReleased

    private void busqueda2KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_busqueda2KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_busqueda2KeyPressed

    private void busqueda2KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_busqueda2KeyReleased

    }//GEN-LAST:event_busqueda2KeyReleased

    private void jTable1MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTable1MousePressed

    }//GEN-LAST:event_jTable1MousePressed

    private void jTable1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTable1MouseClicked
  Thread thread = new Thread() {
            public void run() {
        //PONER DATOS EN JTEXTFIELD
        int seleccion=jTable1.rowAtPoint(evt.getPoint());
        id=Integer.parseInt(String.valueOf(jTable1.getValueAt(seleccion,0)));
        Consulta_Imagen();
        m_produc.setText(String.valueOf(jTable1.getValueAt(seleccion,1)));
        m_des.setText(String.valueOf(jTable1.getValueAt(seleccion,2)));
        m_pre_com.setText(String.valueOf(jTable1.getValueAt(seleccion,4)));
        m_pre_ven.setText(String.valueOf(jTable1.getValueAt(seleccion,5)));
        m_mar.setText(String.valueOf(jTable1.getValueAt(seleccion,3)));
        m_est.setText(String.valueOf(jTable1.getValueAt(seleccion,6)));
        // TODO add your handling code here:       
            }
        };

        thread.start();

    }//GEN-LAST:event_jTable1MouseClicked

    private void jMenu3MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jMenu3MouseClicked
        Login l=new Login();
        l.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_jMenu3MouseClicked

    private void r_prActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_r_prActionPerformed
        R_PROD a=new    R_PROD();
        a.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_r_prActionPerformed

    private void r_emActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_r_emActionPerformed
        R_EMPL a=new   R_EMPL();
        a.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_r_emActionPerformed

    private void r_faActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_r_faActionPerformed
        R_FACTURA a=new R_FACTURA();
        a.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_r_faActionPerformed

    private void r_proActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_r_proActionPerformed
        R_PROVEE a=new R_PROVEE();
        a.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_r_proActionPerformed

    private void r_cliActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_r_cliActionPerformed

        R_CLI A=new R_CLI();
        A.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_r_cliActionPerformed

    private void b_proActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_b_proActionPerformed
        Busqueda_Productos a=new Busqueda_Productos();
        a.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_b_proActionPerformed

    private void b_empActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_b_empActionPerformed
        Busqueda_Empleados a=new Busqueda_Empleados();
        a.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_b_empActionPerformed

    private void b_facActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_b_facActionPerformed
        Busqueda_Facturas a=new Busqueda_Facturas();
        a.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_b_facActionPerformed

    private void b_cliActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_b_cliActionPerformed
        Busqueda_Clientes a=new Busqueda_Clientes ();
        a.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_b_cliActionPerformed

    private void b_ventasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_b_ventasActionPerformed
        Busqueda_Venta P=new  Busqueda_Venta();
        this.setVisible(false);
        P.setVisible(true);
    }//GEN-LAST:event_b_ventasActionPerformed

    private void m_proActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_m_proActionPerformed
        m_prod a=new  m_prod();
        a.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_m_proActionPerformed

    private void m_empActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_m_empActionPerformed
        m_empl a=new  m_empl ();
        a.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_m_empActionPerformed

    private void m_cliActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_m_cliActionPerformed
        m_cliente a=new  m_cliente();
        a.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_m_cliActionPerformed

    private void m_provActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_m_provActionPerformed
        m_prov a=new  m_prov ();
        a.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_m_provActionPerformed

    private void devActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_devActionPerformed
        Devolucion l=new Devolucion();
        l.setVisible(true);
          this.setVisible(false);
    }//GEN-LAST:event_devActionPerformed

    private void venActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_venActionPerformed
        Ventas_Cred a=new Ventas_Cred();
        a.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_venActionPerformed

    private void venEActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_venEActionPerformed
        Ventas a=new Ventas();
        a.setVisible(true);
        this.setVisible(false);

    }//GEN-LAST:event_venEActionPerformed

    private void repActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_repActionPerformed
  Reportes r =new Reportes();
  r.setVisible(true);
    }//GEN-LAST:event_repActionPerformed

    private void r_pagoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_r_pagoActionPerformed
        PAGO P=new PAGO();
        this.setVisible(false);
        P.setVisible(true);
    }//GEN-LAST:event_r_pagoActionPerformed

    private void jButton7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton7ActionPerformed
        System.exit(0);
    }//GEN-LAST:event_jButton7ActionPerformed

    private void activoMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_activoMousePressed
    if(activo.isSelected()){ 
    m_est.setText(activo.getText());
    }
    }//GEN-LAST:event_activoMousePressed

    private void activoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_activoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_activoActionPerformed

    private void inactivoMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_inactivoMousePressed
   
    if(inactivo.isSelected())
    m_est.setText(inactivo.getText());
        
    }//GEN-LAST:event_inactivoMousePressed

    private void activoMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_activoMouseClicked
    if(activo.isSelected()){ 
    m_est.setText(activo.getText());
    }
    }//GEN-LAST:event_activoMouseClicked

    private void inactivoMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_inactivoMouseClicked
   if(inactivo.isSelected())
    m_est.setText(inactivo.getText());
     
    
    }//GEN-LAST:event_inactivoMouseClicked

    private void busquedaKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_busquedaKeyTyped
        //borrar(modelo1);
    busqueda.addKeyListener(new KeyAdapter(){
    public void keyReleased(final KeyEvent e){
    String cadena=(busqueda.getText().toUpperCase());
    busqueda.setText(cadena);
    filtro_nombre_Producto();
    }
   
    
   });
   filtro=new TableRowSorter(jTable1.getModel());
   jTable1.setRowSorter(filtro);

    }//GEN-LAST:event_busquedaKeyTyped

    private void busqueda1KeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_busqueda1KeyTyped
        busqueda1.addKeyListener(new KeyAdapter(){
    public void keyReleased(final KeyEvent e){
    String cadena=(busqueda1.getText().toUpperCase());
    busqueda1.setText(cadena);
    filtro_Descripción_Producto();
    }
   
    
   });
   filtro=new TableRowSorter(jTable1.getModel());
   jTable1.setRowSorter(filtro);
    }//GEN-LAST:event_busqueda1KeyTyped

    private void busqueda2KeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_busqueda2KeyTyped
       
         busqueda2.addKeyListener(new KeyAdapter(){
    public void keyReleased(final KeyEvent e){
    String cadena=(busqueda2.getText().toUpperCase());
    busqueda2.setText(cadena);
    filtro_Marca_Producto();
    }
   
    
   });
   filtro=new TableRowSorter(jTable1.getModel());
   jTable1.setRowSorter(filtro);
    }//GEN-LAST:event_busqueda2KeyTyped

    private void m_pre_comKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_m_pre_comKeyTyped
        char c=evt.getKeyChar();
     if(!Character.isDigit(c)&c!='.'){
       
          evt.consume();
      }
    if(c=='.'&&m_pre_com.getText().contains(".")){
          evt.consume();
      }
      
        if(m_pre_com.getText().length() >= 8)
    {
        evt.consume();
    }
    }//GEN-LAST:event_m_pre_comKeyTyped

    private void m_pre_venKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_m_pre_venKeyTyped
            char c=evt.getKeyChar();
     if(!Character.isDigit(c)&c!='.'){
       
          evt.consume();
      }
    if(c=='.'&&m_pre_ven.getText().contains(".")){
          evt.consume();
      }
      
        if(m_pre_ven.getText().length() >= 8)
    {
        evt.consume();
    }
    }//GEN-LAST:event_m_pre_venKeyTyped

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(m_prod.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(m_prod.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(m_prod.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(m_prod.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new m_prod().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenuItem Eliminacion;
    private javax.swing.JMenuItem Modificacion;
    private javax.swing.JRadioButton activo;
    private javax.swing.JMenuItem b_cli;
    private javax.swing.JMenuItem b_emp;
    private javax.swing.JMenuItem b_fac;
    private javax.swing.JMenuItem b_pro;
    private javax.swing.JMenuItem b_ventas;
    private javax.swing.JTextField busqueda;
    private javax.swing.JTextField busqueda1;
    private javax.swing.JTextField busqueda2;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JMenuItem dev;
    private javax.swing.JMenu devolucion;
    private javax.swing.JMenu ea;
    private javax.swing.JLabel imagenRec;
    private javax.swing.JRadioButton inactivo;
    private javax.swing.JButton jButton7;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenu jMenu3;
    private javax.swing.JMenu jMenu4;
    private javax.swing.JMenu jMenu5;
    private javax.swing.JMenu jMenu6;
    private javax.swing.JMenu jMenu7;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPopupMenu jPopupMenu1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTable1;
    private javax.swing.JLabel lblurl;
    private javax.swing.JLabel loading;
    private javax.swing.JMenuItem m_cli;
    private javax.swing.JTextField m_des;
    private javax.swing.JMenuItem m_emp;
    private javax.swing.JTextField m_est;
    private javax.swing.JTextField m_mar;
    private javax.swing.JTextField m_pre_com;
    private javax.swing.JTextField m_pre_ven;
    private javax.swing.JMenuItem m_pro;
    private javax.swing.JTextField m_produc;
    private javax.swing.JMenuItem m_prov;
    private javax.swing.JMenu modificacion;
    private javax.swing.JMenuItem r_cli;
    private javax.swing.JMenuItem r_em;
    private javax.swing.JMenuItem r_fa;
    private javax.swing.JMenuItem r_pago;
    private javax.swing.JMenuItem r_pr;
    private javax.swing.JMenuItem r_pro;
    private javax.swing.JMenuItem rep;
    private javax.swing.JMenuItem ven;
    private javax.swing.JMenuItem venE;
    // End of variables declaration//GEN-END:variables
}
