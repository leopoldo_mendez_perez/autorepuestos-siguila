/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import clases.TextPrompt;
import clases.Conexion_postgres;
import clases.Activacion;
import clases.Conexion_mysql;
import java.awt.Image;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.sql.CallableStatement;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.RowFilter;
 import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;

/**
 *
 * @author Dennis Antony y Leo
 */
public class Busqueda_Facturas extends javax.swing.JFrame {
    String id;
    int id1;//id para buscar imagen
          Conexion_postgres cn=new Conexion_postgres();
          private TableRowSorter filtro;
                      DefaultTableModel modelo1=new DefaultTableModel();
                      DefaultTableModel modelo2=new DefaultTableModel();
    String Numero_s,Fecha_s;
    public Busqueda_Facturas() {
        initComponents();
                TextPrompt p = new TextPrompt("Ingrese Fecha", Fecha);

                //Primera tabla
tabladatos1.setOpaque(false);
jScrollPane1.setOpaque(false);
jScrollPane1.getViewport().setOpaque(false);
        this.setLocationRelativeTo(null);
                //Primera tabla
jTable1.setOpaque(true);
jScrollPane2.setOpaque(false);
jScrollPane2.getViewport().setOpaque(false);
        this.setLocationRelativeTo(null);
        
                jTable1.setOpaque(false);
        jTable1.setOpaque(false);
        jScrollPane1.getViewport().setOpaque(false);
                    tabladatos1.setOpaque(false);
        tabladatos1.setOpaque(false);
        jScrollPane2.getViewport().setOpaque(false);
        loading.setVisible(false);
        Conexion_postgres cn=new Conexion_postgres();
         TextPrompt r=new TextPrompt("Ingrese Número Factura",Numero);
            modelo1.addColumn("Codigo");
            modelo1.addColumn("Numero_Factura");
            modelo1.addColumn("Fecha de Ingreso");
            modelo1.addColumn("Proveedor");
            modelo1.addColumn("Total");
            modelo1.addColumn("Abono");
            modelo1.addColumn("Pendiente");
            modelo1.addColumn("Receptor Factura");
            modelo1.addColumn("Vendedor");
 jTable1.setModel(modelo1);
 //Tabla detalles
            modelo2.addColumn("Codigo");
            modelo2.addColumn("Codigo_Factura");
             modelo2.addColumn("Codigo Producto");
            modelo2.addColumn("Productos");
            modelo2.addColumn("Descripcion"); 
             modelo2.addColumn("Precio"); 
             modelo2.addColumn("Marca");
              modelo2.addColumn("Cantidad");
             

 tabladatos1.setModel(modelo2);

        this.setLocationRelativeTo(null);
//ACTIVACION
    r_pr.setEnabled(false);
        r_pro.setEnabled(false);
        r_em.setEnabled(false);
        r_fa.setEnabled(false);
        r_cli.setEnabled(false);
        b_pro.setEnabled(false);
        b_emp.setEnabled(false);
        b_fac.setEnabled(false);
        b_cli.setEnabled(false);
      ven.setEnabled(false);
        venE.setEnabled(false);
   rep.setEnabled(false);
      dev.setEnabled(false);
        m_pro.setEnabled(false);
  m_emp.setEnabled(false);
    m_cli.setEnabled(false);
       m_prov.setEnabled(false);
        b_ventas.setEnabled(false);
       r_pago.setEnabled(false);
        Activacion b=new Activacion();
        if(b.ver==1){
            b.entrante(r_pr,r_pro,r_em,r_fa,r_cli,b_pro,b_emp,b_fac,b_cli,modificacion,devolucion,ven,ea,dev,m_pro,
                    m_emp,m_cli,m_prov,rep,venE, b_ventas,r_pago);
        }
           if(b.ver==2){
            b.en(ven,venE,r_fa,b_pro,r_pr,r_pro,r_cli,r_pago,dev,b_cli,b_fac);
        }
           Mostrar_Factura();
    }

   public void filtro_fac(){
       int ColumntaTabla=1;
     filtro.setRowFilter(RowFilter.regexFilter(Numero.getText(),ColumntaTabla));
   }
     public void filtro_fac1(){
       int ColumntaTabla=2;
     filtro.setRowFilter(RowFilter.regexFilter(Fecha.getText(),ColumntaTabla));
   }
  //MOSTRAR FACTURA
         public void Mostrar_Factura(){
    
            String[] datos=new String[10];
          Conexion_mysql con;
    con=new Conexion_mysql(loading);
  java.sql.Connection cn=con.getConnection();
    String readMetaData = "CALL PRC_Cons_Factura() ";
     try {
         cn.setAutoCommit(false); // This line must be written just after the 

         CallableStatement cs= cn.prepareCall(readMetaData);
         //cs.setString(1,Numero.getText());
         //cs.setString(2,Fecha_s);
      //cs.registerOutParameter(3, Types.REF_CURSOR);
//cs.execute();
ResultSet rs = (ResultSet) cs.executeQuery();
if (rs != null) {
    while (rs.next()) {       
     datos[0]=rs.getString(1);
      datos[1]=rs.getString(2);
      datos[2]=rs.getString(3);
      datos[3]=rs.getString(4).toUpperCase();
      datos[4]=rs.getString(5);
       datos[5]=rs.getString(6);
      datos[6]=rs.getString(7);
    
     
       modelo1.addRow(datos);
        

                }//end while
            }//end if
cs.close();
rs.close();
     } catch (SQLException ex) {
System.out.println("Herror"+ex);
     }


}
         //borrar datos tabla
          public void borrar(DefaultTableModel tabla){
     while(tabla.getRowCount()>0){
     tabla.removeRow(0);
     }
        }
          //Extraer datos del calendario
          public void calendario(){
          
    try {
        //jDateChooser el nombre la variable  del componente jdatecgooser
        //java.util.Date  fecha= fechaJ.getDate();
        //DateFormat f=new SimpleDateFormat("yyyyMMdd");
       //Fecha_s=f.format(fecha);
     
 
        //textFecha nombre de la variable del componenten jtextfiel
     
    } catch (Exception e) {
    }
          }
           //MOSTRAR DATOS EN LA TABLA
         public void mostrar_detalle(){
    
            String[] datos=new String[9];
  
            Conexion_mysql con;
    con=new Conexion_mysql(loading);
  java.sql.Connection cn=con.getConnection();
  
    String readMetaData = "CALL  PRC_Cons_Detalle_Busqueda(?) ";
     try {
         cn.setAutoCommit(false); // This line must be written just after the 

         CallableStatement cs= cn.prepareCall(readMetaData);
         cs.setString(1,id);
      
         //cs.registerOutParameter(2, Types.REF_CURSOR);
//cs.execute();
ResultSet rs = (ResultSet) cs.executeQuery();
if (rs != null) {
    while (rs.next()) {       
     datos[0]=rs.getString(1).toUpperCase();
      datos[1]=rs.getString(2).toUpperCase();
      datos[2]=rs.getString(3).toUpperCase();
      datos[3]=rs.getString(4).toUpperCase();
      datos[4]=rs.getString(5).toUpperCase();
      datos[5]=rs.getString(6).toUpperCase();
      datos[6]=rs.getString(7).toUpperCase();
      datos[7]=rs.getString(8).toUpperCase();
       modelo2.addRow(datos);
        

                }//end while
            }//end if
cs.close();
rs.close();
     } catch (SQLException ex) {
         Logger.getLogger(R_FACTURA.class.getName()).log(Level.SEVERE, null, ex);
     }


}
         //Consulta Imgagen
          public void Consulta_Imagen(){
     //Variables para la imagen
InputStream is;  
      ImageIcon foto;
      
           Conexion_mysql con;
    con=new Conexion_mysql(loading);
  java.sql.Connection cn=con.getConnection();
  
    String readMetaData = "CALL PRC_Con_Imag_Re_Fac(?) ";
     try {
         cn.setAutoCommit(false); // This line must be written just after the 

         CallableStatement cs= cn.prepareCall(readMetaData);
         cs.setInt(1,id1);
  
         //cs.registerOutParameter(2, Types.REF_CURSOR);
//cs.execute();
ResultSet rs = (ResultSet) cs.executeQuery();
if (rs != null) {
    while (rs.next()) {       
    
//PROCESO PARA CONSULTAR IMAGEN
 is=rs.getBinaryStream(1);
 try {
            BufferedImage bi=ImageIO.read(is);
            foto=new ImageIcon(bi);
            Image img=foto.getImage();
            Image newimg=img.getScaledInstance(190,190, 
java.awt.Image.SCALE_SMOOTH);
            ImageIcon newicon=new ImageIcon(newimg);
  rec_img.setIcon(newicon);//enviarlo a un jlabel    
 } catch (IOException ex) {
            Logger.getLogger(R_FACTURA.class.getName()).log
(Level.SEVERE, null, ex);
        }
     
//TERMINACION DE PROCESO PARA CONSULTAR IMAGEN

                }//end while
            }//end if
rs.close();
cs.close();
     } catch (SQLException ex) {
         Logger.getLogger(R_FACTURA.class.getName()).log(Level.SEVERE, null, ex);
     }

 }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        loading = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        Numero = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jLabel1 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tabladatos1 = new javax.swing.JTable();
        rec_img = new javax.swing.JLabel();
        jButton6 = new javax.swing.JButton();
        jLabel5 = new javax.swing.JLabel();
        Fecha = new javax.swing.JTextField();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jMenu3 = new javax.swing.JMenu();
        jMenu2 = new javax.swing.JMenu();
        jMenu4 = new javax.swing.JMenu();
        r_pr = new javax.swing.JMenuItem();
        r_em = new javax.swing.JMenuItem();
        r_fa = new javax.swing.JMenuItem();
        r_pro = new javax.swing.JMenuItem();
        r_cli = new javax.swing.JMenuItem();
        jMenu6 = new javax.swing.JMenu();
        b_pro = new javax.swing.JMenuItem();
        b_emp = new javax.swing.JMenuItem();
        b_fac = new javax.swing.JMenuItem();
        b_cli = new javax.swing.JMenuItem();
        b_ventas = new javax.swing.JMenuItem();
        modificacion = new javax.swing.JMenu();
        m_pro = new javax.swing.JMenuItem();
        m_emp = new javax.swing.JMenuItem();
        m_cli = new javax.swing.JMenuItem();
        m_prov = new javax.swing.JMenuItem();
        devolucion = new javax.swing.JMenu();
        dev = new javax.swing.JMenuItem();
        jMenu7 = new javax.swing.JMenu();
        ven = new javax.swing.JMenuItem();
        venE = new javax.swing.JMenuItem();
        ea = new javax.swing.JMenu();
        rep = new javax.swing.JMenuItem();
        jMenu5 = new javax.swing.JMenu();
        r_pago = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("BUSCAR FACTURA");
        setUndecorated(true);

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setMinimumSize(new java.awt.Dimension(500, 355));
        jPanel1.setLayout(null);
        jPanel1.add(jLabel2);
        jLabel2.setBounds(32, 52, 0, 30);

        loading.setIcon(new javax.swing.ImageIcon(getClass().getResource("/main/loading_1.gif"))); // NOI18N
        jPanel1.add(loading);
        loading.setBounds(440, 160, 230, 110);

        jPanel3.setBackground(new java.awt.Color(255, 255, 255));
        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)), "FACTURAS", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 3, 18))); // NOI18N
        jPanel3.setForeground(new java.awt.Color(255, 255, 255));
        jPanel3.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        Numero.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        Numero.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                NumeroKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                NumeroKeyTyped(evt);
            }
        });
        jPanel3.add(Numero, new org.netbeans.lib.awtextra.AbsoluteConstraints(590, 20, 300, 40));

        jTable1.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Numero Factura", "Fecha", "Total", "Abono", "Pendiente"
            }
        ));
        jTable1.setSelectionBackground(new java.awt.Color(51, 204, 255));
        jTable1.setSelectionForeground(new java.awt.Color(0, 0, 51));
        jTable1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTable1MouseClicked(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jTable1MousePressed(evt);
            }
        });
        jScrollPane1.setViewportView(jTable1);

        jPanel3.add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 70, 1190, 230));

        jLabel1.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        jLabel1.setText("Fecha");
        jPanel3.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(300, 20, 80, 40));

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));
        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createEtchedBorder(new java.awt.Color(0, 255, 255), new java.awt.Color(255, 0, 0)), "DETALLE FACTURA", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 3, 18))); // NOI18N
        jPanel2.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        tabladatos1.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        tabladatos1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tabladatos1.setSelectionBackground(new java.awt.Color(51, 204, 255));
        tabladatos1.setSelectionForeground(new java.awt.Color(0, 0, 51));
        tabladatos1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tabladatos1MouseClicked(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                tabladatos1MousePressed(evt);
            }
        });
        tabladatos1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tabladatos1KeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                tabladatos1KeyReleased(evt);
            }
        });
        jScrollPane2.setViewportView(tabladatos1);

        jPanel2.add(jScrollPane2, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 50, 970, 190));
        jPanel2.add(rec_img, new org.netbeans.lib.awtextra.AbsoluteConstraints(990, 50, 190, 190));

        jPanel3.add(jPanel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 300, 1190, 300));

        jButton6.setFont(new java.awt.Font("Tahoma", 3, 24)); // NOI18N
        jButton6.setForeground(new java.awt.Color(255, 255, 255));
        jButton6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/salir.png"))); // NOI18N
        jButton6.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jButton6.setIconTextGap(1);
        jButton6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton6ActionPerformed(evt);
            }
        });
        jPanel3.add(jButton6, new org.netbeans.lib.awtextra.AbsoluteConstraints(1130, 20, 70, 40));

        jLabel5.setBackground(new java.awt.Color(255, 255, 255));
        jLabel5.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        jLabel5.setText("SALIR");
        jPanel3.add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(1140, 50, -1, 30));

        Fecha.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        Fecha.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                FechaKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                FechaKeyTyped(evt);
            }
        });
        jPanel3.add(Fecha, new org.netbeans.lib.awtextra.AbsoluteConstraints(370, 20, 210, 40));

        jPanel1.add(jPanel3);
        jPanel3.setBounds(20, 0, 1210, 610);

        jMenuBar1.setBorder(null);
        jMenuBar1.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jMenuBar1.setFont(new java.awt.Font("Segoe UI", 2, 12)); // NOI18N

        jMenu1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/can.png"))); // NOI18N
        jMenu1.setText("LOGIN");

        jMenu3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Fondo.png"))); // NOI18N
        jMenu3.setText("LOGIN");
        jMenu3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jMenu3MouseClicked(evt);
            }
        });
        jMenu1.add(jMenu3);

        jMenuBar1.add(jMenu1);
        jMenuBar1.add(jMenu2);

        jMenu4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/add (1).png"))); // NOI18N
        jMenu4.setText(" REGISTRO");

        r_pr.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_R, java.awt.event.InputEvent.CTRL_DOWN_MASK));
        r_pr.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/ingreso.png"))); // NOI18N
        r_pr.setText("PRODUCTO");
        r_pr.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                r_prActionPerformed(evt);
            }
        });
        jMenu4.add(r_pr);

        r_em.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_E, java.awt.event.InputEvent.CTRL_DOWN_MASK));
        r_em.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/emple.png"))); // NOI18N
        r_em.setText("EMPLEADOS");
        r_em.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                r_emActionPerformed(evt);
            }
        });
        jMenu4.add(r_em);

        r_fa.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F, java.awt.event.InputEvent.CTRL_DOWN_MASK));
        r_fa.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/REGISTRO FACTURA.png"))); // NOI18N
        r_fa.setText("FACTURA");
        r_fa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                r_faActionPerformed(evt);
            }
        });
        jMenu4.add(r_fa);

        r_pro.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_P, java.awt.event.InputEvent.CTRL_DOWN_MASK));
        r_pro.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/proveNue.png"))); // NOI18N
        r_pro.setText("PROVEEDOR");
        r_pro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                r_proActionPerformed(evt);
            }
        });
        jMenu4.add(r_pro);

        r_cli.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_C, java.awt.event.InputEvent.CTRL_DOWN_MASK));
        r_cli.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/cliente.png"))); // NOI18N
        r_cli.setText("CLIENTE");
        r_cli.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                r_cliActionPerformed(evt);
            }
        });
        jMenu4.add(r_cli);

        jMenuBar1.add(jMenu4);

        jMenu6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Busqueda (1).png"))); // NOI18N
        jMenu6.setText("BUSQUEDA");

        b_pro.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_P, java.awt.event.InputEvent.ALT_DOWN_MASK));
        b_pro.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/prodddd.png"))); // NOI18N
        b_pro.setText("PRODUCTOS");
        b_pro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                b_proActionPerformed(evt);
            }
        });
        jMenu6.add(b_pro);

        b_emp.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_E, java.awt.event.InputEvent.ALT_DOWN_MASK));
        b_emp.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/busPer .png"))); // NOI18N
        b_emp.setText("EMPLEADOS");
        b_emp.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                b_empActionPerformed(evt);
            }
        });
        jMenu6.add(b_emp);

        b_fac.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F, java.awt.event.InputEvent.ALT_DOWN_MASK));
        b_fac.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/busqueda_factura.png"))); // NOI18N
        b_fac.setText("FACTURA");
        b_fac.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                b_facActionPerformed(evt);
            }
        });
        jMenu6.add(b_fac);

        b_cli.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_C, java.awt.event.InputEvent.ALT_DOWN_MASK));
        b_cli.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/bus_em.png"))); // NOI18N
        b_cli.setText("CLIENTE");
        b_cli.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                b_cliActionPerformed(evt);
            }
        });
        jMenu6.add(b_cli);

        b_ventas.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_Z, java.awt.event.InputEvent.ALT_DOWN_MASK));
        b_ventas.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Busqueda1.png"))); // NOI18N
        b_ventas.setText("VENTAS");
        b_ventas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                b_ventasActionPerformed(evt);
            }
        });
        jMenu6.add(b_ventas);

        jMenuBar1.add(jMenu6);

        modificacion.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/modificacion.png"))); // NOI18N
        modificacion.setText("MODIFICAR");

        m_pro.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_K, java.awt.event.InputEvent.ALT_DOWN_MASK));
        m_pro.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/PRODUCTO.png"))); // NOI18N
        m_pro.setText("PRODUCTO");
        m_pro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                m_proActionPerformed(evt);
            }
        });
        modificacion.add(m_pro);

        m_emp.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_E, java.awt.event.InputEvent.SHIFT_DOWN_MASK));
        m_emp.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/empleado.png"))); // NOI18N
        m_emp.setText("EMPLEADO");
        m_emp.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                m_empActionPerformed(evt);
            }
        });
        modificacion.add(m_emp);

        m_cli.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_C, java.awt.event.InputEvent.SHIFT_DOWN_MASK));
        m_cli.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/clientee.png"))); // NOI18N
        m_cli.setText("CLIENTE");
        m_cli.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                m_cliActionPerformed(evt);
            }
        });
        modificacion.add(m_cli);

        m_prov.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_P, java.awt.event.InputEvent.SHIFT_DOWN_MASK));
        m_prov.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/proveedores.png"))); // NOI18N
        m_prov.setText("PROVEEDORES");
        m_prov.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                m_provActionPerformed(evt);
            }
        });
        modificacion.add(m_prov);

        jMenuBar1.add(modificacion);

        devolucion.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/DEV.png"))); // NOI18N
        devolucion.setText("DEVOULUCION");

        dev.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_D, java.awt.event.InputEvent.CTRL_DOWN_MASK));
        dev.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/dev1.png"))); // NOI18N
        dev.setText("DEVOLUCION");
        dev.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                devActionPerformed(evt);
            }
        });
        devolucion.add(dev);

        jMenuBar1.add(devolucion);

        jMenu7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/png-transparent-shopping-cart-graphy-cart-supermarket-vehicle-shopping-bags-trolleys (1) (1).png"))); // NOI18N
        jMenu7.setText("VENTA");

        ven.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_V, java.awt.event.InputEvent.CTRL_DOWN_MASK));
        ven.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/venta a credito.png"))); // NOI18N
        ven.setText("VENTA  A CREDITO");
        ven.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                venActionPerformed(evt);
            }
        });
        jMenu7.add(ven);

        venE.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_B, java.awt.event.InputEvent.CTRL_DOWN_MASK));
        venE.setIcon(new javax.swing.ImageIcon(getClass().getResource("/main/venta efectivo.png"))); // NOI18N
        venE.setText("VENTA EN EFECTIVO");
        venE.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                venEActionPerformed(evt);
            }
        });
        jMenu7.add(venE);

        jMenuBar1.add(jMenu7);

        ea.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/REPORTE.png"))); // NOI18N
        ea.setText("REPORTE");

        rep.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_R, java.awt.event.InputEvent.ALT_DOWN_MASK));
        rep.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/reportess.png"))); // NOI18N
        rep.setText(" REPORTES");
        rep.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                repActionPerformed(evt);
            }
        });
        ea.add(rep);

        jMenuBar1.add(ea);

        jMenu5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Pago_C_P.png"))); // NOI18N
        jMenu5.setText("PAGOS");

        r_pago.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Pago_C_P (2).png"))); // NOI18N
        r_pago.setText("REALIZAR PAGOS");
        r_pago.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                r_pagoActionPerformed(evt);
            }
        });
        jMenu5.add(r_pago);

        jMenuBar1.add(jMenu5);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 1230, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 625, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void NumeroKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_NumeroKeyReleased

//Mostrar_Factura();        // TODO add your handling code here:
    }//GEN-LAST:event_NumeroKeyReleased

    private void jTable1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTable1MouseClicked

    }//GEN-LAST:event_jTable1MouseClicked

    private void tabladatos1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tabladatos1MouseClicked
  
    }//GEN-LAST:event_tabladatos1MouseClicked

    private void tabladatos1MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tabladatos1MousePressed
 Thread thread = new Thread() {
            public void run() {
        int seleccion=tabladatos1.rowAtPoint(evt.getPoint());
       id1=Integer.parseInt(String.valueOf(tabladatos1.getValueAt(seleccion,2)));        // TODO add your handling code here:
       Consulta_Imagen();
        }
        };

        thread.start();
    }//GEN-LAST:event_tabladatos1MousePressed

    private void jTable1MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTable1MousePressed
 Thread thread = new Thread() {
            public void run() {
        borrar(modelo2);
        int seleccion=jTable1.rowAtPoint(evt.getPoint());
       id=String.valueOf(jTable1.getValueAt(seleccion,1));        // TODO add your handling code here:
       rec_img.setIcon(null);
       mostrar_detalle();
         }
        };

        thread.start();
    }//GEN-LAST:event_jTable1MousePressed

    private void tabladatos1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tabladatos1KeyPressed
  
    }//GEN-LAST:event_tabladatos1KeyPressed

    private void tabladatos1KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tabladatos1KeyReleased
     
       
    }//GEN-LAST:event_tabladatos1KeyReleased

    private void jMenu3MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jMenu3MouseClicked
        Login l=new Login();
        l.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_jMenu3MouseClicked

    private void r_prActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_r_prActionPerformed
        R_PROD a=new    R_PROD();
        a.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_r_prActionPerformed

    private void r_emActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_r_emActionPerformed
        R_EMPL a=new   R_EMPL();
        a.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_r_emActionPerformed

    private void r_faActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_r_faActionPerformed
        R_FACTURA a=new R_FACTURA();
        a.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_r_faActionPerformed

    private void r_proActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_r_proActionPerformed
        R_PROVEE a=new R_PROVEE();
        a.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_r_proActionPerformed

    private void r_cliActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_r_cliActionPerformed

        R_CLI A=new R_CLI();
        A.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_r_cliActionPerformed

    private void b_proActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_b_proActionPerformed
        Busqueda_Productos a=new Busqueda_Productos();
        a.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_b_proActionPerformed

    private void b_empActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_b_empActionPerformed
        Busqueda_Empleados a=new Busqueda_Empleados();
        a.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_b_empActionPerformed

    private void b_facActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_b_facActionPerformed
        Busqueda_Facturas a=new Busqueda_Facturas();
        a.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_b_facActionPerformed

    private void b_cliActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_b_cliActionPerformed
        Busqueda_Clientes a=new Busqueda_Clientes ();
        a.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_b_cliActionPerformed

    private void b_ventasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_b_ventasActionPerformed
        Busqueda_Venta P=new  Busqueda_Venta();
        this.setVisible(false);
        P.setVisible(true);
    }//GEN-LAST:event_b_ventasActionPerformed

    private void m_proActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_m_proActionPerformed
        m_prod a=new  m_prod();
        a.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_m_proActionPerformed

    private void m_empActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_m_empActionPerformed
        m_empl a=new  m_empl ();
        a.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_m_empActionPerformed

    private void m_cliActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_m_cliActionPerformed
        m_cliente a=new  m_cliente();
        a.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_m_cliActionPerformed

    private void m_provActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_m_provActionPerformed
        m_prov a=new  m_prov ();
        a.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_m_provActionPerformed

    private void devActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_devActionPerformed
        Devolucion l=new Devolucion();
        l.setVisible(true);
          this.setVisible(false);
    }//GEN-LAST:event_devActionPerformed

    private void venActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_venActionPerformed
        Ventas_Cred a=new Ventas_Cred();
        a.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_venActionPerformed

    private void venEActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_venEActionPerformed
        Ventas a=new Ventas();
        a.setVisible(true);
        this.setVisible(false);

    }//GEN-LAST:event_venEActionPerformed

    private void repActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_repActionPerformed
  Reportes r =new Reportes();
  r.setVisible(true);
  this.setVisible(false);
    }//GEN-LAST:event_repActionPerformed

    private void r_pagoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_r_pagoActionPerformed
        PAGO P=new PAGO();
        this.setVisible(false);
        P.setVisible(true);
    }//GEN-LAST:event_r_pagoActionPerformed

    private void jButton6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton6ActionPerformed
        System.exit(0);
    }//GEN-LAST:event_jButton6ActionPerformed

    private void NumeroKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_NumeroKeyTyped
     //borrar(modelo1);
    Numero.addKeyListener(new KeyAdapter(){
    public void keyReleased(final KeyEvent e){
    String cadena=(Numero.getText().toUpperCase());
    Numero.setText(cadena);
    filtro_fac();}
   
    
   });
   filtro=new TableRowSorter(jTable1.getModel());
   jTable1.setRowSorter(filtro);
    }//GEN-LAST:event_NumeroKeyTyped

    private void FechaKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_FechaKeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_FechaKeyReleased

    private void FechaKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_FechaKeyTyped
Fecha.addKeyListener(new KeyAdapter(){
    public void keyReleased(final KeyEvent e){
    String cadena=(Fecha.getText().toUpperCase());
    Fecha.setText(cadena);
    filtro_fac1();}
   
    
   });
   filtro=new TableRowSorter(jTable1.getModel());
   jTable1.setRowSorter(filtro);
    }//GEN-LAST:event_FechaKeyTyped

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Busqueda_Facturas.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Busqueda_Facturas.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Busqueda_Facturas.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Busqueda_Facturas.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Busqueda_Facturas().setVisible(true);
               
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField Fecha;
    private javax.swing.JTextField Numero;
    private javax.swing.JMenuItem b_cli;
    private javax.swing.JMenuItem b_emp;
    private javax.swing.JMenuItem b_fac;
    private javax.swing.JMenuItem b_pro;
    private javax.swing.JMenuItem b_ventas;
    private javax.swing.JMenuItem dev;
    private javax.swing.JMenu devolucion;
    private javax.swing.JMenu ea;
    private javax.swing.JButton jButton6;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenu jMenu3;
    private javax.swing.JMenu jMenu4;
    private javax.swing.JMenu jMenu5;
    private javax.swing.JMenu jMenu6;
    private javax.swing.JMenu jMenu7;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTable jTable1;
    private javax.swing.JLabel loading;
    private javax.swing.JMenuItem m_cli;
    private javax.swing.JMenuItem m_emp;
    private javax.swing.JMenuItem m_pro;
    private javax.swing.JMenuItem m_prov;
    private javax.swing.JMenu modificacion;
    private javax.swing.JMenuItem r_cli;
    private javax.swing.JMenuItem r_em;
    private javax.swing.JMenuItem r_fa;
    private javax.swing.JMenuItem r_pago;
    private javax.swing.JMenuItem r_pr;
    private javax.swing.JMenuItem r_pro;
    private javax.swing.JLabel rec_img;
    private javax.swing.JMenuItem rep;
    private javax.swing.JTable tabladatos1;
    private javax.swing.JMenuItem ven;
    private javax.swing.JMenuItem venE;
    // End of variables declaration//GEN-END:variables
}
