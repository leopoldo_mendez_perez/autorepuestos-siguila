/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import br.com.adilson.util.Extenso;
import br.com.adilson.util.PrinterMatrix;
import clases.Imp_fac;
import clases.Conexion_postgres;
import clases.Conexion_mysql;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.print.PageFormat;
import java.awt.print.Printable;
import static java.awt.print.Printable.NO_SUCH_PAGE;
import static java.awt.print.Printable.PAGE_EXISTS;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.print.Doc;
import javax.print.DocFlavor;
import javax.print.DocPrintJob;
import javax.print.PrintService;
import javax.print.PrintServiceLookup;
import javax.print.SimpleDoc;
import javax.print.attribute.HashPrintRequestAttributeSet;
import javax.print.attribute.PrintRequestAttributeSet;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.WindowConstants;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Leo
 */
public class Comprobante extends javax.swing.JFrame implements Printable {
    Imp_fac f=new Imp_fac();
    DefaultTableModel modelo2=new DefaultTableModel();
    Conexion_postgres cn=new Conexion_postgres();
    public Comprobante() {
        initComponents();
                //Primera tabla
tabladatos1.setOpaque(false);
jScrollPane1.setOpaque(false);
jScrollPane1.getViewport().setOpaque(false);


        this.setLocationRelativeTo(null);
        num_factura1.setText(Integer.toString(f.getId_venta()));
        cliente.setText(f.getNombre());
        vendedor.setText(f.getEmpleado());
        tot_sin_des.setText(f.getTotal());
        descuento.setText(f.getDescuento());
        tot_con_desc.setText(f.getTot_con_desc());
        //creo la columan de mi tabla
      modelo2.addColumn("Codigo Prod");
            modelo2.addColumn("Nombre_prod");
            modelo2.addColumn("Descripcion");
            modelo2.addColumn("Marca");
            modelo2.addColumn("Precio");
             modelo2.addColumn("Cantidad");
             modelo2.addColumn("subtotal");
 tabladatos1.setModel(modelo2);
 mostrar_detalle_venta();
 if(f.getNombre().equals("C/F")){
 }else{
 title_abon.setText("Abono");
 pen.setText("Pendiente");
  abono.setText(f.getAbono());
 pendiente.setText(f.getPendiente());
 }
    }
  public void mostrar_detalle_venta(){
    
            String[] datos=new String[9];
            
           Conexion_mysql con;
    con=new Conexion_mysql(null);
  java.sql.Connection cn=con.getConnection();
  
    String readMetaData = "CALL  PRC_cons_detalle_venta(?) ";
     try {
         cn.setAutoCommit(false); // This line must be written just after the 

         CallableStatement cs= cn.prepareCall(readMetaData);
         cs.setInt(1,f.getId_venta());
      
         //cs.registerOutParameter(2, Types.REF_CURSOR);
//cs.execute();
ResultSet rs = (ResultSet) cs.executeQuery();
if (rs != null) {
    while (rs.next()) {       
      datos[0]=rs.getString(2).toUpperCase();
      datos[1]=rs.getString(3).toUpperCase();
      datos[2]=rs.getString(4).toUpperCase();
      datos[3]=rs.getString(5).toUpperCase();
      datos[4]=rs.getString(6);
       datos[5]=rs.getString(7);
       datos[6]=Float.toString(rs.getFloat(6)*rs.getInt(7));
       modelo2.addRow(datos);
        

                }//end while
            }//end if
cs.close();
rs.close();
     } catch (SQLException ex) {
         Logger.getLogger(R_FACTURA.class.getName()).log(Level.SEVERE, null, ex);
     }


}
      
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        recivo = new javax.swing.JPanel();
        jLabel11 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        vendedor = new javax.swing.JLabel();
        jSeparator8 = new javax.swing.JSeparator();
        jSeparator1 = new javax.swing.JSeparator();
        jSeparator2 = new javax.swing.JSeparator();
        jSeparator3 = new javax.swing.JSeparator();
        jSeparator4 = new javax.swing.JSeparator();
        jSeparator6 = new javax.swing.JSeparator();
        jSeparator5 = new javax.swing.JSeparator();
        jSeparator7 = new javax.swing.JSeparator();
        jLabel7 = new javax.swing.JLabel();
        num_factura1 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        total = new javax.swing.JLabel();
        fecha = new javax.swing.JLabel();
        nit = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        telefono = new javax.swing.JLabel();
        cliente = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tabladatos1 = new javax.swing.JTable();
        jLabel14 = new javax.swing.JLabel();
        rSLabelFecha1 = new rojeru_san.componentes.RSLabelFecha();
        tot_sin_des = new javax.swing.JLabel();
        descuento = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jSeparator9 = new javax.swing.JSeparator();
        abono = new javax.swing.JLabel();
        title_abon = new javax.swing.JLabel();
        pen = new javax.swing.JLabel();
        pendiente = new javax.swing.JLabel();
        jSeparator10 = new javax.swing.JSeparator();
        jSeparator11 = new javax.swing.JSeparator();
        jLabel12 = new javax.swing.JLabel();
        tot_con_desc = new javax.swing.JLabel();
        jSeparator12 = new javax.swing.JSeparator();
        jButton1 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        recivo.setBackground(new java.awt.Color(255, 255, 255));
        recivo.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel11.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        jLabel11.setText("Carretera Principal Duraznales san Juan Ostuncalco");
        recivo.add(jLabel11, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 50, 340, 20));

        jLabel9.setFont(new java.awt.Font("Vani", 3, 14)); // NOI18N
        jLabel9.setText("Fecha.");
        recivo.add(jLabel9, new org.netbeans.lib.awtextra.AbsoluteConstraints(390, 50, 50, 20));
        recivo.add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 100, -1));

        jLabel13.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        jLabel13.setText("Gracias por preferirnos");
        recivo.add(jLabel13, new org.netbeans.lib.awtextra.AbsoluteConstraints(320, 610, 160, 20));

        vendedor.setFont(new java.awt.Font("Vani", 3, 14)); // NOI18N
        recivo.add(vendedor, new org.netbeans.lib.awtextra.AbsoluteConstraints(320, 140, 160, 20));
        recivo.add(jSeparator8, new org.netbeans.lib.awtextra.AbsoluteConstraints(320, 160, 180, 10));
        recivo.add(jSeparator1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 170, 450, 10));
        recivo.add(jSeparator2, new org.netbeans.lib.awtextra.AbsoluteConstraints(380, 40, 110, 10));
        recivo.add(jSeparator3, new org.netbeans.lib.awtextra.AbsoluteConstraints(360, 100, 100, 10));
        recivo.add(jSeparator4, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 120, 500, 10));
        recivo.add(jSeparator6, new org.netbeans.lib.awtextra.AbsoluteConstraints(250, 100, 90, 10));
        recivo.add(jSeparator5, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 160, 150, 10));
        recivo.add(jSeparator7, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 610, 100, 10));

        jLabel7.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        jLabel7.setText("Cliente:");
        recivo.add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 140, 60, 20));

        num_factura1.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        recivo.add(num_factura1, new org.netbeans.lib.awtextra.AbsoluteConstraints(400, 20, 100, 20));

        jLabel6.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        jLabel6.setText("Total  con Des");
        recivo.add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 560, 170, 30));

        total.setFont(new java.awt.Font("Tahoma", 3, 14)); // NOI18N
        recivo.add(total, new org.netbeans.lib.awtextra.AbsoluteConstraints(360, 500, 80, 30));

        fecha.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        fecha.setText("Vendedor:");
        recivo.add(fecha, new org.netbeans.lib.awtextra.AbsoluteConstraints(230, 140, 80, 20));

        nit.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        nit.setText("Nit: 12312-123");
        recivo.add(nit, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 80, 120, 20));

        jLabel2.setFont(new java.awt.Font("Tahoma", 3, 24)); // NOI18N
        jLabel2.setText("AUTOREPUESTOS\"SIGÜILA\"");
        recivo.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 0, 350, 50));

        jLabel3.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        jLabel3.setText("CODIGO VENTA");
        recivo.add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(360, 10, 150, 20));

        telefono.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        telefono.setText("Tel: 3319-8045");
        recivo.add(telefono, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 80, 110, 20));

        cliente.setFont(new java.awt.Font("Vani", 3, 14)); // NOI18N
        recivo.add(cliente, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 140, 110, 20));

        tabladatos1.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        tabladatos1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "codigo_prod", "Nombre", "Descripción", "Prec_Unit", "Cantidad", "Monto"
            }
        ));
        tabladatos1.setGridColor(new java.awt.Color(255, 255, 255));
        tabladatos1.setSelectionBackground(new java.awt.Color(51, 204, 255));
        jScrollPane1.setViewportView(tabladatos1);

        recivo.add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 190, 480, 340));

        jLabel14.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        jLabel14.setText("Listado de productos comprados.");
        recivo.add(jLabel14, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 170, 270, 20));

        rSLabelFecha1.setForeground(new java.awt.Color(0, 0, 0));
        recivo.add(rSLabelFecha1, new org.netbeans.lib.awtextra.AbsoluteConstraints(310, 70, -1, 30));

        tot_sin_des.setFont(new java.awt.Font("DFKai-SB", 3, 18)); // NOI18N
        recivo.add(tot_sin_des, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 530, 100, 20));

        descuento.setFont(new java.awt.Font("DFKai-SB", 3, 18)); // NOI18N
        recivo.add(descuento, new org.netbeans.lib.awtextra.AbsoluteConstraints(400, 530, 90, 20));

        jLabel8.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        jLabel8.setText("Descuento");
        recivo.add(jLabel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(300, 530, 100, 20));
        recivo.add(jSeparator9, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 580, 100, 10));

        abono.setFont(new java.awt.Font("DFKai-SB", 3, 18)); // NOI18N
        recivo.add(abono, new org.netbeans.lib.awtextra.AbsoluteConstraints(400, 560, 100, 20));

        title_abon.setFont(new java.awt.Font("Urdu Typesetting", 3, 18)); // NOI18N
        recivo.add(title_abon, new org.netbeans.lib.awtextra.AbsoluteConstraints(300, 560, 90, 20));

        pen.setFont(new java.awt.Font("Urdu Typesetting", 3, 18)); // NOI18N
        recivo.add(pen, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 590, 90, 20));

        pendiente.setFont(new java.awt.Font("DFKai-SB", 3, 18)); // NOI18N
        recivo.add(pendiente, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 590, 100, 20));
        recivo.add(jSeparator10, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 550, 100, 10));
        recivo.add(jSeparator11, new org.netbeans.lib.awtextra.AbsoluteConstraints(400, 580, 100, 10));

        jLabel12.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        jLabel12.setText("Total  Sin  Descuento");
        recivo.add(jLabel12, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 530, 190, 30));

        tot_con_desc.setFont(new java.awt.Font("DFKai-SB", 3, 18)); // NOI18N
        recivo.add(tot_con_desc, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 560, 100, 20));
        recivo.add(jSeparator12, new org.netbeans.lib.awtextra.AbsoluteConstraints(400, 550, 100, 10));

        getContentPane().add(recivo, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 510, 630));

        jButton1.setBackground(new java.awt.Color(204, 204, 255));
        jButton1.setFont(new java.awt.Font("Tahoma", 3, 14)); // NOI18N
        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/btn-imprimirMenu.png"))); // NOI18N
        jButton1.setText("Imprimir");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        getContentPane().add(jButton1, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 630, 130, 30));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
  imprimirFactura();    

        //SE LLAMA LA FUNCION PRINT EN UN TRY CACH
      /*  try{
            //se usa una clase printer JOB
            PrinterJob gap= PrinterJob.getPrinterJob();
            //se determina que este jframe pueda ser imprimible
            gap.setPrintable(this);
            boolean top=gap.printDialog();//si se da en boto haceptar se entiende en valor 1 y se imprime
            if(top){//si tiene un valor igual a cero no se ejecuta la impresion
                gap.print();
            }

        }catch(Exception e){
            JOptionPane.showMessageDialog(null, "No se ha imprimido","Error\n"+e,JOptionPane.INFORMATION_MESSAGE);
        }
        Ventas venta=new Ventas();
        venta.setVisible(true);
        this.setVisible(false);*/
    }//GEN-LAST:event_jButton1ActionPerformed

public  void imprimirFactura(){
        PrinterMatrix printer = new PrinterMatrix();
int filas = tabladatos1.getRowCount();

int filaCount=0;
System.out.println("Fila total: "+filas);
        //Definir el tamanho del papel para la impresion  aca 10 lineas y 2 columnas
        if(filas==1){
          
              if(!abono.getText().equals("")||!pendiente.getText().equals("")){
       printer.setOutSize((30)*filas,2);
     }else{
               printer.setOutSize((29)*filas,2);
              }
        }else{
           
                 if(!abono.getText().equals("")||!pendiente.getText().equals("")){
           printer.setOutSize(((9)*filas)+20,2);
     }else{
           printer.setOutSize(((9)*filas)+19,2);
              }
        }
       printer.printTextWrap(1, 0, 0, 80, "*****AUTO REPUESTOS SIGUILA*****");
       printer.printTextWrap(2, 0, 0, 80, "Direccion: Carretera Principal Duraznales San Juan Ostuncalco");
       printer.printTextWrap(3, 0, 0, 80, telefono.getText());
       printer.printTextWrap(4, 0, 0, 80, nit.getText());
       printer.printTextWrap(5, 0, 0, 80, "");
       printer.printTextWrap(6, 0, 0, 80, "");
       
       printer.printTextWrap(7, 0, 0, 80, "*****FACTURA DE VENTA******");
       //printer.printTextWrap(linI, linE, colI, colE, null);
       printer.printTextWrap(8, 0, 0, 22, "Num. Boleta : "+num_factura1.getText());
       printer.printTextWrap(9, 0, 0, 80, "Fecha Boleta: "+rSLabelFecha1.getFecha());
       printer.printTextWrap(10, 0, 0, 80, "CLIENTE: " + cliente.getText());
       printer.printTextWrap(11, 0, 0, 80, "Vendedor: " + vendedor.getText());
       filaCount=12;
     
        for (int i = 0; i < filas; i++) { 
       printer.printTextWrap(filaCount,0,0,80,"____________________________"); 
       filaCount++;
       printer.printTextWrap(filaCount,0, 0, 80, "Cod Prod: "+tabladatos1.getValueAt(i,0).toString()); 
       filaCount++;
       printer.printTextWrap(filaCount,0, 0, 80 ,"Prod: "+tabladatos1.getValueAt(i,1).toString()); 
        filaCount++;
       printer.printTextWrap(filaCount,0, 0, 80 ,"Desc: "+tabladatos1.getValueAt(i,2).toString()); 
        filaCount++;
       printer.printTextWrap(filaCount, 0, 0, 80 ,"Marca: "+tabladatos1.getValueAt(i,3).toString()); 
        filaCount++;
       printer.printTextWrap(filaCount,0, 0, 80,"Prec Un: Q."+tabladatos1.getValueAt(i,4).toString()); 
        filaCount++;
       printer.printTextWrap(filaCount,0,0, 80,"Cant: "+tabladatos1.getValueAt(i,5).toString()); 
        filaCount++;
       printer.printTextWrap(filaCount,0,0, 80,"Mont: Q."+tabladatos1.getValueAt(i,6).toString()); 
        filaCount++;
       printer.printTextWrap(filaCount,0,0,80,"____________________"); 
       filaCount++;
        }
        
      printer.printTextWrap(filaCount,0,0,80,"Esta boleta no tiene valor fiscal");
      printer.printTextWrap(filaCount+1,0,0,80,"Total Sin Descuento: Q."+tot_sin_des.getText());
      printer.printTextWrap(filaCount+2,0,0,80,"Descuento: Q."+descuento.getText());
      printer.printTextWrap(filaCount+3,0,0,80,"Total Con Descuento: Q."+tot_con_desc.getText());
     if(!abono.getText().equals("")||!pendiente.getText().equals("")){
      printer.printTextWrap(filaCount+4,0,0,80,"Prima: Q."+abono.getText());
      printer.printTextWrap(filaCount+5,0,0,80,"Pendiente: Q."+pendiente.getText());
     }
        printer.toFile("impresion.txt");

      FileInputStream inputStream = null;
        try {
            inputStream = new FileInputStream("impresion.txt");
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        }
        if (inputStream == null) {
            return;
        }

        DocFlavor docFormat = DocFlavor.INPUT_STREAM.AUTOSENSE;
        Doc document = new SimpleDoc(inputStream, docFormat, null);

        PrintRequestAttributeSet attributeSet = new HashPrintRequestAttributeSet();

        PrintService defaultPrintService = PrintServiceLookup.lookupDefaultPrintService();


        if (defaultPrintService != null) {
            DocPrintJob printJob = defaultPrintService.createPrintJob();
            
            try {
                printJob.print(document, attributeSet);

            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } else {
            System.err.println("No existen impresoras instaladas");
        }

        //inputStream.close();
     
    }
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Comprobante.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Comprobante.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Comprobante.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Comprobante.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Comprobante().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel abono;
    private javax.swing.JLabel cliente;
    private javax.swing.JLabel descuento;
    private javax.swing.JLabel fecha;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator10;
    private javax.swing.JSeparator jSeparator11;
    private javax.swing.JSeparator jSeparator12;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JSeparator jSeparator3;
    private javax.swing.JSeparator jSeparator4;
    private javax.swing.JSeparator jSeparator5;
    private javax.swing.JSeparator jSeparator6;
    private javax.swing.JSeparator jSeparator7;
    private javax.swing.JSeparator jSeparator8;
    private javax.swing.JSeparator jSeparator9;
    private javax.swing.JLabel nit;
    private javax.swing.JLabel num_factura1;
    private javax.swing.JLabel pen;
    private javax.swing.JLabel pendiente;
    private rojeru_san.componentes.RSLabelFecha rSLabelFecha1;
    private javax.swing.JPanel recivo;
    private javax.swing.JTable tabladatos1;
    private javax.swing.JLabel telefono;
    private javax.swing.JLabel title_abon;
    private javax.swing.JLabel tot_con_desc;
    private javax.swing.JLabel tot_sin_des;
    private javax.swing.JLabel total;
    private javax.swing.JLabel vendedor;
    // End of variables declaration//GEN-END:variables

    @Override
    public int print(Graphics graf, PageFormat pagfor, int index) throws PrinterException {//INDEX EMPIEZA EN PAGINA 1 
        if(index>0){//SOLO IMPRIME UNA PAGINA
        return NO_SUCH_PAGE;
        }
     //SE captura la imagen se le da un formato una escala 
     Graphics2D hub=(Graphics2D) graf;//se menciona el objeto que se manda que es graf
     hub.translate(pagfor.getImageableX()+30,pagfor.getImageableY()+30);//se le indica solo el cuadro de la factura getImageablex obtiene la ubicacion deacuerdo al plano cartesiano
   hub.scale(1.0, 1.0);//escala a la impresion grande o peque;o 
   
   //linea que indica que se va a IMPRIMIR
   recivo.printAll(graf);
   return PAGE_EXISTS;
    }
}
