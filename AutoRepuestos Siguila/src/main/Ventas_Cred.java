package main;

import clases.Imp_fac;
import clases.TextPrompt;
import clases.Conexion_postgres;
import clases.Activacion;
import clases.Conexion_mysql;
import clases.variables;
import java.awt.Color;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;
import javax.swing.RowFilter;
import javax.swing.UIManager;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;

/**
 *
 * @author Leo
 */
public class Ventas_Cred extends javax.swing.JFrame {

    int codigoRecivido, Id_cliente, Id_venta;
    String entrada1, entrada;
    int id_domicilio;
//variable de suma de subtotal
    float sum, resta;
    Imp_fac f = new Imp_fac();
    variables v=new variables();

    Conexion_postgres cn = new Conexion_postgres();
    DefaultTableModel modelo = new DefaultTableModel();
    DefaultTableModel modelo1 = new DefaultTableModel();
    DefaultTableModel modelo2 = new DefaultTableModel();

//variables que envian datos a la tabla detalle
    String Id_E, Nombre_E, Descripcion_E, Marca_E, Precio_E, Estado_E;
//variable que manda la cantidad d producto hacia la tabla de venta
    int cantidad;
   private TableRowSorter filtro;
    public Ventas_Cred() {

        initComponents();
        
        tabladatos1.setOpaque(false);
    jScrollPane1.setOpaque(false);
    jScrollPane1.getViewport().setOpaque(false);
    //segunda tabla
    tabladatos2.setOpaque(false);
    jScrollPane2.setOpaque(false);
    jScrollPane2.getViewport().setOpaque(false);
 //Tercera tabla
    tabladatos3.setOpaque(false);
    jScrollPane3.setOpaque(false);
    jScrollPane3.getViewport().setOpaque(false);

        loading.setVisible(false);
        m_nom.disable();
        m_ap.disable();
        m_dir.disable();
        tabladatos2.setDefaultEditor(Object.class, null);
        tabladatos3.setDefaultEditor(Object.class, null);

        desc.setEnabled(false);
        dire.setEnabled(false);
        combustible.setEnabled(false);
        Activacion ac = new Activacion();
        vendedor.setText(ac.nom_log);
        Productos_Entrantes();
        modelo1.addColumn("CODIGO");
        modelo1.addColumn("NOMBRE");
        modelo1.addColumn("APELLIDO");
        modelo1.addColumn("TELEFONO");
        modelo1.addColumn("DIRECCION");
        tabladatos1.setModel(modelo1);
//Titulo columna tabla producto
        modelo.addColumn("Id_Prod");
        modelo.addColumn("Nombre_Prod");
        modelo.addColumn("Descripcion");
        modelo.addColumn("Marca");
        modelo.addColumn("Precio_Com");
        modelo.addColumn("Precio_Unit");
        modelo.addColumn("Estado");
        modelo.addColumn("Existencia");
        tabladatos2.setModel(modelo);
        //agregar nombre de columna tabla3
        modelo2.addColumn("Codigo");
        modelo2.addColumn("Nombre");
        modelo2.addColumn("Descripcion");
        modelo2.addColumn("Marca");
        modelo2.addColumn("Precio_Ven");
        modelo2.addColumn("Estado_Prod");
        modelo2.addColumn("Cantidad");
        modelo2.addColumn("Subtotal");
        tabladatos3.setModel(modelo2);

        this.setLocationRelativeTo(null);
        TextPrompt u = new TextPrompt("Ingrese Nombre", busqueda);
        TextPrompt v = new TextPrompt("Ingrese Apellido", busqueda1);
        TextPrompt p = new TextPrompt("Ingrese Nombre", bus_prod);
        TextPrompt q = new TextPrompt("Ingrese Descripcion", bus_desc);
        TextPrompt r = new TextPrompt("Ingrese Marca", bus_marc);
        //aCTIVACION
        r_pr.setEnabled(false);
        r_pro.setEnabled(false);
        r_em.setEnabled(false);
        r_fa.setEnabled(false);
        r_cli.setEnabled(false);
        b_pro.setEnabled(false);
        b_emp.setEnabled(false);
        b_fac.setEnabled(false);
        b_cli.setEnabled(false);
        ven.setEnabled(false);
        venE.setEnabled(false);
        rep.setEnabled(false);
        dev.setEnabled(false);
        m_pro.setEnabled(false);
        m_emp.setEnabled(false);
        m_cli.setEnabled(false);
        m_prov.setEnabled(false);
        b_ventas.setEnabled(false);
        r_pago.setEnabled(false);
        Activacion b = new Activacion();
        if (b.ver == 1) {
            b.entrante(r_pr, r_pro, r_em, r_fa, r_cli, b_pro, b_emp, b_fac, b_cli, modificacion, devolucion, ven, ea, dev, m_pro,
                    m_emp, m_cli, m_prov, rep, venE, b_ventas, r_pago);
        }
        if (b.ver == 2) {
            b.en(ven, venE, r_fa, b_pro, r_pr, r_pro, r_cli, r_pago, dev, b_cli, b_fac);
        }
        Mostrar_Clientes();
        consulta_prod_registro_fac();
    }

    public void Insertar_Domicilio() {
         Conexion_mysql con;
  con=new Conexion_mysql(loading);
  java.sql.Connection cn=con.getConnection();
  
        String sql = "call PRC_vent_a_domicilio(?,?)";
        try {

            CallableStatement cs = cn.prepareCall(sql);
            cs.setFloat(1, Float.parseFloat(combustible.getText()));
            cs.setString(2, dire.getText().toUpperCase());
            if (cs.execute()) {
                JOptionPane.showMessageDialog(null, "Registro Completado");
            }
            cs.close();
        } catch (Exception ex) {
            Logger.getLogger(R_PROD.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    //REGISTRO PAGOS

    public void Insertar_Pago() {
         Conexion_mysql con;
  con=new Conexion_mysql(loading);
  java.sql.Connection cn=con.getConnection();
  
        String Sql = "call PRC_re_pag(?,?,?)";
        try {
            CallableStatement cs = cn.prepareCall(Sql);
            cs.setInt(1, Id_venta);
            cs.setFloat(2, Float.parseFloat(abon.getText()));
            cs.setFloat(3, Float.parseFloat(t_pag1.getText()));
            if (cs.execute()) {
                JOptionPane.showMessageDialog(null, "Registro Completado");
            }
            cs.close();
        } catch (Exception e) {
        }

    }
    //CONSULTA ADOMICILIO

    public void Consulta_ID_Domicilio() {
         Conexion_mysql con;
  con=new Conexion_mysql(loading);
  java.sql.Connection cn=con.getConnection();
  
        String readMetaData = "CALL PRC_mostrar_id_domicilio()";
        try {
            cn.setAutoCommit(false); // This line must be written just after the 

            CallableStatement cs = cn.prepareCall(readMetaData);

            //cs.registerOutParameter(1, Types.REF_CURSOR);
            //cs.execute();
            ResultSet rs = (ResultSet) cs.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    id_domicilio = rs.getInt(1);
                    System.out.print("\n ID ADOMICILIO"+id_domicilio+"\n");
                }//end while
            }//end if
        } catch (SQLException ex) {

        }
    }//end if
    //Menu de popMenu para eliminar productos seleccionados producto entrante

    public void Productos_Entrantes() {
        //Sección 1
        // DefaultTableModel model = (DefaultTableModel) tabladatos1.getModel(); 
        JPopupMenu popupMenu = new JPopupMenu();
        JMenuItem menuItem1 = new JMenuItem("Eliminar", new ImageIcon(getClass().getResource("/img/registro.png")));
        menuItem1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                desc.setText("0");
                t_pag1.setText("");
                abon.setText("0");
                int a = tabladatos3.getSelectedRow();
                int cuentaFilasSeleccionadas = tabladatos3.getSelectedRowCount();
                if (cuentaFilasSeleccionadas == 0) {

                    JOptionPane.showMessageDialog(null, "<html><h1 style=\"color:red;\">NO HAY FILA SELECCIONADO</h1></html>", "FILA", JOptionPane.QUESTION_MESSAGE);
                } else {
                    modelo2.removeRow(a);
                    suma();
                }
            }
        });
        popupMenu.add(menuItem1);
        tabladatos3.setComponentPopupMenu(popupMenu);
    }

    //MOSTRAR DATOS EN LA TABLA
    public void Mostrar_Clientes() {

        String[] datos = new String[9];
         Conexion_mysql con;
  con=new Conexion_mysql(loading);
  java.sql.Connection cn=con.getConnection();
  
        String readMetaData = "CALL PRC_Cons_Cliente_Modificacion() ";
        try {
            cn.setAutoCommit(false); // This line must be written just after the 

            CallableStatement cs = cn.prepareCall(readMetaData);
         
            //cs.registerOutParameter(3, Types.REF_CURSOR);
            //cs.execute();
            ResultSet rs = (ResultSet) cs.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    datos[0] = rs.getString(1).toUpperCase();
                    datos[1] = rs.getString(2).toUpperCase();
                    datos[2] = rs.getString(3).toUpperCase();
                    datos[3] = rs.getString(4).toUpperCase();
                    datos[4] = rs.getString(5).toUpperCase();

                    modelo1.addRow(datos);

                }//end while
            }//end if
        } catch (SQLException ex) {
            Logger.getLogger(R_FACTURA.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void borrar(DefaultTableModel tabla) {
        while (tabla.getRowCount() > 0) {
            tabla.removeRow(0);
        }
    }
    //Consulta producto##############3

    public void consulta_prod_registro_fac() {

        String[] datos = new String[9];
         Conexion_mysql con;
  con=new Conexion_mysql(loading);
  java.sql.Connection cn=con.getConnection();
  
        String readMetaData = "CALL PRC_Cons_Prod_Reg_Fac() ";
        try {
            cn.setAutoCommit(false); // This line must be written just after the 

            CallableStatement cs = cn.prepareCall(readMetaData);
          
            //cs.registerOutParameter(4, Types.REF_CURSOR);
            //cs.execute();
            ResultSet rs = (ResultSet) cs.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    datos[0] = rs.getString(1).toUpperCase();
                    datos[1] = rs.getString(2).toUpperCase();
                    datos[2] = rs.getString(3).toUpperCase();
                    datos[3] = rs.getString(4).toUpperCase();
                    datos[4] = rs.getString(5).toUpperCase();
                    datos[5] = rs.getString(6).toUpperCase();
                    datos[6] = rs.getString(7).toUpperCase();
                    datos[7] = rs.getString(8).toUpperCase();
//PROCESO PARA CONSULTAR IMAGEN
//TERMINACION DE PROCESO PARA CONSULTAR IMAGEN
                    modelo.addRow(datos);

                }//end while
            }//end if
        } catch (SQLException ex) {
            Logger.getLogger(R_FACTURA.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void Consulta_Imagen() {
        //Variables para la imagen
        InputStream is;
        ImageIcon foto;
         Conexion_mysql con;
  con=new Conexion_mysql(loading);
  java.sql.Connection cn=con.getConnection();
  
        String readMetaData = "CALL PRC_Con_Imag_Re_Fac(?) ";
        try {
            cn.setAutoCommit(false); // This line must be written just after the 

            CallableStatement cs = cn.prepareCall(readMetaData);
            cs.setInt(1, codigoRecivido);

            //cs.registerOutParameter(2, Types.REF_CURSOR);
            //cs.execute();
            ResultSet rs = (ResultSet) cs.executeQuery();
            if (rs != null) {
                while (rs.next()) {

//PROCESO PARA CONSULTAR IMAGEN
                    is = rs.getBinaryStream(1);
                    try {
                        BufferedImage bi = ImageIO.read(is);
                        foto = new ImageIcon(bi);
                        Image img = foto.getImage();
                        Image newimg = img.getScaledInstance(170, 160,
                                java.awt.Image.SCALE_SMOOTH);
                        ImageIcon newicon = new ImageIcon(newimg);
                        rec_img.setIcon(newicon);//enviarlo a un jlabel    
                    } catch (IOException ex) {
                        Logger.getLogger(R_FACTURA.class.getName()).log(Level.SEVERE, null, ex);
                    }

//TERMINACION DE PROCESO PARA CONSULTAR IMAGEN
                }//end while
            }//end if
        } catch (SQLException ex) {
            Logger.getLogger(R_FACTURA.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
    //REGISTRO VENTA

    public void Registro_Venta(int id_dom) {
         Conexion_mysql con;
  con=new Conexion_mysql(loading);
  java.sql.Connection cn=con.getConnection();
  
        Activacion ac = new Activacion();
        try {
            String Sql = "call PRC_reg_venta(?,?,?,?,?,?,?)";
            CallableStatement cs = cn.prepareCall(Sql);
            cs.setInt(1, ac.codigo_log);
            cs.setInt(2, 2);
            cs.setInt(3, Id_cliente);
            cs.setInt(4, id_dom);
            cs.setFloat(5, Float.parseFloat(desc.getText()));
            cs.setFloat(6, Float.parseFloat(t_pag.getText()));
            cs.setString(7, "PENDIENTE");
            if (cs.execute()) {
                JOptionPane.showMessageDialog(null, "Registro Completado");
            }
            cs.close();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this, e);
        }
    }
    //Consulta Id VENTA

    public void Consulta_id_Venta() {

        String[] datos = new String[9];
         Conexion_mysql con;
  con=new Conexion_mysql(loading);
  java.sql.Connection cn=con.getConnection();
  
        String readMetaData = "CALL PRC_Consulta_Id_Venta() ";
        try {
            cn.setAutoCommit(false); // This line must be written just after the 

            CallableStatement cs = cn.prepareCall(readMetaData);

            //cs.registerOutParameter(1, Types.REF_CURSOR);
            //cs.execute();
            ResultSet rs = (ResultSet) cs.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    Id_venta = rs.getInt(1);
                }//end while
            }//end if
            System.err.println();
        } catch (SQLException ex) {
            Logger.getLogger(R_FACTURA.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void registro_detalle_Venta_Credito() {
         Conexion_mysql con;
  con=new Conexion_mysql(loading);
  java.sql.Connection cn=con.getConnection();
  
        if (tabladatos3.getRowCount() > 0) {
            for (int i = 0; i < tabladatos3.getRowCount(); i++) {
                String Sql = "call PRC_reg_detalle_venta(?,?,?,?)";
                try {
                    CallableStatement cs = cn.prepareCall(Sql);
                    cs.setInt(1, Integer.parseInt(tabladatos3.getValueAt(i, 0).toString()));
                    cs.setInt(2, Id_venta);
                    cs.setFloat(3, Float.parseFloat(tabladatos3.getValueAt(i, 4).toString()));
                    cs.setInt(4, Integer.parseInt(tabladatos3.getValueAt(i, 6).toString()));
                    if (cs.execute()) {
                        JOptionPane.showMessageDialog(null, "Registro Completado");
                    }
                    cs.close();
                } catch (Exception e) {
                    JOptionPane.showMessageDialog(this, e);
                }
            }
        } else {
            JOptionPane.showMessageDialog(this, "La tabla se encuentra vacio");
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        Agregar = new javax.swing.JPopupMenu();
        add = new javax.swing.JMenuItem();
        jPanel1 = new javax.swing.JPanel();
        fecha = new javax.swing.JLabel();
        busqueda1 = new javax.swing.JTextField();
        jScrollPane2 = new javax.swing.JScrollPane();
        tabladatos1 = new javax.swing.JTable();
        vendedor = new javax.swing.JLabel();
        rec_img = new javax.swing.JLabel();
        bus_prod = new javax.swing.JTextField();
        loading = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        tabladatos2 = new javax.swing.JTable();
        bus_desc = new javax.swing.JTextField();
        busqueda = new javax.swing.JTextField();
        jLabel20 = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        bus_marc = new javax.swing.JTextField();
        jLabel15 = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        check = new javax.swing.JCheckBox();
        jSeparator3 = new javax.swing.JSeparator();
        jLabel13 = new javax.swing.JLabel();
        jLabel18 = new javax.swing.JLabel();
        dire = new javax.swing.JTextField();
        combustible = new javax.swing.JTextField();
        m_nom = new javax.swing.JTextField();
        m_ap = new javax.swing.JTextField();
        m_dir = new javax.swing.JTextField();
        rSLabelHora2 = new rojeru_san.componentes.RSLabelHora();
        rSLabelFecha2 = new rojeru_san.componentes.RSLabelFecha();
        jButton7 = new javax.swing.JButton();
        jLabel5 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        jLabel19 = new javax.swing.JLabel();
        jSeparator4 = new javax.swing.JSeparator();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tabladatos3 = new javax.swing.JTable();
        jLabel7 = new javax.swing.JLabel();
        jButton2 = new javax.swing.JButton();
        t_pag = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        t_pag1 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        panel1 = new javax.swing.JPanel();
        desc = new javax.swing.JTextField();
        jLabel27 = new javax.swing.JLabel();
        sel1 = new javax.swing.JCheckBox();
        sel = new javax.swing.JCheckBox();
        jLabel26 = new javax.swing.JLabel();
        abon = new javax.swing.JTextField();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jMenu3 = new javax.swing.JMenu();
        jMenu2 = new javax.swing.JMenu();
        jMenu4 = new javax.swing.JMenu();
        r_pr = new javax.swing.JMenuItem();
        r_em = new javax.swing.JMenuItem();
        r_fa = new javax.swing.JMenuItem();
        r_pro = new javax.swing.JMenuItem();
        r_cli = new javax.swing.JMenuItem();
        jMenu6 = new javax.swing.JMenu();
        b_pro = new javax.swing.JMenuItem();
        b_emp = new javax.swing.JMenuItem();
        b_fac = new javax.swing.JMenuItem();
        b_cli = new javax.swing.JMenuItem();
        b_ventas = new javax.swing.JMenuItem();
        modificacion = new javax.swing.JMenu();
        m_pro = new javax.swing.JMenuItem();
        m_emp = new javax.swing.JMenuItem();
        m_cli = new javax.swing.JMenuItem();
        m_prov = new javax.swing.JMenuItem();
        devolucion = new javax.swing.JMenu();
        dev = new javax.swing.JMenuItem();
        jMenu7 = new javax.swing.JMenu();
        ven = new javax.swing.JMenuItem();
        venE = new javax.swing.JMenuItem();
        ea = new javax.swing.JMenu();
        rep = new javax.swing.JMenuItem();
        jMenu5 = new javax.swing.JMenu();
        r_pago = new javax.swing.JMenuItem();

        add.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/add (1).png"))); // NOI18N
        add.setText("Agregar");
        add.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addActionPerformed(evt);
            }
        });
        Agregar.add(add);

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setUndecorated(true);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setBorder(javax.swing.BorderFactory.createMatteBorder(1, 1, 1, 1, new java.awt.Color(255, 204, 51)));
        jPanel1.setLayout(null);
        jPanel1.add(fecha);
        fecha.setBounds(470, 10, 40, 30);

        busqueda1.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        busqueda1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                busqueda1ActionPerformed(evt);
            }
        });
        busqueda1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                busqueda1KeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                busqueda1KeyTyped(evt);
            }
        });
        jPanel1.add(busqueda1);
        busqueda1.setBounds(250, 70, 160, 30);

        tabladatos1.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        tabladatos1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null},
                {null, null},
                {null, null},
                {null, null}
            },
            new String [] {
                "Id Cliente", "Nombre "
            }
        ));
        tabladatos1.setSelectionBackground(new java.awt.Color(51, 204, 255));
        tabladatos1.setSelectionForeground(new java.awt.Color(0, 0, 51));
        tabladatos1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                tabladatos1MousePressed(evt);
            }
        });
        jScrollPane2.setViewportView(tabladatos1);

        jPanel1.add(jScrollPane2);
        jScrollPane2.setBounds(0, 110, 1200, 110);

        vendedor.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        vendedor.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanel1.add(vendedor);
        vendedor.setBounds(10, 30, 260, 30);

        rec_img.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 0)));
        jPanel1.add(rec_img);
        rec_img.setBounds(1060, 230, 170, 160);

        bus_prod.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        bus_prod.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bus_prodActionPerformed(evt);
            }
        });
        bus_prod.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                bus_prodKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                bus_prodKeyTyped(evt);
            }
        });
        jPanel1.add(bus_prod);
        bus_prod.setBounds(250, 240, 200, 30);

        loading.setIcon(new javax.swing.ImageIcon(getClass().getResource("/main/loading_1.gif"))); // NOI18N
        jPanel1.add(loading);
        loading.setBounds(280, 220, 250, 200);

        tabladatos2.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        tabladatos2.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null}
            },
            new String [] {
                "Id Producto", "Nombre Producto", "Existencia"
            }
        ));
        tabladatos2.setComponentPopupMenu(Agregar);
        tabladatos2.setSelectionBackground(new java.awt.Color(51, 204, 255));
        tabladatos2.setSelectionForeground(new java.awt.Color(0, 0, 51));
        tabladatos2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                tabladatos2MousePressed(evt);
            }
        });
        jScrollPane3.setViewportView(tabladatos2);

        jPanel1.add(jScrollPane3);
        jScrollPane3.setBounds(0, 280, 1030, 110);

        bus_desc.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        bus_desc.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bus_descActionPerformed(evt);
            }
        });
        bus_desc.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                bus_descKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                bus_descKeyTyped(evt);
            }
        });
        jPanel1.add(bus_desc);
        bus_desc.setBounds(480, 240, 220, 30);

        busqueda.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        busqueda.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                busquedaActionPerformed(evt);
            }
        });
        busqueda.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                busquedaKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                busquedaKeyTyped(evt);
            }
        });
        jPanel1.add(busqueda);
        busqueda.setBounds(90, 71, 150, 30);

        jLabel20.setFont(new java.awt.Font("Times New Roman", 1, 24)); // NOI18N
        jLabel20.setText("Selección de Producto");
        jPanel1.add(jLabel20);
        jLabel20.setBounds(10, 240, 240, 30);
        jPanel1.add(jSeparator1);
        jSeparator1.setBounds(10, 60, 1150, 10);

        bus_marc.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        bus_marc.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                bus_marcKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                bus_marcKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                bus_marcKeyTyped(evt);
            }
        });
        jPanel1.add(bus_marc);
        bus_marc.setBounds(740, 240, 220, 30);

        jLabel15.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        jLabel15.setText("Vendedor");
        jPanel1.add(jLabel15);
        jLabel15.setBounds(10, 10, 120, 20);

        jLabel17.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/MOTO.png"))); // NOI18N
        jPanel1.add(jLabel17);
        jLabel17.setBounds(920, 0, 75, 65);

        check.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        check.setText("A Domicilio");
        check.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                checkMouseClicked(evt);
            }
        });
        check.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                checkActionPerformed(evt);
            }
        });
        jPanel1.add(check);
        check.setBounds(280, 20, 120, 30);
        jPanel1.add(jSeparator3);
        jSeparator3.setBounds(0, 400, 1220, 10);

        jLabel13.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        jLabel13.setText("Dirección");
        jPanel1.add(jLabel13);
        jLabel13.setBounds(410, 0, 130, 30);

        jLabel18.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        jLabel18.setText("  Combustible Q ");
        jPanel1.add(jLabel18);
        jLabel18.setBounds(710, 0, 140, 30);

        dire.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        dire.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                direActionPerformed(evt);
            }
        });
        jPanel1.add(dire);
        dire.setBounds(410, 30, 290, 30);

        combustible.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        combustible.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                combustibleActionPerformed(evt);
            }
        });
        jPanel1.add(combustible);
        combustible.setBounds(720, 30, 200, 30);

        m_nom.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        m_nom.setDisabledTextColor(new java.awt.Color(0, 0, 0));
        m_nom.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                m_nomKeyPressed(evt);
            }
        });
        jPanel1.add(m_nom);
        m_nom.setBounds(660, 70, 160, 30);

        m_ap.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        m_ap.setDisabledTextColor(new java.awt.Color(0, 0, 0));
        m_ap.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                m_apKeyPressed(evt);
            }
        });
        jPanel1.add(m_ap);
        m_ap.setBounds(850, 70, 160, 30);

        m_dir.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        m_dir.setDisabledTextColor(new java.awt.Color(0, 0, 0));
        jPanel1.add(m_dir);
        m_dir.setBounds(1030, 70, 160, 30);
        jPanel1.add(rSLabelHora2);
        rSLabelHora2.setBounds(980, 0, 200, 30);
        jPanel1.add(rSLabelFecha2);
        rSLabelFecha2.setBounds(970, 30, 200, 30);

        jButton7.setFont(new java.awt.Font("Tahoma", 3, 24)); // NOI18N
        jButton7.setForeground(new java.awt.Color(255, 255, 255));
        jButton7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/salir.png"))); // NOI18N
        jButton7.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jButton7.setIconTextGap(1);
        jButton7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton7ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton7);
        jButton7.setBounds(1170, 10, 50, 40);

        jLabel5.setBackground(new java.awt.Color(255, 255, 255));
        jLabel5.setFont(new java.awt.Font("Tahoma", 3, 14)); // NOI18N
        jLabel5.setText("SALIR");
        jPanel1.add(jLabel5);
        jLabel5.setBounds(1170, 50, 44, 17);

        jLabel16.setFont(new java.awt.Font("Times New Roman", 1, 24)); // NOI18N
        jLabel16.setText("Cliente Seleccionado");
        jPanel1.add(jLabel16);
        jLabel16.setBounds(420, 70, 230, 30);

        jLabel19.setFont(new java.awt.Font("Times New Roman", 1, 24)); // NOI18N
        jLabel19.setText("Cliente");
        jPanel1.add(jLabel19);
        jLabel19.setBounds(10, 70, 90, 30);
        jPanel1.add(jSeparator4);
        jSeparator4.setBounds(0, 230, 1060, 10);

        getContentPane().add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 1240, 410));

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));
        jPanel2.setLayout(null);

        tabladatos3.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        tabladatos3.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null}
            },
            new String [] {
                "Línea", "Descripcion", "Cantidad", "Prec. Unit. Q", "Dscto.%", "Sub Total Q"
            }
        ));
        tabladatos3.setSelectionBackground(new java.awt.Color(51, 204, 255));
        tabladatos3.setSelectionForeground(new java.awt.Color(0, 0, 51));
        tabladatos3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                tabladatos3MousePressed(evt);
            }
        });
        jScrollPane1.setViewportView(tabladatos3);

        jPanel2.add(jScrollPane1);
        jScrollPane1.setBounds(10, 30, 1170, 120);

        jLabel7.setFont(new java.awt.Font("Times New Roman", 1, 24)); // NOI18N
        jLabel7.setText("Detalle de Venta");
        jPanel2.add(jLabel7);
        jLabel7.setBounds(10, 0, 250, 30);

        jButton2.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        jButton2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/save.png"))); // NOI18N
        jButton2.setText("Realizar Venta");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });
        jPanel2.add(jButton2);
        jButton2.setBounds(1010, 190, 210, 30);

        t_pag.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        t_pag.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanel2.add(t_pag);
        t_pag.setBounds(110, 160, 170, 20);

        jLabel10.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        jLabel10.setText("Subtotal Q.");
        jPanel2.add(jLabel10);
        jLabel10.setBounds(10, 160, 110, 20);

        t_pag1.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        t_pag1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanel2.add(t_pag1);
        t_pag1.setBounds(940, 160, 200, 20);

        jLabel11.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        jLabel11.setText("Pendiente Q.");
        jPanel2.add(jLabel11);
        jLabel11.setBounds(830, 160, 120, 20);

        panel1.setBackground(new java.awt.Color(255, 255, 255));
        panel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());
        jPanel2.add(panel1);
        panel1.setBounds(90, 500, 170, 70);

        desc.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        desc.setText("0");
        desc.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        desc.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                descActionPerformed(evt);
            }
        });
        desc.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                descKeyReleased(evt);
            }
        });
        jPanel2.add(desc);
        desc.setBounds(430, 160, 120, 20);

        jLabel27.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        jLabel27.setText("Descuento Q.");
        jPanel2.add(jLabel27);
        jLabel27.setBounds(320, 160, 110, 20);

        sel1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                sel1ActionPerformed(evt);
            }
        });
        jPanel2.add(sel1);
        sel1.setBounds(290, 160, 19, 19);

        sel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                selActionPerformed(evt);
            }
        });
        jPanel2.add(sel);
        sel.setBounds(560, 160, 19, 19);

        jLabel26.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        jLabel26.setText("Abono Q.");
        jPanel2.add(jLabel26);
        jLabel26.setBounds(590, 160, 90, 20);

        abon.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        abon.setText("0");
        abon.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        abon.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                abonKeyReleased(evt);
            }
        });
        jPanel2.add(abon);
        abon.setBounds(680, 160, 140, 20);

        getContentPane().add(jPanel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 410, 1240, 230));

        jMenuBar1.setBorder(null);
        jMenuBar1.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jMenuBar1.setFont(new java.awt.Font("Segoe UI", 2, 12)); // NOI18N

        jMenu1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/can.png"))); // NOI18N
        jMenu1.setText("LOGIN");

        jMenu3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Fondo.png"))); // NOI18N
        jMenu3.setText("LOGIN");
        jMenu3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jMenu3MouseClicked(evt);
            }
        });
        jMenu1.add(jMenu3);

        jMenuBar1.add(jMenu1);
        jMenuBar1.add(jMenu2);

        jMenu4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/add (1).png"))); // NOI18N
        jMenu4.setText(" REGISTRO");

        r_pr.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_R, java.awt.event.InputEvent.CTRL_DOWN_MASK));
        r_pr.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/ingreso.png"))); // NOI18N
        r_pr.setText("PRODUCTO");
        r_pr.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                r_prActionPerformed(evt);
            }
        });
        jMenu4.add(r_pr);

        r_em.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_E, java.awt.event.InputEvent.CTRL_DOWN_MASK));
        r_em.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/emple.png"))); // NOI18N
        r_em.setText("EMPLEADOS");
        r_em.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                r_emActionPerformed(evt);
            }
        });
        jMenu4.add(r_em);

        r_fa.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F, java.awt.event.InputEvent.CTRL_DOWN_MASK));
        r_fa.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/REGISTRO FACTURA.png"))); // NOI18N
        r_fa.setText("FACTURA");
        r_fa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                r_faActionPerformed(evt);
            }
        });
        jMenu4.add(r_fa);

        r_pro.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_P, java.awt.event.InputEvent.CTRL_DOWN_MASK));
        r_pro.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/proveNue.png"))); // NOI18N
        r_pro.setText("PROVEEDOR");
        r_pro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                r_proActionPerformed(evt);
            }
        });
        jMenu4.add(r_pro);

        r_cli.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_C, java.awt.event.InputEvent.CTRL_DOWN_MASK));
        r_cli.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/cliente.png"))); // NOI18N
        r_cli.setText("CLIENTE");
        r_cli.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                r_cliActionPerformed(evt);
            }
        });
        jMenu4.add(r_cli);

        jMenuBar1.add(jMenu4);

        jMenu6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Busqueda (1).png"))); // NOI18N
        jMenu6.setText("BUSQUEDA");

        b_pro.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_P, java.awt.event.InputEvent.ALT_DOWN_MASK));
        b_pro.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/prodddd.png"))); // NOI18N
        b_pro.setText("PRODUCTOS");
        b_pro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                b_proActionPerformed(evt);
            }
        });
        jMenu6.add(b_pro);

        b_emp.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_E, java.awt.event.InputEvent.ALT_DOWN_MASK));
        b_emp.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/busPer .png"))); // NOI18N
        b_emp.setText("EMPLEADOS");
        b_emp.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                b_empActionPerformed(evt);
            }
        });
        jMenu6.add(b_emp);

        b_fac.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F, java.awt.event.InputEvent.ALT_DOWN_MASK));
        b_fac.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/busqueda_factura.png"))); // NOI18N
        b_fac.setText("FACTURA");
        b_fac.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                b_facActionPerformed(evt);
            }
        });
        jMenu6.add(b_fac);

        b_cli.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_C, java.awt.event.InputEvent.ALT_DOWN_MASK));
        b_cli.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/bus_em.png"))); // NOI18N
        b_cli.setText("CLIENTE");
        b_cli.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                b_cliActionPerformed(evt);
            }
        });
        jMenu6.add(b_cli);

        b_ventas.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_Z, java.awt.event.InputEvent.ALT_DOWN_MASK));
        b_ventas.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Busqueda1.png"))); // NOI18N
        b_ventas.setText("VENTAS");
        b_ventas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                b_ventasActionPerformed(evt);
            }
        });
        jMenu6.add(b_ventas);

        jMenuBar1.add(jMenu6);

        modificacion.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/modificacion.png"))); // NOI18N
        modificacion.setText("MODIFICAR");

        m_pro.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_K, java.awt.event.InputEvent.ALT_DOWN_MASK));
        m_pro.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/PRODUCTO.png"))); // NOI18N
        m_pro.setText("PRODUCTO");
        m_pro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                m_proActionPerformed(evt);
            }
        });
        modificacion.add(m_pro);

        m_emp.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_E, java.awt.event.InputEvent.SHIFT_DOWN_MASK));
        m_emp.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/empleado.png"))); // NOI18N
        m_emp.setText("EMPLEADO");
        m_emp.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                m_empActionPerformed(evt);
            }
        });
        modificacion.add(m_emp);

        m_cli.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_C, java.awt.event.InputEvent.SHIFT_DOWN_MASK));
        m_cli.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/clientee.png"))); // NOI18N
        m_cli.setText("CLIENTE");
        m_cli.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                m_cliActionPerformed(evt);
            }
        });
        modificacion.add(m_cli);

        m_prov.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_P, java.awt.event.InputEvent.SHIFT_DOWN_MASK));
        m_prov.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/proveedores.png"))); // NOI18N
        m_prov.setText("PROVEEDORES");
        m_prov.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                m_provActionPerformed(evt);
            }
        });
        modificacion.add(m_prov);

        jMenuBar1.add(modificacion);

        devolucion.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/DEV.png"))); // NOI18N
        devolucion.setText("DEVOULUCION");

        dev.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_D, java.awt.event.InputEvent.CTRL_DOWN_MASK));
        dev.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/dev1.png"))); // NOI18N
        dev.setText("DEVOLUCION");
        dev.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                devActionPerformed(evt);
            }
        });
        devolucion.add(dev);

        jMenuBar1.add(devolucion);

        jMenu7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/png-transparent-shopping-cart-graphy-cart-supermarket-vehicle-shopping-bags-trolleys (1) (1).png"))); // NOI18N
        jMenu7.setText("VENTA");

        ven.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_V, java.awt.event.InputEvent.CTRL_DOWN_MASK));
        ven.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/venta a credito.png"))); // NOI18N
        ven.setText("VENTA  A CREDITO");
        ven.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                venActionPerformed(evt);
            }
        });
        jMenu7.add(ven);

        venE.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_B, java.awt.event.InputEvent.CTRL_DOWN_MASK));
        venE.setIcon(new javax.swing.ImageIcon(getClass().getResource("/main/venta efectivo.png"))); // NOI18N
        venE.setText("VENTA EN EFECTIVO");
        venE.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                venEActionPerformed(evt);
            }
        });
        jMenu7.add(venE);

        jMenuBar1.add(jMenu7);

        ea.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/REPORTE.png"))); // NOI18N
        ea.setText("REPORTE");

        rep.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_R, java.awt.event.InputEvent.ALT_DOWN_MASK));
        rep.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/reportess.png"))); // NOI18N
        rep.setText(" REPORTES");
        rep.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                repActionPerformed(evt);
            }
        });
        ea.add(rep);

        jMenuBar1.add(ea);

        jMenu5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Pago_C_P.png"))); // NOI18N
        jMenu5.setText("PAGOS");

        r_pago.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Pago_C_P (2).png"))); // NOI18N
        r_pago.setText("REALIZAR PAGOS");
        r_pago.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                r_pagoActionPerformed(evt);
            }
        });
        jMenu5.add(r_pago);

        jMenuBar1.add(jMenu5);

        setJMenuBar(jMenuBar1);

        pack();
    }// </editor-fold>//GEN-END:initComponents
 //filtro por nombre de producto

    void filtro_nombre_Producto() {
        int ColumntaTabla = 1;
        filtro.setRowFilter(RowFilter.regexFilter(bus_prod.getText(), ColumntaTabla));
    }
    //filtro por nombre de producto

    void filtro_Descripción_Producto() {
        int ColumntaTabla = 2;
        filtro.setRowFilter(RowFilter.regexFilter(bus_desc.getText(), ColumntaTabla));
    }
    //filtro por nombre de producto

    void filtro_Marca_Producto() {
        int ColumntaTabla = 3;
        filtro.setRowFilter(RowFilter.regexFilter(bus_marc.getText(), ColumntaTabla));
    }
   //filtro por nombre de producto

    void filtro_nombre_cliente() {
        int ColumntaTabla = 1;
        filtro.setRowFilter(RowFilter.regexFilter(busqueda.getText(), ColumntaTabla));
    }
   //filtro por nombre de producto

    void filtro_apellido_cliente() {
        int ColumntaTabla = 2;
        filtro.setRowFilter(RowFilter.regexFilter(busqueda1.getText(), ColumntaTabla));
    }

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
   Thread thread = new Thread() {
            public void run() {
        if (m_nom.getText().equals("")) {//si no ingresaran cliente
            Icon m = new ImageIcon(getClass().getResource("/img/Fondo.png"));
            JOptionPane.showMessageDialog(null,
                    "<html><h1 style=\"color:red;\">NO SE HA SELECCIONADO UN CLIENTE</h1></html>", "Error", JOptionPane.INFORMATION_MESSAGE, m);

        } else {
            f.setEmpleado(vendedor.getText());
            f.setNombre(m_nom.getText());
            f.setTotal(t_pag.getText());
            f.setDescuento(desc.getText());
            f.setPendiente(t_pag1.getText());
            f.setAbono(abon.getText());
            f.setTot_con_desc(Float.toString(Float.parseFloat(t_pag.getText()) - Float.parseFloat(desc.getText())));
            if (check.isSelected()) {
                Insertar_Domicilio();
                Consulta_ID_Domicilio();
                Registro_Venta(id_domicilio);
                Consulta_id_Venta();
                registro_detalle_Venta_Credito();

            } else {
                Registro_Venta(1);
                Consulta_id_Venta();
                registro_detalle_Venta_Credito();
            }

            Insertar_Pago();

            busqueda.setText("");
            busqueda1.setText("");
            bus_prod.setText("");
            bus_desc.setText("");
            bus_marc.setText("");
            m_nom.setText("");
            m_ap.setText("");
            m_dir.setText("");
            desc.setText("");
            t_pag.setText("");
            t_pag1.setText("");
            sum = 0;
            resta = 0;
            abon.setText("0");
            desc.setText("0");
            borrar(modelo);
            borrar(modelo2);
            borrar(modelo1);
            consulta_prod_registro_fac();
            Mostrar_Clientes();
            f.setId_venta(Id_venta);
            Comprobante c = new Comprobante();
            c.setVisible(true);
         
        }
          
        }
        };
        thread.start();
    }//GEN-LAST:event_jButton2ActionPerformed
    public void suma() {//Sumo la tabla de producto
        double suma = 0;
        double suma1 = 0;

        for (int i = 0; i < tabladatos3.getRowCount(); i++) {

            suma = Double.parseDouble(tabladatos3.getValueAt(i, 7).toString());
            suma1 += suma;

        }
        t_pag.setText(Double.toString(suma1));
        t_pag1.setText(Double.toString(suma1));
    }
    private void m_apKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_m_apKeyPressed

    }//GEN-LAST:event_m_apKeyPressed

    private void m_nomKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_m_nomKeyPressed

    }//GEN-LAST:event_m_nomKeyPressed

    private void busquedaKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_busquedaKeyReleased
       
    }//GEN-LAST:event_busquedaKeyReleased

    private void busquedaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_busquedaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_busquedaActionPerformed

    private void bus_descActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bus_descActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_bus_descActionPerformed

    private void bus_prodActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bus_prodActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_bus_prodActionPerformed

    private void busqueda1KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_busqueda1KeyReleased
      
    }//GEN-LAST:event_busqueda1KeyReleased

    private void busqueda1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_busqueda1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_busqueda1ActionPerformed

    private void tabladatos1MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tabladatos1MousePressed
        int seleccion = tabladatos1.rowAtPoint(evt.getPoint());
        Id_cliente = Integer.parseInt(String.valueOf(tabladatos1.getValueAt(seleccion, 0)));
        m_nom.setText(String.valueOf(tabladatos1.getValueAt(seleccion, 1)));
        m_ap.setText(String.valueOf(tabladatos1.getValueAt(seleccion, 2)));
        m_dir.setText(String.valueOf(tabladatos1.getValueAt(seleccion, 4)));


    }//GEN-LAST:event_tabladatos1MousePressed

    private void bus_marcKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_bus_marcKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_bus_marcKeyPressed

    private void bus_marcKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_bus_marcKeyReleased

        
    }//GEN-LAST:event_bus_marcKeyReleased

    private void bus_prodKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_bus_prodKeyReleased
      
    }//GEN-LAST:event_bus_prodKeyReleased

    private void bus_descKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_bus_descKeyReleased
       
    }//GEN-LAST:event_bus_descKeyReleased

    private void tabladatos2MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tabladatos2MousePressed
         Thread thread = new Thread() {
            public void run() {
  int seleccion = tabladatos2.rowAtPoint(evt.getPoint());
        codigoRecivido = Integer.parseInt(String.valueOf(tabladatos2.getValueAt(seleccion, 0)));
        Consulta_Imagen();
        Id_E = String.valueOf(tabladatos2.getValueAt(seleccion, 0));
        Nombre_E = String.valueOf(tabladatos2.getValueAt(seleccion, 1));
        Descripcion_E = String.valueOf(tabladatos2.getValueAt(seleccion, 2));
        Marca_E = String.valueOf(tabladatos2.getValueAt(seleccion, 3));
        Precio_E = String.valueOf(tabladatos2.getValueAt(seleccion, 5));
        Estado_E = String.valueOf(tabladatos2.getValueAt(seleccion, 6));
        abon.setText("0");
        desc.setText("0");
            }
        };

        thread.start();
      
    }//GEN-LAST:event_tabladatos2MousePressed

    private void addActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addActionPerformed

        entrada = JOptionPane.showInputDialog("<html><h1>Escriba la cantidad<h1></html>");

        if (entrada == null) {
            System.out.println("La operacion ha sido cancelada");
        } else {

            if (entrada.equals("")) {
                JOptionPane.showMessageDialog(null, "<html><h1>Vuelva a ingresar el Cantidad<h1></html>", "ERROR", JOptionPane.ERROR_MESSAGE);
                entrada1 = JOptionPane.showInputDialog("<html><h1>Escriba la cantidad<h1></html>");

                //
                //Eliminar filas repetidas
                int cuentaFilasSeleccionadas = tabladatos1.getSelectedRowCount();
                float subtotal = 0;
                boolean ver = false;
                if (tabladatos1.getRowCount() >= 0) {

                    for (int i = 0; i < modelo2.getRowCount(); i++) {
                        if (Integer.parseInt((String) tabladatos3.getValueAt(i, 0)) == Integer.parseInt(Id_E)) {
                            modelo2.removeRow(i);
                            JOptionPane.showMessageDialog(null, "<html><h1>PRODUCTO REPETIDO !!!!!!<br>POR FAVOR INGRESE NUEVAMENTE<h1></html>", "ERROR", JOptionPane.ERROR_MESSAGE);
                            suma();
                            ver = true;//si el producto es repetido se activa true
                        } else {

                        }
                    }

                }
                //
                if (ver == false) {//

                    try {
                        cantidad = Integer.parseInt(entrada1);
                        Float Resultado = null;

                        Resultado = Float.parseFloat(Precio_E) * cantidad;

                        String[] datos = new String[9];
                        datos[0] = Id_E;
                        datos[1] = Nombre_E;
                        datos[2] = Descripcion_E;
                        datos[3] = Marca_E;
                        datos[4] = Precio_E;
                        datos[5] = Estado_E;
                        datos[6] = Integer.toString(cantidad);
                        datos[7] = Float.toString(Resultado);
                        modelo2.addRow(datos);
                        suma();
                    } catch (Exception e) {
                        System.out.println("Error" + e);
                    }

                } else {

                }
                ///

            } else {
                cantidad = Integer.parseInt(entrada);
                Float Resultado = null;
                //Eliminar filas repetidas
                int cuentaFilasSeleccionadas = tabladatos1.getSelectedRowCount();
                float subtotal = 0;
                boolean ver = false;
                if (tabladatos1.getRowCount() >= 0) {

                    for (int i = 0; i < modelo2.getRowCount(); i++) {
                        if (Integer.parseInt((String) tabladatos3.getValueAt(i, 0)) == Integer.parseInt(Id_E)) {
                            modelo2.removeRow(i);
                            JOptionPane.showMessageDialog(null, "<html><h1>PRODUCTO REPETIDO !!!!!!<br>POR FAVOR INGRESE NUEVAMENTE<h1></html>", "ERROR", JOptionPane.ERROR_MESSAGE);
                            suma();
                            ver = true;//si el producto es repetido se activa true
                        } else {

                        }
                    }

                }
                if (ver == false) {//

                    try {

                        Resultado = Float.parseFloat(Precio_E) * cantidad;

                        String[] datos = new String[9];
                        datos[0] = Id_E;
                        datos[1] = Nombre_E;
                        datos[2] = Descripcion_E;
                        datos[3] = Marca_E;
                        datos[4] = Precio_E;
                        datos[5] = Estado_E;
                        datos[6] = Integer.toString(cantidad);
                        datos[7] = Float.toString(Resultado);
                        modelo2.addRow(datos);
                        suma();
                    } catch (Exception e) {
                        System.out.println("Error" + e);
                    }

                } else {

                }
                ///

            }

        }
    }//GEN-LAST:event_addActionPerformed

    private void tabladatos3MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tabladatos3MousePressed
        int seleccion = tabladatos3.rowAtPoint(evt.getPoint());
        resta = Float.parseFloat(String.valueOf(tabladatos3.getValueAt(seleccion, 7)));
    }//GEN-LAST:event_tabladatos3MousePressed

    private void checkMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_checkMouseClicked


    }//GEN-LAST:event_checkMouseClicked

    private void combustibleActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_combustibleActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_combustibleActionPerformed

    private void direActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_direActionPerformed
        // combustible.setEditable(false);
    }//GEN-LAST:event_direActionPerformed

    private void abonKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_abonKeyReleased
        if (sel.isSelected()) {
            try {
                if (Float.parseFloat(t_pag.getText()) - Float.parseFloat(desc.getText()) - Float.parseFloat(abon.getText()) < 0) {
                    JOptionPane.showMessageDialog(null,
                            "<html><h1>ABONO NO ACEPTADO</h1></html>",
                            "DESCUENTO", JOptionPane.QUESTION_MESSAGE);
                    abon.setText("");
                    t_pag1.setText("");
                } else {
                    t_pag1.setText(Float.toString(Float.parseFloat(t_pag.getText()) - Float.parseFloat(desc.getText()) - Float.parseFloat(abon.getText())));

                }

            } catch (Exception e) {
                System.out.println(e);
                t_pag1.setText("");

                t_pag1.setText(Float.toString(Float.parseFloat(t_pag.getText()) - Float.parseFloat(desc.getText())));
            }
        }

    }//GEN-LAST:event_abonKeyReleased

    private void selActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_selActionPerformed
        try {

            if (desc.getText().equals("")) {
                JOptionPane.showMessageDialog(null, "Por favor desmargar el descuento para activar el abono");
            } else {
                if (sel.isSelected()) {
                    abon.setText("");
                    abon.setEnabled(true);
                } else {
                    abon.setEnabled(false);
                    abon.setText("0");

                    t_pag1.setText("");
                    t_pag1.setText(t_pag.getText());
                    t_pag1.setText(Float.toString(Float.parseFloat(t_pag.getText()) - Float.parseFloat(desc.getText()) - Float.parseFloat(abon.getText())));

                }
            }
        } catch (Exception e) {
        }
    }//GEN-LAST:event_selActionPerformed

    private void checkActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_checkActionPerformed
        if (check.isSelected()) {
            dire.setEnabled(true);
            combustible.setEnabled(true);
        } else {
            dire.setEnabled(false);
            combustible.setEnabled(false);
        }
    }//GEN-LAST:event_checkActionPerformed

    private void descKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_descKeyReleased

        if (sel1.isSelected()) {
            try {
                if (Float.parseFloat(t_pag.getText()) - Float.parseFloat(desc.getText()) - Float.parseFloat(abon.getText()) < 0) {
                    JOptionPane.showMessageDialog(null,
                            "<html><h1>DESCUENTO NO ACEPTADO</h1></html>",
                            "DESCUENTO", JOptionPane.QUESTION_MESSAGE);
                    desc.setText("");
                    t_pag1.setText("");
                } else {
                    t_pag1.setText(Float.toString(Float.parseFloat(t_pag.getText()) - Float.parseFloat(desc.getText()) - Float.parseFloat(abon.getText())));

                }

            } catch (Exception e) {
                System.out.println(e);

                t_pag1.setText("");

                t_pag1.setText(Float.toString(Float.parseFloat(t_pag.getText()) - Float.parseFloat(abon.getText())));

            }

        }
    }//GEN-LAST:event_descKeyReleased

    private void sel1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_sel1ActionPerformed
        if (abon.getText().equals("")) {
            JOptionPane.showMessageDialog(null, "DESMARCAR EL ABONO");
        } else {
            try {

                if (sel1.isSelected()) {
                    desc.setText("");
                    desc.setEnabled(true);
                } else {
                    desc.setEnabled(false);
                    desc.setText("0");

                    t_pag1.setText("");
                    t_pag1.setText(t_pag.getText());
                    t_pag1.setText(Float.toString(Float.parseFloat(t_pag.getText()) - Float.parseFloat(desc.getText()) - Float.parseFloat(abon.getText())));

                }
            } catch (Exception e) {
            }
        }
    }//GEN-LAST:event_sel1ActionPerformed

    private void descActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_descActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_descActionPerformed

    private void jMenu3MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jMenu3MouseClicked
        Login l = new Login();
        l.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_jMenu3MouseClicked

    private void r_prActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_r_prActionPerformed
        R_PROD a = new R_PROD();
        a.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_r_prActionPerformed

    private void r_emActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_r_emActionPerformed
        R_EMPL a = new R_EMPL();
        a.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_r_emActionPerformed

    private void r_faActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_r_faActionPerformed
        R_FACTURA a = new R_FACTURA();
        a.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_r_faActionPerformed

    private void r_proActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_r_proActionPerformed
        R_PROVEE a = new R_PROVEE();
        a.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_r_proActionPerformed

    private void r_cliActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_r_cliActionPerformed

        R_CLI A = new R_CLI();
        A.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_r_cliActionPerformed

    private void b_proActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_b_proActionPerformed
        Busqueda_Productos a = new Busqueda_Productos();
        a.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_b_proActionPerformed

    private void b_empActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_b_empActionPerformed
        Busqueda_Empleados a = new Busqueda_Empleados();
        a.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_b_empActionPerformed

    private void b_facActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_b_facActionPerformed
        Busqueda_Facturas a = new Busqueda_Facturas();
        a.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_b_facActionPerformed

    private void b_cliActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_b_cliActionPerformed
        Busqueda_Clientes a = new Busqueda_Clientes();
        a.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_b_cliActionPerformed

    private void b_ventasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_b_ventasActionPerformed
        Busqueda_Venta P = new Busqueda_Venta();
        this.setVisible(false);
        P.setVisible(true);
    }//GEN-LAST:event_b_ventasActionPerformed

    private void m_proActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_m_proActionPerformed
        m_prod a = new m_prod();
        a.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_m_proActionPerformed

    private void m_empActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_m_empActionPerformed
        m_empl a = new m_empl();
        a.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_m_empActionPerformed

    private void m_cliActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_m_cliActionPerformed
        m_cliente a = new m_cliente();
        a.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_m_cliActionPerformed

    private void m_provActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_m_provActionPerformed
        m_prov a = new m_prov();
        a.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_m_provActionPerformed

    private void devActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_devActionPerformed
        Devolucion l = new Devolucion();
        l.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_devActionPerformed

    private void venActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_venActionPerformed
        Ventas_Cred a = new Ventas_Cred();
        a.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_venActionPerformed

    private void venEActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_venEActionPerformed
        Ventas a = new Ventas();
        a.setVisible(true);
        this.setVisible(false);

    }//GEN-LAST:event_venEActionPerformed

    private void repActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_repActionPerformed
        Reportes r = new Reportes();
        r.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_repActionPerformed

    private void r_pagoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_r_pagoActionPerformed
        PAGO P = new PAGO();
        this.setVisible(false);
        P.setVisible(true);
    }//GEN-LAST:event_r_pagoActionPerformed

    private void jButton7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton7ActionPerformed
        System.exit(0);
    }//GEN-LAST:event_jButton7ActionPerformed

    private void bus_prodKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_bus_prodKeyTyped
char c=evt.getKeyChar();
     if(Character.isDigit(c)){
       
          evt.consume();
      }
        //borrar(modelo1);
        bus_prod.addKeyListener(new KeyAdapter() {
            public void keyReleased(final KeyEvent e) {
                String cadena = (bus_prod.getText().toUpperCase());
                bus_prod.setText(cadena);
                filtro_nombre_Producto();
            }

        });
        filtro = new TableRowSorter(tabladatos2.getModel());
        tabladatos2.setRowSorter(filtro);
    }//GEN-LAST:event_bus_prodKeyTyped

    private void bus_descKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_bus_descKeyTyped
            bus_desc.addKeyListener(new KeyAdapter() {
            public void keyReleased(final KeyEvent e) {
                String cadena = (bus_desc.getText().toUpperCase());
                bus_desc.setText(cadena);
                filtro_Descripción_Producto();
            }

        });
        filtro = new TableRowSorter(tabladatos2.getModel());
        tabladatos2.setRowSorter(filtro);
    }//GEN-LAST:event_bus_descKeyTyped

    private void bus_marcKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_bus_marcKeyTyped
        bus_marc.addKeyListener(new KeyAdapter() {
            public void keyReleased(final KeyEvent e) {
                String cadena = (bus_marc.getText().toUpperCase());
                bus_marc.setText(cadena);
                filtro_Marca_Producto();
            }

        });
        filtro = new TableRowSorter(tabladatos2.getModel());
        tabladatos2.setRowSorter(filtro);
    }//GEN-LAST:event_bus_marcKeyTyped

    private void busqueda1KeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_busqueda1KeyTyped

char c=evt.getKeyChar();
     if(Character.isDigit(c)){
       
          evt.consume();
      }        
//borrar(modelo1);
         busqueda1.addKeyListener(new KeyAdapter() {
            public void keyReleased(final KeyEvent e) {
                String cadena = ( busqueda1.getText().toUpperCase());
                 busqueda1.setText(cadena);
                filtro_apellido_cliente();
            }

        });
        filtro = new TableRowSorter(tabladatos1.getModel());
        tabladatos1.setRowSorter(filtro);
    }//GEN-LAST:event_busqueda1KeyTyped

    private void busquedaKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_busquedaKeyTyped

char c=evt.getKeyChar();
     if(Character.isDigit(c)){
       
          evt.consume();
      }
        //borrar(modelo1);
         busqueda.addKeyListener(new KeyAdapter() {
            public void keyReleased(final KeyEvent e) {
                String cadena = ( busqueda.getText().toUpperCase());
                 busqueda.setText(cadena);
                filtro_nombre_cliente();
            }

        });
        filtro = new TableRowSorter(tabladatos1.getModel());
        tabladatos1.setRowSorter(filtro);
    }//GEN-LAST:event_busquedaKeyTyped

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Ventas_Cred.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Ventas_Cred.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Ventas_Cred.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Ventas_Cred.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Ventas_Cred().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPopupMenu Agregar;
    private javax.swing.JTextField abon;
    private javax.swing.JMenuItem add;
    private javax.swing.JMenuItem b_cli;
    private javax.swing.JMenuItem b_emp;
    private javax.swing.JMenuItem b_fac;
    private javax.swing.JMenuItem b_pro;
    private javax.swing.JMenuItem b_ventas;
    private javax.swing.JTextField bus_desc;
    private javax.swing.JTextField bus_marc;
    private javax.swing.JTextField bus_prod;
    private javax.swing.JTextField busqueda;
    private javax.swing.JTextField busqueda1;
    private javax.swing.JCheckBox check;
    private javax.swing.JTextField combustible;
    private javax.swing.JTextField desc;
    private javax.swing.JMenuItem dev;
    private javax.swing.JMenu devolucion;
    private javax.swing.JTextField dire;
    private javax.swing.JMenu ea;
    private javax.swing.JLabel fecha;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton7;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenu jMenu3;
    private javax.swing.JMenu jMenu4;
    private javax.swing.JMenu jMenu5;
    private javax.swing.JMenu jMenu6;
    private javax.swing.JMenu jMenu7;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator3;
    private javax.swing.JSeparator jSeparator4;
    private javax.swing.JLabel loading;
    private javax.swing.JTextField m_ap;
    private javax.swing.JMenuItem m_cli;
    private javax.swing.JTextField m_dir;
    private javax.swing.JMenuItem m_emp;
    private javax.swing.JTextField m_nom;
    private javax.swing.JMenuItem m_pro;
    private javax.swing.JMenuItem m_prov;
    private javax.swing.JMenu modificacion;
    private javax.swing.JPanel panel1;
    private rojeru_san.componentes.RSLabelFecha rSLabelFecha2;
    private rojeru_san.componentes.RSLabelHora rSLabelHora2;
    private javax.swing.JMenuItem r_cli;
    private javax.swing.JMenuItem r_em;
    private javax.swing.JMenuItem r_fa;
    private javax.swing.JMenuItem r_pago;
    private javax.swing.JMenuItem r_pr;
    private javax.swing.JMenuItem r_pro;
    private javax.swing.JLabel rec_img;
    private javax.swing.JMenuItem rep;
    private javax.swing.JCheckBox sel;
    private javax.swing.JCheckBox sel1;
    private javax.swing.JLabel t_pag;
    private javax.swing.JLabel t_pag1;
    private javax.swing.JTable tabladatos1;
    private javax.swing.JTable tabladatos2;
    private javax.swing.JTable tabladatos3;
    private javax.swing.JMenuItem ven;
    private javax.swing.JMenuItem venE;
    private javax.swing.JLabel vendedor;
    // End of variables declaration//GEN-END:variables
}
