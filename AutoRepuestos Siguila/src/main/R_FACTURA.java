/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import clases.TextPrompt;
import clases.Conexion_postgres;
import clases.Activacion;
import clases.Conexion_mysql;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Blob;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import static java.util.Optional.empty;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;
import javax.swing.RowFilter;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;

/**
 *
 * @author Leo
 */
public class R_FACTURA extends javax.swing.JFrame {
          private TableRowSorter filtro;

    int codigoRecivido, codigoRecivido_Factura;
    String ruta = null;
    Conexion_postgres cn = new Conexion_postgres();
    String Nombre_E, Descripcion_E, Marca_E;
    int contador, existencia;
    DefaultTableModel modelo = new DefaultTableModel();
    DefaultTableModel modelo1 = new DefaultTableModel();
    //variable temporal para mandar en la otra tabla
    String Nombre_prod1, descripcion1, marca, precio_ven, estado;

    public R_FACTURA() {
        initComponents();
             //Primera tabla
tabladatos.setOpaque(false);
jScrollPane2.setOpaque(false);
jScrollPane2.getViewport().setOpaque(false);
      //Segunda tabla
tabladatos1.setOpaque(false);
jScrollPane1.setOpaque(false);
jScrollPane1.getViewport().setOpaque(false);
        loading.setVisible(false);
        loading.setVisible(false);
        this.setLocationRelativeTo(null);

        //no modificar filas tabla
        tabladatos1.setDefaultEditor(Object.class, null);
        tabladatos.setDefaultEditor(Object.class, null);
        Activacion ac = new Activacion();
        Receptor_E.setText(ac.nom_log);
        //se
        //Menu Eliminacion Producos Entrantes
        Productos_Entrantes();
        //Menu Agregar Producos Buscados a tabla productos entrantes
        Productos_Buscados();
        modelo.addColumn("Id_Prod");
        modelo.addColumn("Nombre_Prod");
        modelo.addColumn("Descripcion_Prod");
        modelo.addColumn("Marca");
        modelo.addColumn("Precio_Comp");
        modelo.addColumn("Precio_Ven");
        modelo.addColumn("Estado_Prod");
        modelo.addColumn("Existencia");
        tabladatos.setModel(modelo);
        //tabla de datos 1
        modelo1.addColumn("Id_Prod");
        modelo1.addColumn("Nombre_Prod");
        modelo1.addColumn("Descripcion_Prod");
        modelo1.addColumn("Marca");
        modelo1.addColumn("Precio_Ven");
        modelo1.addColumn("Estado_Prod");
        modelo1.addColumn("Cantidad");
        this.setLocationRelativeTo(null);
        tabladatos1.setModel(modelo1);

        Consulta_proveedores_para_facturua();
//        Consulta_empleado_para_fac();
        this.setLocationRelativeTo(null);
        //mando texto como marca de agua
        TextPrompt p = new TextPrompt("Ingrese Nombre Producto", busqueda);
        TextPrompt q = new TextPrompt("Ingrese Descripcion", busqueda1);
        TextPrompt r = new TextPrompt("Ingrese Marca", busqueda2);

        TextPrompt s = new TextPrompt("Ingrese Numero Factura", No_Factura);
        TextPrompt t = new TextPrompt("Ingrese Total de Factura", Tot_Fac);
        TextPrompt u = new TextPrompt("Ingrese Abono", Abono);
        TextPrompt v = new TextPrompt("Ingrese al Nombre Vendedor", r_vendedor);

//ACTIVACION
        r_pr.setEnabled(false);
        r_pro.setEnabled(false);
        r_em.setEnabled(false);
        r_fa.setEnabled(false);
        r_cli.setEnabled(false);
        b_pro.setEnabled(false);
        b_emp.setEnabled(false);
        b_fac.setEnabled(false);
        b_cli.setEnabled(false);
        ven.setEnabled(false);
        venE.setEnabled(false);
        rep.setEnabled(false);
        dev.setEnabled(false);
        m_pro.setEnabled(false);
        m_emp.setEnabled(false);
        m_cli.setEnabled(false);
        m_prov.setEnabled(false);
        b_ventas.setEnabled(false);
        r_pago.setEnabled(false);
        Activacion b = new Activacion();
        if (b.ver == 1) {
            b.entrante(r_pr, r_pro, r_em, r_fa, r_cli, b_pro, b_emp, b_fac, b_cli, modificacion, devolucion, ven, ea, dev, m_pro,
                    m_emp, m_cli, m_prov, rep, venE, b_ventas, r_pago);
        }
        if (b.ver == 2) {
            b.en(ven, venE, r_fa, b_pro, r_pr, r_pro, r_cli, r_pago, dev, b_cli, b_fac);
        }
        consulta_prod_registro_fac();

    }

    //REGISTRO DE PAGOS FACTURA
    public void Insertar_Fact_Pagos() {

        Conexion_mysql con;
        con = new Conexion_mysql(loading);
        java.sql.Connection cn = con.getConnection();

        String Sql = "call PRC_Pag_Ingreso_Factura(?,?,?)";
        try {
            CallableStatement cs = cn.prepareCall(Sql);
            cs.setInt(1, codigoRecivido_Factura);
            cs.setFloat(2, Float.parseFloat(Abono.getText()));
            cs.setFloat(3, Float.parseFloat(r_pen.getText()));
            if (cs.execute()) {
                JOptionPane.showMessageDialog(null, "Registro Completado");
            }
            cs.close();
        } catch (Exception e) {
            System.out.println(e);  
        }

    }

    //
    //Menu de popMenu para eliminar productos seleccionados producto entrante
    public void Productos_Entrantes() {
        //Sección 1
        // DefaultTableModel model = (DefaultTableModel) tabladatos1.getModel(); 
        JPopupMenu popupMenu = new JPopupMenu();
        JMenuItem menuItem1 = new JMenuItem("Eliminar", new ImageIcon(getClass().getResource("/img/registro.png")));
        menuItem1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                int cuentaFilasSeleccionadas = tabladatos1.getSelectedRowCount();
                if (cuentaFilasSeleccionadas == 0) {
                    JOptionPane.showMessageDialog(null, "No hay filas seleccinada");
                } else {
                    int a = tabladatos1.getSelectedRow();
                    modelo1.removeRow(a);
                }

            }
        });
        popupMenu.add(menuItem1);
        tabladatos1.setComponentPopupMenu(popupMenu);
    }
 //filtro por nombre de producto
   void filtro_nombre_Producto(){
          int ColumntaTabla=1;
     filtro.setRowFilter(RowFilter.regexFilter(busqueda.getText(),ColumntaTabla));
   }
     //filtro por nombre de producto
   void filtro_Descripción_Producto(){
          int ColumntaTabla=2;
     filtro.setRowFilter(RowFilter.regexFilter(busqueda1.getText(),ColumntaTabla));
   }
     //filtro por nombre de producto
   void filtro_Marca_Producto(){
          int ColumntaTabla=3;
     filtro.setRowFilter(RowFilter.regexFilter(busqueda2.getText(),ColumntaTabla));
   }
    //Menu para seleccionar Productos Entrantes
    public void Productos_Buscados() {

        //Sección 1
        // DefaultTableModel model = (DefaultTableModel) tabladatos1.getModel(); 
        JPopupMenu popupMenu = new JPopupMenu();
        JMenuItem menuItem1 = new JMenuItem("Agregar", new ImageIcon(getClass().getResource("/img/new.png")));
        menuItem1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                try {
                    String[] datos = new String[9];
                    String a = JOptionPane.showInputDialog("Cantidad");

                    if (a == null) {
                        System.out.println("La operacion ha sido cancelada");
                    } else {
                        if (a.equals("")) {
                            if (!(Integer.parseInt(a) <= 0)) {
                                JOptionPane.showMessageDialog(null, "Vuelva a ingressar la cantidad", "ERROR", JOptionPane.ERROR_MESSAGE);
                                String a1 = JOptionPane.showInputDialog("Cantidad");
                                datos[0] = Integer.toString(codigoRecivido);
                                datos[1] = Nombre_prod1;
                                datos[2] = descripcion1;
                                datos[3] = marca;
                                datos[4] = precio_ven;
                                datos[5] = estado;
                                datos[6] = a1;
                                modelo1.addRow(datos);
                            } else {
                                JOptionPane.showMessageDialog(null, "No se puede ingresar cantidad<0", "ERROR", JOptionPane.ERROR_MESSAGE);

                            }
                        } else {
                            if (!(Integer.parseInt(a) <= 0)) {
                                datos[0] = Integer.toString(codigoRecivido);
                                datos[1] = Nombre_prod1;
                                datos[2] = descripcion1;
                                datos[3] = marca;
                                datos[4] = precio_ven;
                                datos[5] = estado;
                                datos[6] = a;
                                modelo1.addRow(datos);
                            } else {
                                JOptionPane.showMessageDialog(null, "No se puede ingresar cantidad<0", "ERROR", JOptionPane.ERROR_MESSAGE);

                            }
                        }

                    }
                } catch (Exception e) {
                    System.out.println(e);  
                    JOptionPane.showMessageDialog(null, "Cantidad en letra invalido", "ERROR", JOptionPane.ERROR_MESSAGE);

                }
            }

        });
        popupMenu.add(menuItem1);

        tabladatos.setComponentPopupMenu(popupMenu);

    }

    public void borrar(DefaultTableModel tabla) {
        while (tabla.getRowCount() > 0) {
            tabla.removeRow(0);
        }

    }

    public void Consulta_proveedores_para_facturua() {

        Conexion_mysql con;
        con = new Conexion_mysql(loading);
        java.sql.Connection cn = con.getConnection();

        String readMetaData = "CALL PRC_con_provee()";
        try {
            cn.setAutoCommit(false); // This line must be written just after the 

            CallableStatement cs = cn.prepareCall(readMetaData);

            //cs.registerOutParameter(1, Types.REF_CURSOR);
            //cs.execute();
            ResultSet rs = (ResultSet) cs.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    Cod_empresa.addItem(rs.getString(1).toUpperCase());

                    empresa.addItem(rs.getString(2).toUpperCase().toUpperCase());
                }//end while
            }//end if
        } catch (SQLException ex) {
            Logger.getLogger(R_FACTURA.class.getName()).log(Level.SEVERE, null, ex);
        }

    }//end if

    /*   public void Consulta_empleado_para_fac(){   
    cn.conectar();
    String readMetaData = "CALL con_per_reg_fac(?)";
     try {
         cn.cn.setAutoCommit(false); // This line must be written just after the 

         CallableStatement cs= cn.cn.prepareCall(readMetaData);
         
         cs.registerOutParameter(1, Types.REF_CURSOR);
cs.execute();
ResultSet rs = (ResultSet) cs.getObject(1);
if (rs != null) {
    while (rs.next()) {       
       receptor.addItem(rs.getString(1).toUpperCase().toUpperCase());

                }//end while
            }//end if
     } catch (SQLException ex) {
         Logger.getLogger(R_FACTURA.class.getName()).log(Level.SEVERE, null, ex);
     }


}//end if*/
    public void Consulta_Imagen() {
        //Variables para la imagen
        InputStream is;
        ImageIcon foto;

        Conexion_mysql con;
        con = new Conexion_mysql(loading);
        java.sql.Connection cn = con.getConnection();

        String readMetaData = "CALL PRC_Con_Imag_Re_Fac(?) ";
        try {
            cn.setAutoCommit(false); // This line must be written just after the 

            CallableStatement cs = cn.prepareCall(readMetaData);
            cs.setInt(1, codigoRecivido);

            //cs.registerOutParameter(2, Types.REF_CURSOR);
            //cs.execute();
            ResultSet rs = (ResultSet) cs.executeQuery();
            if (rs != null) {
                while (rs.next()) {

//PROCESO PARA CONSULTAR IMAGEN
                    is = rs.getBinaryStream(1);
                    try {
                        BufferedImage bi = ImageIO.read(is);
                        foto = new ImageIcon(bi);
                        Image img = foto.getImage();
                        Image newimg = img.getScaledInstance(210, 190,
                                java.awt.Image.SCALE_SMOOTH);
                        ImageIcon newicon = new ImageIcon(newimg);
                        rec_img.setIcon(newicon);//enviarlo a un jlabel    
                    } catch (IOException ex) {
                        Logger.getLogger(R_FACTURA.class.getName()).log(Level.SEVERE, null, ex);
                    }

//TERMINACION DE PROCESO PARA CONSULTAR IMAGEN
                }//end while
            }//end if
        } catch (SQLException ex) {
            Logger.getLogger(R_FACTURA.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void consulta_prod_registro_fac() {

        String[] datos = new String[9];

        Conexion_mysql con;
        con = new Conexion_mysql(loading);
        java.sql.Connection cn = con.getConnection();

        String readMetaData = "CALL PRC_Cons_Prod_Reg_Fac() ";
        try {
            cn.setAutoCommit(false); // This line must be written just after the 

            CallableStatement cs = cn.prepareCall(readMetaData);

            //cs.registerOutParameter(4, Types.REF_CURSOR);
            //cs.execute();
            ResultSet rs = (ResultSet) cs.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    datos[0] = rs.getString(1).toUpperCase();
                    datos[1] = rs.getString(2).toUpperCase();
                    datos[2] = rs.getString(3).toUpperCase();
                    datos[3] = rs.getString(4).toUpperCase();
                    datos[4] = rs.getString(5).toUpperCase();
                    datos[5] = rs.getString(6).toUpperCase();
                    datos[6] = rs.getString(7).toUpperCase();
                    datos[7] = rs.getString(8).toUpperCase();
//PROCESO PARA CONSULTAR IMAGEN
//TERMINACION DE PROCESO PARA CONSULTAR IMAGEN
                    modelo.addRow(datos);

                }//end while
            }//end if
        } catch (SQLException ex) {
            Logger.getLogger(R_FACTURA.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
//REGISTRO FACTURA

    public void Insertar_Factura() {
        Conexion_mysql con;
        con = new Conexion_mysql(loading);
        java.sql.Connection cn = con.getConnection();

        Activacion ac = new Activacion();
        Float total = Float.parseFloat(Tot_Fac.getText());

        try {
            String Sql = "call PRC_re_ingreso_fact(?,?,?,?,?,?)";
            CallableStatement cs = cn.prepareCall(Sql);
            cs.setString(1, No_Factura.getText().toUpperCase());
            cs.setInt(2, Integer.parseInt(Cod_empresa.getSelectedItem().toString()));
            cs.setFloat(3, total);
            cs.setString(4, ac.nom_log);
            cs.setString(5, r_vendedor.getText().toUpperCase());
            cs.setString(6, "PENDIENTE");
            if (cs.execute()) {
                JOptionPane.showMessageDialog(null, "Registro Completado");
            }
            cs.close();
        } catch (Exception e) {
         System.out.println(e);  
        }
    }

    public void registro_detalle_factura() {
        Conexion_mysql con;
        con = new Conexion_mysql(loading);
        java.sql.Connection cn = con.getConnection();

        if (tabladatos1.getRowCount() > 0) {

            try {
                String Sql = "call PRC_Insersion_Detalle_Factura(?,?,?)";
                for (int i = 0; i < tabladatos1.getRowCount(); i++) {
                    CallableStatement cs = cn.prepareCall(Sql);

                    cs.setInt(1, codigoRecivido_Factura);
                    cs.setInt(2, Integer.parseInt(tabladatos1.getValueAt(i, 0).toString()));
                    cs.setInt(3, Integer.parseInt(tabladatos1.getValueAt(i, 6).toString()));
                    if (cs.execute()) {
                        JOptionPane.showMessageDialog(null, "Registro Completado");
                    }
                }
                ///

                // cs.close();              
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, "<html><h1>Ingreso en campo cantidad o precio en tipo letra <p> <h2>Favor revisarlo</h2></h1></html>");

System.out.println(e);            }
            //

        } else {
            JOptionPane.showMessageDialog(this, "La tabla se encuentra vacio");
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        loading = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        No_Factura = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        Abono = new javax.swing.JTextField();
        r_vendedor = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        r_pen = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tabladatos1 = new javax.swing.JTable();
        jScrollPane2 = new javax.swing.JScrollPane();
        tabladatos = new javax.swing.JTable();
        jLabel9 = new javax.swing.JLabel();
        busqueda1 = new javax.swing.JTextField();
        busqueda2 = new javax.swing.JTextField();
        busqueda = new javax.swing.JTextField();
        jLabel12 = new javax.swing.JLabel();
        jButton2 = new javax.swing.JButton();
        jLabel17 = new javax.swing.JLabel();
        empresa = new javax.swing.JComboBox<>();
        Cod_empresa = new javax.swing.JComboBox<>();
        Tot_Fac = new javax.swing.JTextField();
        jLabel18 = new javax.swing.JLabel();
        jLabel19 = new javax.swing.JLabel();
        jLabel20 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        rec_img = new javax.swing.JLabel();
        lblurl1 = new javax.swing.JLabel();
        Receptor_E = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        jLabel14 = new javax.swing.JLabel();
        jButton7 = new javax.swing.JButton();
        jLabel5 = new javax.swing.JLabel();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jMenu3 = new javax.swing.JMenu();
        jMenu2 = new javax.swing.JMenu();
        jMenu4 = new javax.swing.JMenu();
        r_pr = new javax.swing.JMenuItem();
        r_em = new javax.swing.JMenuItem();
        r_fa = new javax.swing.JMenuItem();
        r_pro = new javax.swing.JMenuItem();
        r_cli = new javax.swing.JMenuItem();
        jMenu6 = new javax.swing.JMenu();
        b_pro = new javax.swing.JMenuItem();
        b_emp = new javax.swing.JMenuItem();
        b_fac = new javax.swing.JMenuItem();
        b_cli = new javax.swing.JMenuItem();
        b_ventas = new javax.swing.JMenuItem();
        modificacion = new javax.swing.JMenu();
        m_pro = new javax.swing.JMenuItem();
        m_emp = new javax.swing.JMenuItem();
        m_cli = new javax.swing.JMenuItem();
        m_prov = new javax.swing.JMenuItem();
        devolucion = new javax.swing.JMenu();
        dev = new javax.swing.JMenuItem();
        jMenu7 = new javax.swing.JMenu();
        ven = new javax.swing.JMenuItem();
        venE = new javax.swing.JMenuItem();
        ea = new javax.swing.JMenu();
        rep = new javax.swing.JMenuItem();
        jMenu5 = new javax.swing.JMenu();
        r_pago = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setUndecorated(true);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        loading.setIcon(new javax.swing.ImageIcon(getClass().getResource("/main/loading_1.gif"))); // NOI18N
        jPanel1.add(loading, new org.netbeans.lib.awtextra.AbsoluteConstraints(590, -20, 310, 160));

        jLabel1.setFont(new java.awt.Font("Times New Roman", 1, 24)); // NOI18N
        jLabel1.setText("REGISTRO FACTURA");
        jPanel1.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(400, 0, 450, 50));

        jLabel2.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        jLabel2.setText("EMPRESA");
        jPanel1.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 150, 120, 20));

        jLabel3.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        jLabel3.setText("NO.FACTURA");
        jPanel1.add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 100, 140, -1));

        No_Factura.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        No_Factura.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                No_FacturaKeyReleased(evt);
            }
        });
        jPanel1.add(No_Factura, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 100, 200, 30));

        jLabel4.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        jLabel4.setText("ABONO");
        jPanel1.add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 240, 120, -1));

        Abono.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        Abono.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                AbonoKeyReleased(evt);
            }
        });
        jPanel1.add(Abono, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 230, 200, 30));

        r_vendedor.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        jPanel1.add(r_vendedor, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 270, 200, 30));

        jLabel6.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        jLabel6.setText("TOT.FACTURA");
        jPanel1.add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 200, 160, -1));

        r_pen.setFont(new java.awt.Font("Times New Roman", 1, 24)); // NOI18N
        r_pen.setForeground(new java.awt.Color(255, 0, 51));
        jPanel1.add(r_pen, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 310, 200, 50));

        tabladatos1.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        tabladatos1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tabladatos1.setSelectionBackground(new java.awt.Color(51, 204, 255));
        jScrollPane1.setViewportView(tabladatos1);

        jPanel1.add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 440, 860, 190));

        tabladatos.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        tabladatos.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tabladatos.setSelectionBackground(new java.awt.Color(51, 204, 255));
        tabladatos.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tabladatosMouseClicked(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                tabladatosMousePressed(evt);
            }
        });
        jScrollPane2.setViewportView(tabladatos);

        jPanel1.add(jScrollPane2, new org.netbeans.lib.awtextra.AbsoluteConstraints(410, 210, 750, 180));

        jLabel9.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        jLabel9.setText("PENDIENTE");
        jPanel1.add(jLabel9, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 340, 120, -1));

        busqueda1.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        busqueda1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                busqueda1KeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                busqueda1KeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                busqueda1KeyTyped(evt);
            }
        });
        jPanel1.add(busqueda1, new org.netbeans.lib.awtextra.AbsoluteConstraints(640, 160, 170, 30));

        busqueda2.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        busqueda2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                busqueda2KeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                busqueda2KeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                busqueda2KeyTyped(evt);
            }
        });
        jPanel1.add(busqueda2, new org.netbeans.lib.awtextra.AbsoluteConstraints(820, 160, 160, 30));

        busqueda.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        busqueda.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                busquedaActionPerformed(evt);
            }
        });
        busqueda.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                busquedaKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                busquedaKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                busquedaKeyTyped(evt);
            }
        });
        jPanel1.add(busqueda, new org.netbeans.lib.awtextra.AbsoluteConstraints(400, 160, 230, 30));

        jLabel12.setFont(new java.awt.Font("Tahoma", 3, 14)); // NOI18N
        jLabel12.setText("ALMACENAR");
        jPanel1.add(jLabel12, new org.netbeans.lib.awtextra.AbsoluteConstraints(1090, 600, 100, 20));

        jButton2.setFont(new java.awt.Font("Tahoma", 3, 14)); // NOI18N
        jButton2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Guardar (1).png"))); // NOI18N
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton2, new org.netbeans.lib.awtextra.AbsoluteConstraints(1110, 540, 70, -1));

        jLabel17.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        jLabel17.setText("RECEPTOR");
        jPanel1.add(jLabel17, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 50, 110, -1));

        empresa.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        empresa.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                empresaItemStateChanged(evt);
            }
        });
        jPanel1.add(empresa, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 140, 200, 30));

        jPanel1.add(Cod_empresa, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 140, 180, 30));

        Tot_Fac.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        Tot_Fac.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                Tot_FacKeyReleased(evt);
            }
        });
        jPanel1.add(Tot_Fac, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 190, 200, 30));

        jLabel18.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        jLabel18.setText("Descipcion");
        jPanel1.add(jLabel18, new org.netbeans.lib.awtextra.AbsoluteConstraints(640, 130, 110, -1));

        jLabel19.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        jLabel19.setText("Marca");
        jPanel1.add(jLabel19, new org.netbeans.lib.awtextra.AbsoluteConstraints(820, 130, 80, -1));

        jLabel20.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        jLabel20.setText("Nombre");
        jPanel1.add(jLabel20, new org.netbeans.lib.awtextra.AbsoluteConstraints(400, 130, 110, -1));

        jLabel16.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        jLabel16.setText("VENDEDOR");
        jPanel1.add(jLabel16, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 280, 120, -1));
        jPanel1.add(rec_img, new org.netbeans.lib.awtextra.AbsoluteConstraints(950, 10, 210, 190));
        jPanel1.add(lblurl1, new org.netbeans.lib.awtextra.AbsoluteConstraints(510, 310, 200, 20));

        Receptor_E.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        Receptor_E.setForeground(new java.awt.Color(0, 255, 255));
        Receptor_E.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(51, 51, 255)));
        Receptor_E.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                Receptor_EKeyReleased(evt);
            }
        });
        jPanel1.add(Receptor_E, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 50, 200, 30));
        jPanel1.add(jSeparator1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 400, 1230, 10));

        jLabel14.setFont(new java.awt.Font("Times New Roman", 1, 24)); // NOI18N
        jLabel14.setText("LISTA DE PRODUCTOS ENTRANTE");
        jPanel1.add(jLabel14, new org.netbeans.lib.awtextra.AbsoluteConstraints(380, 410, 420, 20));

        jButton7.setFont(new java.awt.Font("Tahoma", 3, 24)); // NOI18N
        jButton7.setForeground(new java.awt.Color(255, 255, 255));
        jButton7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/salir.png"))); // NOI18N
        jButton7.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jButton7.setIconTextGap(1);
        jButton7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton7ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton7, new org.netbeans.lib.awtextra.AbsoluteConstraints(1160, 0, 50, 40));

        jLabel5.setBackground(new java.awt.Color(255, 255, 255));
        jLabel5.setFont(new java.awt.Font("Tahoma", 3, 14)); // NOI18N
        jLabel5.setText("SALIR");
        jPanel1.add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(1160, 40, -1, -1));

        getContentPane().add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 1210, 640));

        jMenuBar1.setBorder(null);
        jMenuBar1.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jMenuBar1.setFont(new java.awt.Font("Segoe UI", 2, 12)); // NOI18N

        jMenu1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/can.png"))); // NOI18N
        jMenu1.setText("LOGIN");

        jMenu3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Fondo.png"))); // NOI18N
        jMenu3.setText("LOGIN");
        jMenu3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jMenu3MouseClicked(evt);
            }
        });
        jMenu1.add(jMenu3);

        jMenuBar1.add(jMenu1);
        jMenuBar1.add(jMenu2);

        jMenu4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/add (1).png"))); // NOI18N
        jMenu4.setText(" REGISTRO");

        r_pr.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_R, java.awt.event.InputEvent.CTRL_DOWN_MASK));
        r_pr.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/ingreso.png"))); // NOI18N
        r_pr.setText("PRODUCTO");
        r_pr.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                r_prActionPerformed(evt);
            }
        });
        jMenu4.add(r_pr);

        r_em.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_E, java.awt.event.InputEvent.CTRL_DOWN_MASK));
        r_em.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/emple.png"))); // NOI18N
        r_em.setText("EMPLEADOS");
        r_em.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                r_emActionPerformed(evt);
            }
        });
        jMenu4.add(r_em);

        r_fa.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F, java.awt.event.InputEvent.CTRL_DOWN_MASK));
        r_fa.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/REGISTRO FACTURA.png"))); // NOI18N
        r_fa.setText("FACTURA");
        r_fa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                r_faActionPerformed(evt);
            }
        });
        jMenu4.add(r_fa);

        r_pro.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_P, java.awt.event.InputEvent.CTRL_DOWN_MASK));
        r_pro.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/proveNue.png"))); // NOI18N
        r_pro.setText("PROVEEDOR");
        r_pro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                r_proActionPerformed(evt);
            }
        });
        jMenu4.add(r_pro);

        r_cli.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_C, java.awt.event.InputEvent.CTRL_DOWN_MASK));
        r_cli.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/cliente.png"))); // NOI18N
        r_cli.setText("CLIENTE");
        r_cli.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                r_cliActionPerformed(evt);
            }
        });
        jMenu4.add(r_cli);

        jMenuBar1.add(jMenu4);

        jMenu6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Busqueda (1).png"))); // NOI18N
        jMenu6.setText("BUSQUEDA");

        b_pro.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_P, java.awt.event.InputEvent.ALT_DOWN_MASK));
        b_pro.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/prodddd.png"))); // NOI18N
        b_pro.setText("PRODUCTOS");
        b_pro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                b_proActionPerformed(evt);
            }
        });
        jMenu6.add(b_pro);

        b_emp.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_E, java.awt.event.InputEvent.ALT_DOWN_MASK));
        b_emp.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/busPer .png"))); // NOI18N
        b_emp.setText("EMPLEADOS");
        b_emp.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                b_empActionPerformed(evt);
            }
        });
        jMenu6.add(b_emp);

        b_fac.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F, java.awt.event.InputEvent.ALT_DOWN_MASK));
        b_fac.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/busqueda_factura.png"))); // NOI18N
        b_fac.setText("FACTURA");
        b_fac.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                b_facActionPerformed(evt);
            }
        });
        jMenu6.add(b_fac);

        b_cli.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_C, java.awt.event.InputEvent.ALT_DOWN_MASK));
        b_cli.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/bus_em.png"))); // NOI18N
        b_cli.setText("CLIENTE");
        b_cli.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                b_cliActionPerformed(evt);
            }
        });
        jMenu6.add(b_cli);

        b_ventas.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_Z, java.awt.event.InputEvent.ALT_DOWN_MASK));
        b_ventas.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Busqueda1.png"))); // NOI18N
        b_ventas.setText("VENTAS");
        b_ventas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                b_ventasActionPerformed(evt);
            }
        });
        jMenu6.add(b_ventas);

        jMenuBar1.add(jMenu6);

        modificacion.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/modificacion.png"))); // NOI18N
        modificacion.setText("MODIFICAR");

        m_pro.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_K, java.awt.event.InputEvent.ALT_DOWN_MASK));
        m_pro.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/PRODUCTO.png"))); // NOI18N
        m_pro.setText("PRODUCTO");
        m_pro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                m_proActionPerformed(evt);
            }
        });
        modificacion.add(m_pro);

        m_emp.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_E, java.awt.event.InputEvent.SHIFT_DOWN_MASK));
        m_emp.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/empleado.png"))); // NOI18N
        m_emp.setText("EMPLEADO");
        m_emp.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                m_empActionPerformed(evt);
            }
        });
        modificacion.add(m_emp);

        m_cli.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_C, java.awt.event.InputEvent.SHIFT_DOWN_MASK));
        m_cli.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/clientee.png"))); // NOI18N
        m_cli.setText("CLIENTE");
        m_cli.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                m_cliActionPerformed(evt);
            }
        });
        modificacion.add(m_cli);

        m_prov.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_P, java.awt.event.InputEvent.SHIFT_DOWN_MASK));
        m_prov.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/proveedores.png"))); // NOI18N
        m_prov.setText("PROVEEDORES");
        m_prov.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                m_provActionPerformed(evt);
            }
        });
        modificacion.add(m_prov);

        jMenuBar1.add(modificacion);

        devolucion.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/DEV.png"))); // NOI18N
        devolucion.setText("DEVOULUCION");

        dev.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_D, java.awt.event.InputEvent.CTRL_DOWN_MASK));
        dev.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/dev1.png"))); // NOI18N
        dev.setText("DEVOLUCION");
        dev.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                devActionPerformed(evt);
            }
        });
        devolucion.add(dev);

        jMenuBar1.add(devolucion);

        jMenu7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/png-transparent-shopping-cart-graphy-cart-supermarket-vehicle-shopping-bags-trolleys (1) (1).png"))); // NOI18N
        jMenu7.setText("VENTA");

        ven.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_V, java.awt.event.InputEvent.CTRL_DOWN_MASK));
        ven.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/venta a credito.png"))); // NOI18N
        ven.setText("VENTA  A CREDITO");
        ven.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                venActionPerformed(evt);
            }
        });
        jMenu7.add(ven);

        venE.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_B, java.awt.event.InputEvent.CTRL_DOWN_MASK));
        venE.setIcon(new javax.swing.ImageIcon(getClass().getResource("/main/venta efectivo.png"))); // NOI18N
        venE.setText("VENTA EN EFECTIVO");
        venE.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                venEActionPerformed(evt);
            }
        });
        jMenu7.add(venE);

        jMenuBar1.add(jMenu7);

        ea.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/REPORTE.png"))); // NOI18N
        ea.setText("REPORTE");

        rep.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_R, java.awt.event.InputEvent.ALT_DOWN_MASK));
        rep.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/reportess.png"))); // NOI18N
        rep.setText(" REPORTES");
        rep.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                repActionPerformed(evt);
            }
        });
        ea.add(rep);

        jMenuBar1.add(ea);

        jMenu5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Pago_C_P.png"))); // NOI18N
        jMenu5.setText("PAGOS");

        r_pago.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Pago_C_P (2).png"))); // NOI18N
        r_pago.setText("REALIZAR PAGOS");
        r_pago.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                r_pagoActionPerformed(evt);
            }
        });
        jMenu5.add(r_pago);

        jMenuBar1.add(jMenu5);

        setJMenuBar(jMenuBar1);

        pack();
    }// </editor-fold>//GEN-END:initComponents
public void Extraer_Id() {

        Conexion_mysql con;
        con = new Conexion_mysql(loading);
        java.sql.Connection cn = con.getConnection();

        String readMetaData = "CALL PRC_Consulta_id_Factura(?) ";
        try {
            cn.setAutoCommit(false); // This line must be written just after the 

            CallableStatement cs = cn.prepareCall(readMetaData);
            cs.setString(1, No_Factura.getText().toUpperCase());

            //cs.registerOutParameter(2, Types.REF_CURSOR);
            //cs.execute();
            ResultSet rs = (ResultSet) cs.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    codigoRecivido_Factura = rs.getInt(1);
                    System.out.println("\nID Factura: "+codigoRecivido_Factura+"\n");
                }//end while
            }//end if
        } catch (SQLException ex) {
            Logger.getLogger(R_FACTURA.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println(codigoRecivido_Factura);
    }
    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
      
  if (!No_Factura.getText().equals("") && !Tot_Fac.getText().equals("")
                        && !Abono.getText().equals("") && !r_vendedor.getText().equals("")) {
      
       
     Thread thread = new Thread() {
            public void run() {
              Insertar_Factura();
       Extraer_Id();
       registro_detalle_factura();
     Insertar_Fact_Pagos();//registrar pago en factura pagos
        Receptor_E.setText("");
        No_Factura.setText("");
        Tot_Fac.setText("");
        Abono.setText("");
        r_vendedor.setText("");
        r_pen.setText("");
      borrar(modelo);
        borrar(modelo1);
      }
        };

       thread.start();   
     
      
                } else {
                    JOptionPane.showMessageDialog(null, "<html><h1>Ningun Campo debe quedar Vacio</h1></html>");
                    
                }
            

         
     
                          
   Thread thread1 = new Thread() {
            public void run() {
               consulta_prod_registro_fac();
      }
        };

       thread1.start();   
    }//GEN-LAST:event_jButton2ActionPerformed

    private void empresaItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_empresaItemStateChanged
        int a = empresa.getSelectedIndex();
        if (evt.getStateChange() == ItemEvent.SELECTED) {

            Cod_empresa.setSelectedIndex(a);
            Cod_empresa.disable();

        }
    }//GEN-LAST:event_empresaItemStateChanged

    private void busquedaKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_busquedaKeyReleased
       
    }//GEN-LAST:event_busquedaKeyReleased

    private void busquedaKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_busquedaKeyPressed

    }//GEN-LAST:event_busquedaKeyPressed

    private void busqueda2KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_busqueda2KeyReleased

       
    }//GEN-LAST:event_busqueda2KeyReleased

    private void busqueda2KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_busqueda2KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_busqueda2KeyPressed

    private void busqueda1KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_busqueda1KeyReleased
       
    }//GEN-LAST:event_busqueda1KeyReleased

    private void busqueda1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_busqueda1KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_busqueda1KeyPressed

    private void tabladatosMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tabladatosMouseClicked

    }//GEN-LAST:event_tabladatosMouseClicked

    private void AbonoKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_AbonoKeyReleased
     
        double pendiente = 0;
        double total;
        double abono;
        try {
     if(Integer.parseInt(Tot_Fac.getText())>Integer.parseInt(Abono.getText())){

            total = Float.parseFloat(Abono.getText());
            abono = Float.parseFloat(Tot_Fac.getText());
            pendiente = abono - total;
            r_pen.setText(Double.toString(pendiente));
              if(evt.getKeyCode() == KeyEvent.VK_ENTER){
            r_vendedor.requestFocus();
        }
     }else{
     JOptionPane.showMessageDialog(null, "El abono no puede exceder ");
     Abono.setText("");
     if(evt.getKeyCode() == KeyEvent.VK_ENTER){
            Abono.requestFocus();
        }
     }
        } catch (NumberFormatException e) {
            System.out.print("\n" + e);
        }


    }//GEN-LAST:event_AbonoKeyReleased

    private void tabladatosMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tabladatosMousePressed
        Thread thread = new Thread() {
            public void run() {
  int seleccionn = tabladatos.rowAtPoint(evt.getPoint());
        codigoRecivido = Integer.parseInt(String.valueOf(tabladatos.getValueAt(seleccionn, 0)));
        Nombre_prod1 = String.valueOf(tabladatos.getValueAt(seleccionn, 1));
        descripcion1 = String.valueOf(tabladatos.getValueAt(seleccionn, 2));
        marca = String.valueOf(tabladatos.getValueAt(seleccionn, 3));
        precio_ven = String.valueOf(tabladatos.getValueAt(seleccionn, 5));
        estado = String.valueOf(tabladatos.getValueAt(seleccionn, 6));
        existencia = Integer.parseInt(String.valueOf(tabladatos.getValueAt(seleccionn, 7)));
        Consulta_Imagen();

            }
        };

        thread.start();
      
    }//GEN-LAST:event_tabladatosMousePressed

    private void busquedaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_busquedaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_busquedaActionPerformed

    private void jMenu3MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jMenu3MouseClicked
        Login l = new Login();
        l.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_jMenu3MouseClicked

    private void r_prActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_r_prActionPerformed
        R_PROD a = new R_PROD();
        a.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_r_prActionPerformed

    private void r_emActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_r_emActionPerformed
        R_EMPL a = new R_EMPL();
        a.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_r_emActionPerformed

    private void r_faActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_r_faActionPerformed
        R_FACTURA a = new R_FACTURA();
        a.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_r_faActionPerformed

    private void r_proActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_r_proActionPerformed
        R_PROVEE a = new R_PROVEE();
        a.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_r_proActionPerformed

    private void r_cliActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_r_cliActionPerformed

        R_CLI A = new R_CLI();
        A.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_r_cliActionPerformed

    private void b_proActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_b_proActionPerformed
        Busqueda_Productos a = new Busqueda_Productos();
        a.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_b_proActionPerformed

    private void b_empActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_b_empActionPerformed
        Busqueda_Empleados a = new Busqueda_Empleados();
        a.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_b_empActionPerformed

    private void b_facActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_b_facActionPerformed
        Busqueda_Facturas a = new Busqueda_Facturas();
        a.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_b_facActionPerformed

    private void b_cliActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_b_cliActionPerformed
        Busqueda_Clientes a = new Busqueda_Clientes();
        a.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_b_cliActionPerformed

    private void b_ventasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_b_ventasActionPerformed
        Busqueda_Venta P = new Busqueda_Venta();
        this.setVisible(false);
        P.setVisible(true);
    }//GEN-LAST:event_b_ventasActionPerformed

    private void m_proActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_m_proActionPerformed
        m_prod a = new m_prod();
        a.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_m_proActionPerformed

    private void m_empActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_m_empActionPerformed
        m_empl a = new m_empl();
        a.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_m_empActionPerformed

    private void m_cliActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_m_cliActionPerformed
        m_cliente a = new m_cliente();
        a.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_m_cliActionPerformed

    private void m_provActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_m_provActionPerformed
        m_prov a = new m_prov();
        a.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_m_provActionPerformed

    private void devActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_devActionPerformed
        Devolucion l = new Devolucion();
        l.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_devActionPerformed

    private void venActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_venActionPerformed
        Ventas_Cred a = new Ventas_Cred();
        a.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_venActionPerformed

    private void venEActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_venEActionPerformed
        Ventas a = new Ventas();
        a.setVisible(true);
        this.setVisible(false);

    }//GEN-LAST:event_venEActionPerformed

    private void repActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_repActionPerformed
        Reportes r = new Reportes();
        r.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_repActionPerformed

    private void r_pagoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_r_pagoActionPerformed
        PAGO P = new PAGO();
        this.setVisible(false);
        P.setVisible(true);
    }//GEN-LAST:event_r_pagoActionPerformed

    private void jButton7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton7ActionPerformed
        System.exit(0);
    }//GEN-LAST:event_jButton7ActionPerformed

    private void busquedaKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_busquedaKeyTyped
   //borrar(modelo1);
    busqueda.addKeyListener(new KeyAdapter(){
    public void keyReleased(final KeyEvent e){
    String cadena=(busqueda.getText().toUpperCase());
    busqueda.setText(cadena);
    filtro_nombre_Producto();
    }
   
    
   });
   filtro=new TableRowSorter(tabladatos.getModel());
   tabladatos.setRowSorter(filtro);
    }//GEN-LAST:event_busquedaKeyTyped

    private void busqueda1KeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_busqueda1KeyTyped
   busqueda1.addKeyListener(new KeyAdapter(){
    public void keyReleased(final KeyEvent e){
    String cadena=(busqueda1.getText().toUpperCase());
    busqueda1.setText(cadena);
    filtro_Descripción_Producto();
    }
   
    
   });
   filtro=new TableRowSorter(tabladatos.getModel());
   tabladatos.setRowSorter(filtro);    }//GEN-LAST:event_busqueda1KeyTyped

    private void busqueda2KeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_busqueda2KeyTyped
 busqueda2.addKeyListener(new KeyAdapter(){
    public void keyReleased(final KeyEvent e){
    String cadena=(busqueda2.getText().toUpperCase());
    busqueda2.setText(cadena);
    filtro_Marca_Producto();
    }
   
    
   });
   filtro=new TableRowSorter(tabladatos.getModel());
   tabladatos.setRowSorter(filtro);
    }//GEN-LAST:event_busqueda2KeyTyped

    private void Receptor_EKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_Receptor_EKeyReleased
         if(evt.getKeyCode() == KeyEvent.VK_ENTER){
            No_Factura.requestFocus();
        }
    }//GEN-LAST:event_Receptor_EKeyReleased

    private void No_FacturaKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_No_FacturaKeyReleased
       if(evt.getKeyCode() == KeyEvent.VK_ENTER){
            empresa.requestFocus();
        }
    }//GEN-LAST:event_No_FacturaKeyReleased

    private void Tot_FacKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_Tot_FacKeyReleased
         if(evt.getKeyCode() == KeyEvent.VK_ENTER){
            Abono.requestFocus();
        }
    }//GEN-LAST:event_Tot_FacKeyReleased

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(R_FACTURA.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(R_FACTURA.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(R_FACTURA.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(R_FACTURA.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new R_FACTURA().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField Abono;
    private javax.swing.JComboBox<String> Cod_empresa;
    private javax.swing.JTextField No_Factura;
    private javax.swing.JLabel Receptor_E;
    private javax.swing.JTextField Tot_Fac;
    private javax.swing.JMenuItem b_cli;
    private javax.swing.JMenuItem b_emp;
    private javax.swing.JMenuItem b_fac;
    private javax.swing.JMenuItem b_pro;
    private javax.swing.JMenuItem b_ventas;
    private javax.swing.JTextField busqueda;
    private javax.swing.JTextField busqueda1;
    private javax.swing.JTextField busqueda2;
    private javax.swing.JMenuItem dev;
    private javax.swing.JMenu devolucion;
    private javax.swing.JMenu ea;
    private javax.swing.JComboBox<String> empresa;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton7;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenu jMenu3;
    private javax.swing.JMenu jMenu4;
    private javax.swing.JMenu jMenu5;
    private javax.swing.JMenu jMenu6;
    private javax.swing.JMenu jMenu7;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JLabel lblurl1;
    private javax.swing.JLabel loading;
    private javax.swing.JMenuItem m_cli;
    private javax.swing.JMenuItem m_emp;
    private javax.swing.JMenuItem m_pro;
    private javax.swing.JMenuItem m_prov;
    private javax.swing.JMenu modificacion;
    private javax.swing.JMenuItem r_cli;
    private javax.swing.JMenuItem r_em;
    private javax.swing.JMenuItem r_fa;
    private javax.swing.JMenuItem r_pago;
    private javax.swing.JLabel r_pen;
    private javax.swing.JMenuItem r_pr;
    private javax.swing.JMenuItem r_pro;
    private javax.swing.JTextField r_vendedor;
    private javax.swing.JLabel rec_img;
    private javax.swing.JMenuItem rep;
    private javax.swing.JTable tabladatos;
    private javax.swing.JTable tabladatos1;
    private javax.swing.JMenuItem ven;
    private javax.swing.JMenuItem venE;
    // End of variables declaration//GEN-END:variables
}
