/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import clases.Conexion_postgres;
import clases.Activacion;
import clases.Conexion_mysql;
import clases.TextPrompt;
import java.awt.Image;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.CallableStatement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

/**
 *
 * @author Leo
 */
public class R_PROD extends javax.swing.JFrame {

    Conexion_postgres cn = new Conexion_postgres();
    String ruta = null;

    public R_PROD() {
        initComponents();
                        TextPrompt s = new TextPrompt("Ingrese Nombre Producto", r_nom);
                TextPrompt t = new TextPrompt("Ingrese Descripción", r_des);
                TextPrompt r = new TextPrompt("Ingrese Marca", r_marca);
                TextPrompt a = new TextPrompt("Ingrese Cantidad", r_cant);
                TextPrompt m = new TextPrompt("Ingrese Precio Compra", r_pre_com);
                  TextPrompt d = new TextPrompt("Ingrese Precio Venta", r_pre_ven);
        loading.setVisible(false);
        this.setLocationRelativeTo(null);
        /*    TextPrompt p=new TextPrompt("Nombre",r_nom);
            TextPrompt q=new TextPrompt("Descripcion",r_des);
             TextPrompt r=new TextPrompt("Precio compra",r_pre_com);
              TextPrompt s=new TextPrompt("Precio Venta",r_pre_ven);
            TextPrompt t=new TextPrompt("Cantidad",r_cant);
             TextPrompt u=new TextPrompt("Marca",r_marca);*/
        //ACIVACION
        r_pr.setEnabled(false);
        r_pro.setEnabled(false);
        r_em.setEnabled(false);
        r_fa.setEnabled(false);
        r_cli.setEnabled(false);
        b_pro.setEnabled(false);
        b_emp.setEnabled(false);
        b_fac.setEnabled(false);
        b_cli.setEnabled(false);
        ven.setEnabled(false);
        venE.setEnabled(false);
        rep.setEnabled(false);
        dev.setEnabled(false);
        m_pro.setEnabled(false);
        m_emp.setEnabled(false);
        m_cli.setEnabled(false);
        m_prov.setEnabled(false);
        b_ventas.setEnabled(false);
        r_pago.setEnabled(false);
        Activacion b = new Activacion();
        if (b.ver == 1) {
            b.entrante(r_pr, r_pro, r_em, r_fa, r_cli, b_pro, b_emp, b_fac, b_cli, modificacion, devolucion, ven, ea, dev, m_pro,
                    m_emp, m_cli, m_prov, rep, venE, b_ventas, r_pago);
        }
        if (b.ver == 2) {
            b.en(ven, venE, r_fa, b_pro, r_pr, r_pro, r_cli, r_pago, dev, b_cli, b_fac);
        }
        
    }

    public void registro_prod() {
          Conexion_mysql con;
    con=new Conexion_mysql(loading);
  java.sql.Connection cn=con.getConnection();
        String sql = "call PRC_re_prod(?,?,?,?,?,?,?,?)";
        try {
            
            FileInputStream fi = null;
            
            CallableStatement cs = cn.prepareCall(sql);
            File file = new File(ruta);
            fi = new FileInputStream(file);
            cs.setString(1, r_nom.getText().toUpperCase());
            cs.setString(2, r_des.getText().toUpperCase());
            cs.setFloat(3, Float.parseFloat(r_pre_com.getText()));
            cs.setFloat(4, Float.parseFloat(r_pre_ven.getText()));
            if (activo.isSelected()) {
                cs.setString(5, activo.getText().toUpperCase());                
            }
            if (no_activo.isSelected()) {
                cs.setString(5, no_activo.getText().toUpperCase());                
            }
            cs.setInt(6, Integer.parseInt(r_cant.getText()));
            cs.setString(7, r_marca.getText().toUpperCase());
            cs.setBinaryStream(8, fi);
            if (cs.execute()) {
                JOptionPane.showMessageDialog(null, "Registro Completado");
            }
             JOptionPane.showMessageDialog(null, "Registro Completado");
            cs.close();
        } catch (Exception ex) {
    JOptionPane.showMessageDialog(null, "<html><h1>Ingreso en campo cantidad o precio en tipo letra <p> <h2>Favor revisarlo</h2></h1></html>");

            Logger.getLogger(R_PROD.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        Botones = new javax.swing.ButtonGroup();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        r_nom = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        r_des = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        r_pre_com = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        r_pre_ven = new javax.swing.JTextField();
        r_cant = new javax.swing.JTextField();
        lblimagen = new javax.swing.JLabel();
        lblurl = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jButton4 = new javax.swing.JButton();
        jButton1 = new javax.swing.JButton();
        r_marca = new javax.swing.JTextField();
        jLabel11 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        no_activo = new javax.swing.JRadioButton();
        activo = new javax.swing.JRadioButton();
        jButton7 = new javax.swing.JButton();
        jLabel8 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        loading = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        r_cant1 = new javax.swing.JTextField();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jMenu3 = new javax.swing.JMenu();
        jMenu2 = new javax.swing.JMenu();
        jMenu4 = new javax.swing.JMenu();
        r_pr = new javax.swing.JMenuItem();
        r_em = new javax.swing.JMenuItem();
        r_fa = new javax.swing.JMenuItem();
        r_pro = new javax.swing.JMenuItem();
        r_cli = new javax.swing.JMenuItem();
        jMenu6 = new javax.swing.JMenu();
        b_pro = new javax.swing.JMenuItem();
        b_emp = new javax.swing.JMenuItem();
        b_fac = new javax.swing.JMenuItem();
        b_cli = new javax.swing.JMenuItem();
        b_ventas = new javax.swing.JMenuItem();
        modificacion = new javax.swing.JMenu();
        m_pro = new javax.swing.JMenuItem();
        m_emp = new javax.swing.JMenuItem();
        m_cli = new javax.swing.JMenuItem();
        m_prov = new javax.swing.JMenuItem();
        devolucion = new javax.swing.JMenu();
        dev = new javax.swing.JMenuItem();
        jMenu7 = new javax.swing.JMenu();
        ven = new javax.swing.JMenuItem();
        venE = new javax.swing.JMenuItem();
        ea = new javax.swing.JMenu();
        rep = new javax.swing.JMenuItem();
        jMenu5 = new javax.swing.JMenu();
        r_pago = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setUndecorated(true);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel1.setFont(new java.awt.Font("Times New Roman", 1, 36)); // NOI18N
        jLabel1.setText("REGISTRO PRODUCTO");
        jPanel1.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(210, 10, 450, 60));

        r_nom.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        r_nom.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                r_nomActionPerformed(evt);
            }
        });
        r_nom.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                r_nomKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                r_nomKeyTyped(evt);
            }
        });
        jPanel1.add(r_nom, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 120, 290, 30));

        jLabel2.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        jLabel2.setText("NOMBRE PRODUCTO");
        jPanel1.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 90, 190, -1));

        r_des.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        r_des.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                r_desKeyReleased(evt);
            }
        });
        jPanel1.add(r_des, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 200, 290, 30));

        jLabel3.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        jLabel3.setText(" DESCRIPCIÓN");
        jPanel1.add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 170, 140, -1));

        jLabel4.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        jLabel4.setText("PRECIO_COMPRA");
        jPanel1.add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(370, 80, 170, -1));

        r_pre_com.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        r_pre_com.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                r_pre_comKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                r_pre_comKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                r_pre_comKeyTyped(evt);
            }
        });
        jPanel1.add(r_pre_com, new org.netbeans.lib.awtextra.AbsoluteConstraints(370, 110, 290, 30));

        jLabel6.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        jLabel6.setText("ESTADO");
        jPanel1.add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(390, 300, 80, -1));

        jLabel7.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        jLabel7.setText("% Ganancia");
        jPanel1.add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(370, 150, 120, 20));

        jLabel5.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        jLabel5.setText("PRECIO_VENTA");
        jPanel1.add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(370, 230, 160, -1));

        r_pre_ven.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        r_pre_ven.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                r_pre_venKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                r_pre_venKeyTyped(evt);
            }
        });
        jPanel1.add(r_pre_ven, new org.netbeans.lib.awtextra.AbsoluteConstraints(370, 260, 290, 30));

        r_cant.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        r_cant.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                r_cantKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                r_cantKeyTyped(evt);
            }
        });
        jPanel1.add(r_cant, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 360, 300, 30));
        jPanel1.add(lblimagen, new org.netbeans.lib.awtextra.AbsoluteConstraints(710, 60, 330, 340));
        jPanel1.add(lblurl, new org.netbeans.lib.awtextra.AbsoluteConstraints(720, 410, 270, 20));

        jLabel12.setFont(new java.awt.Font("Tahoma", 3, 18)); // NOI18N
        jLabel12.setForeground(new java.awt.Color(255, 255, 255));
        jLabel12.setText("IMAGEN PRODUCTO");
        jPanel1.add(jLabel12, new org.netbeans.lib.awtextra.AbsoluteConstraints(550, 60, 190, 20));

        jButton4.setFont(new java.awt.Font("Arial Black", 0, 14)); // NOI18N
        jButton4.setForeground(new java.awt.Color(255, 204, 51));
        jButton4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/buscar.png"))); // NOI18N
        jButton4.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 255, 255), 2));
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton4, new org.netbeans.lib.awtextra.AbsoluteConstraints(810, 450, 80, 70));

        jButton1.setFont(new java.awt.Font("Tahoma", 3, 14)); // NOI18N
        jButton1.setForeground(new java.awt.Color(0, 255, 204));
        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Guardar (1).png"))); // NOI18N
        jButton1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 255, 255), 2));
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton1, new org.netbeans.lib.awtextra.AbsoluteConstraints(970, 450, 80, 70));

        r_marca.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        r_marca.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                r_marcaKeyReleased(evt);
            }
        });
        jPanel1.add(r_marca, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 280, 300, 30));

        jLabel11.setFont(new java.awt.Font("Tahoma", 3, 12)); // NOI18N
        jLabel11.setText("Cargar Imagen");
        jPanel1.add(jLabel11, new org.netbeans.lib.awtextra.AbsoluteConstraints(800, 520, 100, -1));

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));
        jPanel2.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255)));
        jPanel2.setForeground(new java.awt.Color(255, 255, 255));
        jPanel2.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        no_activo.setBackground(new java.awt.Color(255, 255, 255));
        Botones.add(no_activo);
        no_activo.setText("NO_ACTIVO");
        jPanel2.add(no_activo, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 40, -1, -1));

        activo.setBackground(new java.awt.Color(255, 255, 255));
        Botones.add(activo);
        activo.setText("ACTIVO");
        jPanel2.add(activo, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, -1, -1));

        jPanel1.add(jPanel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(380, 330, 190, 70));

        jButton7.setFont(new java.awt.Font("Tahoma", 3, 24)); // NOI18N
        jButton7.setForeground(new java.awt.Color(255, 255, 255));
        jButton7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/salir.png"))); // NOI18N
        jButton7.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jButton7.setIconTextGap(1);
        jButton7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton7ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton7, new org.netbeans.lib.awtextra.AbsoluteConstraints(930, 0, 50, 40));

        jLabel8.setBackground(new java.awt.Color(255, 255, 255));
        jLabel8.setFont(new java.awt.Font("Tahoma", 3, 14)); // NOI18N
        jLabel8.setText("SALIR");
        jPanel1.add(jLabel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(930, 40, -1, -1));

        jLabel13.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        jLabel13.setText("MARCA");
        jPanel1.add(jLabel13, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 250, 110, -1));

        jLabel14.setFont(new java.awt.Font("Tahoma", 3, 12)); // NOI18N
        jLabel14.setText("Guardar");
        jPanel1.add(jLabel14, new org.netbeans.lib.awtextra.AbsoluteConstraints(980, 520, 60, -1));

        loading.setIcon(new javax.swing.ImageIcon(getClass().getResource("/main/loading_1.gif"))); // NOI18N
        jPanel1.add(loading, new org.netbeans.lib.awtextra.AbsoluteConstraints(330, 390, 310, 90));

        jLabel9.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        jLabel9.setText("CANTIDAD");
        jPanel1.add(jLabel9, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 320, 120, -1));

        r_cant1.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        r_cant1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                r_cant1KeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                r_cant1KeyTyped(evt);
            }
        });
        jPanel1.add(r_cant1, new org.netbeans.lib.awtextra.AbsoluteConstraints(370, 180, 110, 40));

        getContentPane().add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 1070, 550));

        jMenuBar1.setBorder(null);
        jMenuBar1.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jMenuBar1.setFont(new java.awt.Font("Segoe UI", 2, 12)); // NOI18N

        jMenu1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/can.png"))); // NOI18N
        jMenu1.setText("LOGIN");

        jMenu3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Fondo.png"))); // NOI18N
        jMenu3.setText("LOGIN");
        jMenu3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jMenu3MouseClicked(evt);
            }
        });
        jMenu1.add(jMenu3);

        jMenuBar1.add(jMenu1);
        jMenuBar1.add(jMenu2);

        jMenu4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/add (1).png"))); // NOI18N
        jMenu4.setText(" REGISTRO");

        r_pr.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_R, java.awt.event.InputEvent.CTRL_DOWN_MASK));
        r_pr.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/ingreso.png"))); // NOI18N
        r_pr.setText("PRODUCTO");
        r_pr.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                r_prActionPerformed(evt);
            }
        });
        jMenu4.add(r_pr);

        r_em.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_E, java.awt.event.InputEvent.CTRL_DOWN_MASK));
        r_em.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/emple.png"))); // NOI18N
        r_em.setText("EMPLEADOS");
        r_em.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                r_emActionPerformed(evt);
            }
        });
        jMenu4.add(r_em);

        r_fa.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F, java.awt.event.InputEvent.CTRL_DOWN_MASK));
        r_fa.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/REGISTRO FACTURA.png"))); // NOI18N
        r_fa.setText("FACTURA");
        r_fa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                r_faActionPerformed(evt);
            }
        });
        jMenu4.add(r_fa);

        r_pro.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_P, java.awt.event.InputEvent.CTRL_DOWN_MASK));
        r_pro.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/proveNue.png"))); // NOI18N
        r_pro.setText("PROVEEDOR");
        r_pro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                r_proActionPerformed(evt);
            }
        });
        jMenu4.add(r_pro);

        r_cli.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_C, java.awt.event.InputEvent.CTRL_DOWN_MASK));
        r_cli.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/cliente.png"))); // NOI18N
        r_cli.setText("CLIENTE");
        r_cli.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                r_cliActionPerformed(evt);
            }
        });
        jMenu4.add(r_cli);

        jMenuBar1.add(jMenu4);

        jMenu6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Busqueda (1).png"))); // NOI18N
        jMenu6.setText("BUSQUEDA");

        b_pro.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_P, java.awt.event.InputEvent.ALT_DOWN_MASK));
        b_pro.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/prodddd.png"))); // NOI18N
        b_pro.setText("PRODUCTOS");
        b_pro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                b_proActionPerformed(evt);
            }
        });
        jMenu6.add(b_pro);

        b_emp.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_E, java.awt.event.InputEvent.ALT_DOWN_MASK));
        b_emp.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/busPer .png"))); // NOI18N
        b_emp.setText("EMPLEADOS");
        b_emp.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                b_empActionPerformed(evt);
            }
        });
        jMenu6.add(b_emp);

        b_fac.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F, java.awt.event.InputEvent.ALT_DOWN_MASK));
        b_fac.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/busqueda_factura.png"))); // NOI18N
        b_fac.setText("FACTURA");
        b_fac.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                b_facActionPerformed(evt);
            }
        });
        jMenu6.add(b_fac);

        b_cli.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_C, java.awt.event.InputEvent.ALT_DOWN_MASK));
        b_cli.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/bus_em.png"))); // NOI18N
        b_cli.setText("CLIENTE");
        b_cli.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                b_cliActionPerformed(evt);
            }
        });
        jMenu6.add(b_cli);

        b_ventas.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_Z, java.awt.event.InputEvent.ALT_DOWN_MASK));
        b_ventas.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Busqueda1.png"))); // NOI18N
        b_ventas.setText("VENTAS");
        b_ventas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                b_ventasActionPerformed(evt);
            }
        });
        jMenu6.add(b_ventas);

        jMenuBar1.add(jMenu6);

        modificacion.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/modificacion.png"))); // NOI18N
        modificacion.setText("MODIFICAR");

        m_pro.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_K, java.awt.event.InputEvent.ALT_DOWN_MASK));
        m_pro.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/PRODUCTO.png"))); // NOI18N
        m_pro.setText("PRODUCTO");
        m_pro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                m_proActionPerformed(evt);
            }
        });
        modificacion.add(m_pro);

        m_emp.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_E, java.awt.event.InputEvent.SHIFT_DOWN_MASK));
        m_emp.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/empleado.png"))); // NOI18N
        m_emp.setText("EMPLEADO");
        m_emp.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                m_empActionPerformed(evt);
            }
        });
        modificacion.add(m_emp);

        m_cli.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_C, java.awt.event.InputEvent.SHIFT_DOWN_MASK));
        m_cli.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/clientee.png"))); // NOI18N
        m_cli.setText("CLIENTE");
        m_cli.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                m_cliActionPerformed(evt);
            }
        });
        modificacion.add(m_cli);

        m_prov.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_P, java.awt.event.InputEvent.SHIFT_DOWN_MASK));
        m_prov.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/proveedores.png"))); // NOI18N
        m_prov.setText("PROVEEDORES");
        m_prov.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                m_provActionPerformed(evt);
            }
        });
        modificacion.add(m_prov);

        jMenuBar1.add(modificacion);

        devolucion.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/DEV.png"))); // NOI18N
        devolucion.setText("DEVOULUCION");

        dev.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_D, java.awt.event.InputEvent.CTRL_DOWN_MASK));
        dev.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/dev1.png"))); // NOI18N
        dev.setText("DEVOLUCION");
        dev.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                devActionPerformed(evt);
            }
        });
        devolucion.add(dev);

        jMenuBar1.add(devolucion);

        jMenu7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/png-transparent-shopping-cart-graphy-cart-supermarket-vehicle-shopping-bags-trolleys (1) (1).png"))); // NOI18N
        jMenu7.setText("VENTA");

        ven.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_V, java.awt.event.InputEvent.CTRL_DOWN_MASK));
        ven.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/venta a credito.png"))); // NOI18N
        ven.setText("VENTA  A CREDITO");
        ven.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                venActionPerformed(evt);
            }
        });
        jMenu7.add(ven);

        venE.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_B, java.awt.event.InputEvent.CTRL_DOWN_MASK));
        venE.setIcon(new javax.swing.ImageIcon(getClass().getResource("/main/venta efectivo.png"))); // NOI18N
        venE.setText("VENTA EN EFECTIVO");
        venE.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                venEActionPerformed(evt);
            }
        });
        jMenu7.add(venE);

        jMenuBar1.add(jMenu7);

        ea.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/REPORTE.png"))); // NOI18N
        ea.setText("REPORTE");

        rep.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_R, java.awt.event.InputEvent.ALT_DOWN_MASK));
        rep.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/reportess.png"))); // NOI18N
        rep.setText(" REPORTES");
        rep.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                repActionPerformed(evt);
            }
        });
        ea.add(rep);

        jMenuBar1.add(ea);

        jMenu5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Pago_C_P.png"))); // NOI18N
        jMenu5.setText("PAGOS");

        r_pago.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Pago_C_P (2).png"))); // NOI18N
        r_pago.setText("REALIZAR PAGOS");
        r_pago.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                r_pagoActionPerformed(evt);
            }
        });
        jMenu5.add(r_pago);

        jMenuBar1.add(jMenu5);

        setJMenuBar(jMenuBar1);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
        
        JFileChooser j = new JFileChooser();
        j.setCurrentDirectory(new File("C:\\Users\\Leo\\Documents\\Productos"));
        
        int ap = j.showOpenDialog(this);
        if (ap == JFileChooser.APPROVE_OPTION) {
            ruta = j.getSelectedFile().getAbsolutePath();
            
            try {
                lblimagen.setIcon(new ImageIcon(ruta));
                String cadena = ruta;
                String[] parts = cadena.split("\\.");                
                if (parts[1].equals("png") || parts[1].equals("jpg") || parts[1].equals("gif")
                        || parts[1].equals("jpge")) {
                    lblurl.setText(ruta);

                    //cambiar medida imagen
                    Image icono1 = ImageIO.read(j.getSelectedFile()).getScaledInstance(lblimagen.getWidth(), lblimagen.getHeight(), Image.SCALE_DEFAULT);//especificar el tama;o de imagen

                    lblimagen.setIcon(new ImageIcon(icono1));
                } else {
                     lblimagen.setText("");
                    JOptionPane.showMessageDialog(null, "<html><h1>El archivo cargado no es una imagen</h1></html>");
                   lblurl.setText("");
                }
                
            } catch (IOException ex) {
                Logger.getLogger(m_prod.class.getName()).log(Level.SEVERE, null, ex);
            }
            
        }
    }//GEN-LAST:event_jButton4ActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
      Thread thread = new Thread() {
            public void run() {

               if (!lblurl.getText().equals("")) {
            
            if ((no_activo.isSelected() == true || activo.isSelected() == true)) {
                if (!r_des.getText().equals("") && !r_des.getText().equals("")
                        && !r_pre_com.getText().equals("") && !r_pre_ven.getText().equals("")
                        && !r_cant.getText().equals("") && !r_marca.getText().equals("")) {
                     
                    registro_prod();                    
                    r_des.setText("");
                    r_pre_com.setText("");
                    r_pre_ven.setText("");
                    r_cant.setText("");
                    r_marca.setText("");
                 lblimagen.setIcon(null);
                } else {
                    JOptionPane.showMessageDialog(null, "<html><h1>Ningun Campo debe quedar Vacio</h1></html>");
                    
                }
                
            } else {
                JOptionPane.showMessageDialog(null, "<html><h1>Debes seleccionar un estado</h1></html>");
                
            }
            
        } else {
            JOptionPane.showMessageDialog(null, "<html><h1>Debes cargar la imagen del producto</h1></html>");
        }
       
            }
        };

        thread.start(); 
        
       
    }//GEN-LAST:event_jButton1ActionPerformed

    private void r_nomActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_r_nomActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_r_nomActionPerformed

    private void jMenu3MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jMenu3MouseClicked
        Login l = new Login();
        l.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_jMenu3MouseClicked

    private void r_prActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_r_prActionPerformed
        R_PROD a = new R_PROD();
        a.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_r_prActionPerformed

    private void r_emActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_r_emActionPerformed
        R_EMPL a = new R_EMPL();
        a.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_r_emActionPerformed

    private void r_faActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_r_faActionPerformed
        R_FACTURA a = new R_FACTURA();
        a.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_r_faActionPerformed

    private void r_proActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_r_proActionPerformed
        R_PROVEE a = new R_PROVEE();
        a.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_r_proActionPerformed

    private void r_cliActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_r_cliActionPerformed
        
        R_CLI A = new R_CLI();
        A.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_r_cliActionPerformed

    private void b_proActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_b_proActionPerformed
        Busqueda_Productos a = new Busqueda_Productos();
        a.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_b_proActionPerformed

    private void b_empActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_b_empActionPerformed
        Busqueda_Empleados a = new Busqueda_Empleados();
        a.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_b_empActionPerformed

    private void b_facActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_b_facActionPerformed
        Busqueda_Facturas a = new Busqueda_Facturas();
        a.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_b_facActionPerformed

    private void b_cliActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_b_cliActionPerformed
        Busqueda_Clientes a = new Busqueda_Clientes();
        a.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_b_cliActionPerformed

    private void b_ventasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_b_ventasActionPerformed
        Busqueda_Venta P = new Busqueda_Venta();
        this.setVisible(false);
        P.setVisible(true);
    }//GEN-LAST:event_b_ventasActionPerformed

    private void m_proActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_m_proActionPerformed
        m_prod a = new m_prod();
        a.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_m_proActionPerformed

    private void m_empActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_m_empActionPerformed
        m_empl a = new m_empl();
        a.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_m_empActionPerformed

    private void m_cliActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_m_cliActionPerformed
        m_cliente a = new m_cliente();
        a.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_m_cliActionPerformed

    private void m_provActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_m_provActionPerformed
        m_prov a = new m_prov();
        a.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_m_provActionPerformed

    private void devActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_devActionPerformed
        Devolucion l = new Devolucion();
        l.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_devActionPerformed

    private void venActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_venActionPerformed
        Ventas_Cred a = new Ventas_Cred();
        a.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_venActionPerformed

    private void venEActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_venEActionPerformed
        Ventas a = new Ventas();
        a.setVisible(true);
        this.setVisible(false);

    }//GEN-LAST:event_venEActionPerformed

    private void repActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_repActionPerformed
        Reportes r = new Reportes();
        r.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_repActionPerformed

    private void r_pagoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_r_pagoActionPerformed
        PAGO P = new PAGO();
        this.setVisible(false);
        P.setVisible(true);
    }//GEN-LAST:event_r_pagoActionPerformed

    private void jButton7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton7ActionPerformed
        System.exit(0);
    }//GEN-LAST:event_jButton7ActionPerformed

    private void r_nomKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_r_nomKeyReleased
  if(evt.getKeyCode() == KeyEvent.VK_ENTER){
            r_des.requestFocus();
        }
    }//GEN-LAST:event_r_nomKeyReleased

    private void r_desKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_r_desKeyReleased
 if(evt.getKeyCode() == KeyEvent.VK_ENTER){
            r_marca.requestFocus();
        }
    }//GEN-LAST:event_r_desKeyReleased

    private void r_pre_comKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_r_pre_comKeyReleased

 if(r_cant1.getText().equals("")||r_pre_com.getText().equals("")){
        r_pre_ven.setText(Double.toString(Double.parseDouble("0")+(Double.parseDouble("0")*(Integer.parseInt("0"))/100)));
 }else{
        r_pre_ven.setText(Double.toString(Double.parseDouble(r_pre_com.getText())+(Double.parseDouble(r_pre_com.getText())*(Integer.parseInt(r_cant1.getText()))/100)));
    }
        if(evt.getKeyCode() == KeyEvent.VK_ENTER){
            r_pre_ven.requestFocus();
        }
    }//GEN-LAST:event_r_pre_comKeyReleased

    private void r_marcaKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_r_marcaKeyReleased
 if(evt.getKeyCode() == KeyEvent.VK_ENTER){
            r_cant.requestFocus();
        }
    }//GEN-LAST:event_r_marcaKeyReleased

    private void r_cantKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_r_cantKeyReleased
      if(evt.getKeyCode() == KeyEvent.VK_ENTER){
            r_pre_com.requestFocus();
        }
    }//GEN-LAST:event_r_cantKeyReleased

    private void r_pre_venKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_r_pre_venKeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_r_pre_venKeyReleased

    private void r_pre_comKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_r_pre_comKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_r_pre_comKeyPressed

    private void r_cantKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_r_cantKeyTyped
         char c=evt.getKeyChar();
    //  if(!Character.isDigit(c)&c!='.'){
        if(!Character.isDigit(c)){
          evt.consume();
      }
    /*  if(c=='.'&&r_tel.getText().contains(".")){
          evt.consume();
      }*/
      
        if(r_cant.getText().length() >= 8)
    {
        evt.consume();
    }
    }//GEN-LAST:event_r_cantKeyTyped

    private void r_pre_comKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_r_pre_comKeyTyped
             char c=evt.getKeyChar();
     if(!Character.isDigit(c)&c!='.'){
       
          evt.consume();
      }
    if(c=='.'&&r_pre_com.getText().contains(".")){
          evt.consume();
      }
      
        if(r_pre_com.getText().length() >= 8)
    {
        evt.consume();
    }
    }//GEN-LAST:event_r_pre_comKeyTyped

    private void r_pre_venKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_r_pre_venKeyTyped
         char c=evt.getKeyChar();
     if(!Character.isDigit(c)&c!='.'){
       
          evt.consume();
      }
    if(c=='.'&&r_pre_ven.getText().contains(".")){
          evt.consume();
      }
      
        if(r_pre_ven.getText().length() >= 8)
    {
        evt.consume();
    }
    }//GEN-LAST:event_r_pre_venKeyTyped

    private void r_cant1KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_r_cant1KeyReleased
 if(r_cant1.getText().equals("")||r_pre_com.getText().equals("")){
        r_pre_ven.setText(Double.toString(Double.parseDouble("0")+(Double.parseDouble("0")*(Integer.parseInt("0"))/100)));
 }else{
        r_pre_ven.setText(Double.toString(Double.parseDouble(r_pre_com.getText())+(Double.parseDouble(r_pre_com.getText())*(Integer.parseInt(r_cant1.getText()))/100)));
    }
    }//GEN-LAST:event_r_cant1KeyReleased

    private void r_cant1KeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_r_cant1KeyTyped
        // TODO add your handling code here:
    }//GEN-LAST:event_r_cant1KeyTyped

    private void r_nomKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_r_nomKeyTyped
        char c=evt.getKeyChar();
     if(Character.isDigit(c)){
       
          evt.consume();
      }
    }//GEN-LAST:event_r_nomKeyTyped

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(R_PROD.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(R_PROD.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(R_PROD.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(R_PROD.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
                } catch (ClassNotFoundException ex) {
                    Logger.getLogger(R_PROD.class.getName()).log(Level.SEVERE, null, ex);
                } catch (InstantiationException ex) {
                    Logger.getLogger(R_PROD.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IllegalAccessException ex) {
                    Logger.getLogger(R_PROD.class.getName()).log(Level.SEVERE, null, ex);
                } catch (UnsupportedLookAndFeelException ex) {
                    Logger.getLogger(R_PROD.class.getName()).log(Level.SEVERE, null, ex);
                }
                new R_PROD().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup Botones;
    private javax.swing.JRadioButton activo;
    private javax.swing.JMenuItem b_cli;
    private javax.swing.JMenuItem b_emp;
    private javax.swing.JMenuItem b_fac;
    private javax.swing.JMenuItem b_pro;
    private javax.swing.JMenuItem b_ventas;
    private javax.swing.JMenuItem dev;
    private javax.swing.JMenu devolucion;
    private javax.swing.JMenu ea;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton4;
    private javax.swing.JButton jButton7;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenu jMenu3;
    private javax.swing.JMenu jMenu4;
    private javax.swing.JMenu jMenu5;
    private javax.swing.JMenu jMenu6;
    private javax.swing.JMenu jMenu7;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JLabel lblimagen;
    private javax.swing.JLabel lblurl;
    private javax.swing.JLabel loading;
    private javax.swing.JMenuItem m_cli;
    private javax.swing.JMenuItem m_emp;
    private javax.swing.JMenuItem m_pro;
    private javax.swing.JMenuItem m_prov;
    private javax.swing.JMenu modificacion;
    private javax.swing.JRadioButton no_activo;
    private javax.swing.JTextField r_cant;
    private javax.swing.JTextField r_cant1;
    private javax.swing.JMenuItem r_cli;
    private javax.swing.JTextField r_des;
    private javax.swing.JMenuItem r_em;
    private javax.swing.JMenuItem r_fa;
    private javax.swing.JTextField r_marca;
    private javax.swing.JTextField r_nom;
    private javax.swing.JMenuItem r_pago;
    private javax.swing.JMenuItem r_pr;
    private javax.swing.JTextField r_pre_com;
    private javax.swing.JTextField r_pre_ven;
    private javax.swing.JMenuItem r_pro;
    private javax.swing.JMenuItem rep;
    private javax.swing.JMenuItem ven;
    private javax.swing.JMenuItem venE;
    // End of variables declaration//GEN-END:variables
}
