/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import clases.Conexion_postgres;
import clases.Activacion;
import clases.Conexion_mysql;
import clases.TextPrompt;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.RowFilter;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;

public class PAGO extends javax.swing.JFrame {
 private TableRowSorter filtro;

    int id_ven_fac;
    String id_Factura;
    DefaultTableModel modelo2 = new DefaultTableModel();


    public boolean isCellEditable(int row, int column) {
        return false;
    }

    public PAGO() {
        initComponents();
                TextPrompt r = new TextPrompt("Ingrese Nombre", busqueda);
                TextPrompt s = new TextPrompt("Ingrese Fecga", busqueda1);

                        //Primera tabla
tabladatos.setOpaque(false);
jScrollPane1.setOpaque(false);
jScrollPane1.getViewport().setOpaque(false);
                        //Segunda tabla
tabladatos2.setOpaque(false);
jScrollPane4.setOpaque(false);
jScrollPane4.getViewport().setOpaque(false);

                        //Tercera tabla
tabladatos1.setOpaque(false);
jScrollPane2.setOpaque(false);
jScrollPane2.getViewport().setOpaque(false);
        loading.setVisible(false);
        ((DefaultTableCellRenderer) tabladatos4.getDefaultRenderer(Object.class)).setOpaque(false);
        rcs.setOpaque(false);
        rcs.getViewport().setOpaque(false);
//borrar cuadricula table.setShowGrid(false);
        tabladatos4.setRowHeight(0, 30);//cambio tama;o de fila
        modelo2.addColumn("Codigo Prod");
        modelo2.addColumn("Nombre_prod");
        modelo2.addColumn("Descripcion");
        modelo2.addColumn("Marca");
        modelo2.addColumn("Precio");
        modelo2.addColumn("Cantidad");
        modelo2.addColumn("subtotal");
        tabladatos4.setModel(modelo2);
        tabladatos2.setDefaultEditor(Object.class, null);
        tabladatos2.setDefaultEditor(Object.class, null);
        tabladatos.setDefaultEditor(Object.class, null);
        this.setLocationRelativeTo(null);

        busqueda1.setEnabled(false);
        busqueda.setEnabled(false);
        pago.setEnabled(false);
        //BLOQUEO Y DESBLOQUEO
        r_pr.setEnabled(false);
        r_pro.setEnabled(false);
        r_em.setEnabled(false);
        r_fa.setEnabled(false);
        r_cli.setEnabled(false);
        b_pro.setEnabled(false);
        b_emp.setEnabled(false);
        b_fac.setEnabled(false);
        b_cli.setEnabled(false);
        ven.setEnabled(false);
        venE.setEnabled(false);
        rep.setEnabled(false);
        dev.setEnabled(false);
        m_pro.setEnabled(false);
        m_emp.setEnabled(false);
        m_cli.setEnabled(false);
        m_prov.setEnabled(false);
        b_ventas.setEnabled(false);
        r_pago.setEnabled(false);
        Activacion b = new Activacion();
        if (b.ver == 1) {
            b.entrante(r_pr, r_pro, r_em, r_fa, r_cli, b_pro, b_emp, b_fac, b_cli, modificacion, devolucion, ven, ea, dev, m_pro,
                    m_emp, m_cli, m_prov, rep, venE, b_ventas, r_pago);
        }
        if (b.ver == 2) {
            b.en(ven, venE, r_fa, b_pro, r_pr, r_pro, r_cli, r_pago, dev, b_cli, b_fac);
        }
        //Texto dentro de jtexfield

    }

    //borrar datos tabla
    public void borrar(DefaultTableModel tabla) {
        while (tabla.getRowCount() > 0) {
            tabla.removeRow(0);
        }
    }

    //registro de pagos
    public void Insertar_Pago(float abon1) {
  
        Conexion_mysql con;
  con=new Conexion_mysql(loading);
  java.sql.Connection cn=con.getConnection();
  
        String Sql = "call PRC_re_pag(?,?,?)";
        try {
            CallableStatement cs = cn.prepareCall(Sql);
            cs.setInt(1, id_ven_fac);
            cs.setFloat(2, abon1);
            cs.setFloat(3, Float.parseFloat(pen.getText()) - abon1);
            if (cs.execute()) {
                JOptionPane.showMessageDialog(null, "Registro Completado");
            }
            cs.close();
        } catch (Exception e) {
        }

    }
    //Mostrar detalle

    //REGISTRO DE PAGOS FACTURA
    public void Insertar_Fact_Pagos(float abon2) {
        
         Conexion_mysql con;
   con=new Conexion_mysql(loading);
  java.sql.Connection cn=con.getConnection();
  
        String Sql = "call PRC_Pag_Ingreso_Factura(?,?,?)";
        try {
            CallableStatement cs = cn.prepareCall(Sql);
            cs.setInt(1, id_ven_fac);
            cs.setFloat(2, abon2);
            cs.setFloat(3, Float.parseFloat(pen.getText()) - abon2);
            if (cs.execute()) {
                JOptionPane.showMessageDialog(null, "Registro Completado");
            }
            cs.close();
        } catch (Exception e) {
        }

    }
    //COnsulta pagos
    //##########################

    public void Mostrar_Pagos() {
        DefaultTableModel modelo3 = new DefaultTableModel();
        borrar(modelo3);
        //agrego columnas de la tabla
        modelo3.addColumn("Abono");
        modelo3.addColumn("Pendiente");
        modelo3.addColumn("Fecha_de_pago");
        tabladatos1.setModel(modelo3);
        String[] datos = new String[10];
        
         Conexion_mysql con;
  con=new Conexion_mysql(loading);
  java.sql.Connection cn=con.getConnection();
  
        String readMetaData = "CALL PRC_cons_tot_pagos(?) ";
        try {
            cn.setAutoCommit(false); // This line must be written just after the 

            CallableStatement cs = cn.prepareCall(readMetaData);
            cs.setInt(1, id_ven_fac);
            //cs.registerOutParameter(2, Types.REF_CURSOR);
            //cs.execute();
            ResultSet rs = (ResultSet) cs.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    datos[0] = rs.getString(2);
                    datos[1] = rs.getString(3);
                    datos[2] = rs.getString(4);
                    modelo3.addRow(datos);
                }//end while
            }//end if
            cs.close();
            rs.close();
        } catch (SQLException ex) {
            System.out.println("Herror" + ex);
        }

    }
    //Consulta de todo los pagos en factura

    public void Mostrar_Pagos_Factura() {
        DefaultTableModel modelo3 = new DefaultTableModel();
        borrar(modelo3);
        //agrego columnas de la tabla
        modelo3.addColumn("Abono");
        modelo3.addColumn("Pendiente");
        modelo3.addColumn("Fecha_de_pago");
        tabladatos1.setModel(modelo3);
        String[] datos = new String[10];
        
         Conexion_mysql con;
  con=new Conexion_mysql(loading);
  java.sql.Connection cn=con.getConnection();
  
        String readMetaData = "CALL PRC_cons_tod_pag_de_Factura(?) ";
        try {
            cn.setAutoCommit(false); // This line must be written just after the 

            CallableStatement cs = cn.prepareCall(readMetaData);
            cs.setInt(1, id_ven_fac);
            //cs.registerOutParameter(2, Types.REF_CURSOR);
            //cs.execute();
            ResultSet rs = (ResultSet) cs.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    datos[0] = rs.getString(1);
                    datos[1] = rs.getString(2);
                    datos[2] = rs.getString(3);
                    modelo3.addRow(datos);
                }//end while
            }//end if
            cs.close();
            rs.close();
        } catch (SQLException ex) {
            System.out.println("Herror" + ex);
        }

    }

    public void Mostrar_Factura() {

        DefaultTableModel modelo1 = new DefaultTableModel();
        //agrego columnas de la tabla
        modelo1.addColumn("Codigo");
        modelo1.addColumn("Numero_Factura");
        modelo1.addColumn("Fecha de Ingreso");
        modelo1.addColumn("Receptor Factura");
        modelo1.addColumn("Vendedor");
        modelo1.addColumn("Proveedor");
        modelo1.addColumn("Total");

        tabladatos.setModel(modelo1);
        borrar(modelo1);
        String[] datos = new String[10];
        
         Conexion_mysql con;
  con=new Conexion_mysql(loading);
  java.sql.Connection cn=con.getConnection();
  
        String readMetaData = "CALL PRC_Cons_Factura_Pago() ";
        try {
            cn.setAutoCommit(false); // This line must be written just after the 

            CallableStatement cs = cn.prepareCall(readMetaData);
           
            //cs.registerOutParameter(2, Types.REF_CURSOR);
            //cs.execute();
            ResultSet rs = (ResultSet) cs.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    datos[0] = rs.getString(1);
                    datos[1] = rs.getString(2);
                    datos[2] = rs.getString(3);
                    datos[3] = rs.getString(4).toUpperCase();
                    datos[4] = rs.getString(5);
                    datos[5] = rs.getString(6);
                    datos[6] = rs.getString(7);

                    modelo1.addRow(datos);

                }//end while
            }//end if
            cs.close();
            rs.close();
        } catch (SQLException ex) {
            System.out.println("Herror" + ex);
        }

    }
    //CONSULTA DE PAGOS DE FACTURA

    public void venta_C(int id_venI) {
        DefaultTableModel modelo2 = new DefaultTableModel();
        //agrego las columnas de las tablas
        modelo2.addColumn("Codigo");
        modelo2.addColumn("Vendedor");
        modelo2.addColumn("Fecha");
        modelo2.addColumn("Venta");
        modelo2.addColumn("Cliente");
        modelo2.addColumn("Apellido");
        modelo2.addColumn("Total");
        modelo2.addColumn("Abono");
        modelo2.addColumn("Pendiente");
        borrar(modelo2);
        tabladatos2.setModel(modelo2);
        String[] datos = new String[10];
  
        Conexion_mysql con;
  con=new Conexion_mysql(loading);
  java.sql.Connection cn=con.getConnection();
  
        String readMetaData = "CALL PRC_Cons_Pago_Ven_C_Pago(?) ";
        try {
            cn.setAutoCommit(false); // This line must be written just after the 

            CallableStatement cs = cn.prepareCall(readMetaData);
            cs.setInt(1, id_venI);
            //cs.registerOutParameter(2, Types.REF_CURSOR);
            //cs.execute();
            ResultSet rs = (ResultSet) cs.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    datos[0] = rs.getString(1);
                    datos[1] = rs.getString(2);
                    datos[2] = rs.getString(3);
                    datos[3] = rs.getString(4);
                    datos[4] = rs.getString(5);
                    datos[5] = rs.getString(6);
                    datos[6] = rs.getString(7);
                    datos[7] = rs.getString(8);
                    datos[8] = rs.getString(9);
                    modelo2.addRow(datos);

                }//end while
            }//end if
            rs.close();
        } catch (SQLException ex) {
            System.out.println("Herror" + ex);
        }
    }

    public void Factura_Pagos(int id_venI) {
        DefaultTableModel modelo2 = new DefaultTableModel();
        //agrego las columnas de las tablas
        modelo2.addColumn("Codigo");
        modelo2.addColumn("Fecha");
        modelo2.addColumn("Total");
        modelo2.addColumn("Abono");
        modelo2.addColumn("Pendiente");

        borrar(modelo2);
        tabladatos2.setModel(modelo2);
        String[] datos = new String[10];
        
         Conexion_mysql con;
   con=new Conexion_mysql(loading);
  java.sql.Connection cn=con.getConnection();
  
        String readMetaData = "CALL PRC_Cons_Det_Fac_Pag(?) ";
        try {
            cn.setAutoCommit(false); // This line must be written just after the 

            CallableStatement cs = cn.prepareCall(readMetaData);
            cs.setInt(1, id_venI);
            //cs.registerOutParameter(2, Types.REF_CURSOR);
            //cs.execute();
            ResultSet rs = (ResultSet) cs.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    datos[0] = rs.getString(1);
                    datos[1] = rs.getString(2);
                    datos[2] = rs.getString(3);
                    datos[3] = rs.getString(4);
                    datos[4] = rs.getString(5);

                    modelo2.addRow(datos);

                }//end while
            }//end if
            rs.close();
        } catch (SQLException ex) {
            System.out.println("Herror" + ex);
        }
    }

    public void Cons_Factura() {
        DefaultTableModel modelo1 = new DefaultTableModel();
        //agrego las columnas de las tablas
        modelo1.addColumn("Codigo");
        modelo1.addColumn("Vendedor");
        modelo1.addColumn("Fecha");
        modelo1.addColumn("Nombre_Cli");
        modelo1.addColumn("Combu.Adom");
        modelo1.addColumn("Descuento");
        modelo1.addColumn("Total");
        modelo1.addColumn("Estado");
        borrar(modelo1);
        tabladatos.setModel(modelo1);
        String[] datos = new String[11];
         
        Conexion_mysql con;
  con=new Conexion_mysql(loading);
  java.sql.Connection cn=con.getConnection();
  
        String readMetaData = "CALL PRC_t_fac_cliente() ";
        try {
            cn.setAutoCommit(false); // This line must be written just after the 

            CallableStatement cs = cn.prepareCall(readMetaData);
          
            //cs.registerOutParameter(3, Types.REF_CURSOR);
            //cs.execute();
            ResultSet rs = (ResultSet) cs.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    datos[0] = rs.getString(1);
                    datos[1] = rs.getString(2);
                    datos[2] = rs.getString(3);
                    datos[3] = rs.getString(4);
                    datos[4] = rs.getString(5);
                    datos[5] = rs.getString(6);
                    datos[6] = rs.getString(7);
                    datos[7] = rs.getString(8);

                    System.out.print("Result =" + rs.getString(8));

                    modelo1.addRow(datos);

                }//end while
            }//end if
            rs.close();
        } catch (SQLException ex) {
            System.out.println("Herror" + ex);
        }
    }

    //MODIFICACION ESTADO
    public void m_estado() {
  
        Conexion_mysql con;
  con=new Conexion_mysql(loading);
  java.sql.Connection cn=con.getConnection();
  
        String Sql = "call PRC_Modificacion_Estado(?)";
        try {
            CallableStatement cs = cn.prepareCall(Sql);
            cs.setInt(1, id_ven_fac);
            if (cs.execute()) {
                JOptionPane.showMessageDialog(null, "Registro Completado");
            }
            cs.close();
        } catch (Exception e) {
        }

    }
    //Modificacion de estado de factura 

    public void m_estado_FACTURA() {
         
        Conexion_mysql con;
  con=new Conexion_mysql(loading);
  java.sql.Connection cn=con.getConnection();
  
        String Sql = "call PRC_Modificacion_Estado_Factura(?)";
        try {
            CallableStatement cs = cn.prepareCall(Sql);
            cs.setInt(1, id_ven_fac);
            if (cs.execute()) {
                JOptionPane.showMessageDialog(null, "Registro Completado");
            }
            cs.close();
        } catch (Exception e) {
        }

    }

    public void Modificar_Pagos() {
        String entrada = JOptionPane.showInputDialog(null, "INGRESA LA CANTIDAD A PAGAR");
        ////

        if (entrada == null) {
            System.out.println("La operacion ha sido cancelada");
        } else {
            try {
                if (!(Integer.parseInt(entrada) <= 0)) {
                    if (entrada.equals("")) {

                        JOptionPane.showMessageDialog(null, "NO HA INGRESADO LA CANTIDAD VUELVE A INGRESARLO", "Error", JOptionPane.ERROR_MESSAGE);
                        String entrada1 = JOptionPane.showInputDialog(null, "CANTIDAD A VENDER");
                        Float pago_entrante = Float.parseFloat(entrada1);
                        if (pago_entrante > Float.parseFloat(pen.getText())) {
                            JOptionPane.showMessageDialog(null, "<html><h1>CANTIDAD MALLOR QUE EL PENDIENTE O MENOR</h1></html>");
                        } else {
                            try {

                                if (pago_entrante == Float.parseFloat(pen.getText())) {
                                    m_estado();
                                    Insertar_Pago(pago_entrante);//mando a insertar en pagos
                                } else {
                                    Insertar_Pago(pago_entrante);//mando a insertar en pagos
                                }
                            } catch (Exception e) {
                                System.out.println("Error" + e);
                            }
                        }
                    } else {
                        Float pago_entrante = Float.parseFloat(entrada);
                        if (pago_entrante > Float.parseFloat(pen.getText())) {
                            JOptionPane.showMessageDialog(null, "<html><h1>CANTIDAD MALLOR QUE EL PENDIENTE</h1></html>");
                        } else {
                            try {

                                if (pago_entrante == Float.parseFloat(pen.getText())) {
                                    m_estado();
                                    Insertar_Pago(pago_entrante);//mando a insertar en pagos

                                } else {
                                    Insertar_Pago(pago_entrante);//mando a insertar en pagos
                                }

                            } catch (Exception e) {
                                System.out.println("Error" + e);
                            }

                        }
                    }

                } else {
                    JOptionPane.showMessageDialog(null, "<html><h1>CANTIDAD NO PUEDE SER MENOR O IGUAL A 0</h1></html>");

                }
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, "<html><h1>Campo Vacio o letra no permitido</h1></html>");

            }
        }

    }
    //MODIFICAR PAGOS DE FACTURA

    public void Modificar_Pagos_Factura() {
        String entrada = JOptionPane.showInputDialog(null, "INGRESA LA CANTIDAD A PAGAR");
        ////
        try {
            if (entrada == null) {
                System.out.println("La operacion ha sido cancelada");
            } else {
                if (!(Integer.parseInt(entrada) <= 0)) {

                    if (entrada.equals("")) {
                        JOptionPane.showMessageDialog(null, "NO HA INGRESADO LA CANTIDAD VUELVE A INGRESARLO", "Error", JOptionPane.ERROR_MESSAGE);
                        String entrada1 = JOptionPane.showInputDialog(null, "CANTIDAD A VENDER");
                        Float pago_entrante = Float.parseFloat(entrada);
                        if (pago_entrante > Float.parseFloat(pen.getText())) {
                            JOptionPane.showMessageDialog(null, "<html><h1>CANTIDAD MALLOR QUE EL PENDIENTE</h1></html>");
                        } else {
                            try {

                                if (pago_entrante == Float.parseFloat(pen.getText())) {
                                    m_estado_FACTURA();
                                    Insertar_Fact_Pagos(pago_entrante);//mando a insertar en pagos
                                } else {
                                    Insertar_Fact_Pagos(pago_entrante);//mando a insertar en pagos
                                }
                            } catch (Exception e) {
                                System.out.println("Error" + e);
                            }
                        }
                    } else {
                        Float pago_entrante = Float.parseFloat(entrada);
                        if (pago_entrante > Float.parseFloat(pen.getText())) {
                            JOptionPane.showMessageDialog(null, "<html><h1>CANTIDAD MALLOR QUE EL PENDIENTE</h1></html>");
                        } else {
                            try {

                                if (pago_entrante == Float.parseFloat(pen.getText())) {
                                    m_estado_FACTURA();
                                    Insertar_Fact_Pagos(pago_entrante);//mando a insertar en pagos

                                } else {
                                    Insertar_Fact_Pagos(pago_entrante);//mando a insertar en pagos
                                }

                            } catch (Exception e) {
                                System.out.println("Error" + e);
                            }
                        }

                    }

                } else {
                    JOptionPane.showMessageDialog(null, "<html><h1>CANTIDAD NO PUEDE SER MENOR O IGUAL A 0</h1></html>");
                }

            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "<html><h1>Campo Vacio o letra no permitido</h1></html>");

        }
    }
    //MOSTRAR DETALLE FACTURA

    public void mostrar_detalle() {
        DefaultTableModel modelo3 = new DefaultTableModel();
        modelo3.addColumn("Codigo");
        modelo3.addColumn("Codigo_Factura");
        modelo3.addColumn("Codigo Producto");
        modelo3.addColumn("Productos");
        modelo3.addColumn("Descripcion");
        modelo3.addColumn("Precio");
        modelo3.addColumn("Marca");
        modelo3.addColumn("Cantidad");
        borrar(modelo2);
        tabladatos4.setModel(modelo3);
        String[] datos = new String[9];
         
        Conexion_mysql con;
  con=new Conexion_mysql(loading);
  java.sql.Connection cn=con.getConnection();
  
        String readMetaData = "CALL  PRC_Cons_Detalle_Busqueda(?,?) ";
        try {
            cn.setAutoCommit(false); // This line must be written just after the 

            CallableStatement cs = cn.prepareCall(readMetaData);
            cs.setString(1, id_Factura);

            cs.registerOutParameter(2, Types.REF_CURSOR);
            cs.execute();
            ResultSet rs = (ResultSet) cs.getObject(2);
            if (rs != null) {
                while (rs.next()) {
                    datos[0] = rs.getString(1).toUpperCase();
                    datos[1] = rs.getString(2).toUpperCase();
                    datos[2] = rs.getString(3).toUpperCase();
                    datos[3] = rs.getString(4).toUpperCase();
                    datos[4] = rs.getString(5).toUpperCase();
                    datos[5] = rs.getString(6).toUpperCase();
                    datos[6] = rs.getString(7).toUpperCase();
                    datos[7] = rs.getString(8).toUpperCase();
                    modelo3.addRow(datos);

                }//end while
            }//end if
            cs.close();
            rs.close();
        } catch (SQLException ex) {
            Logger.getLogger(R_FACTURA.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
public void Filtroo(){
     int ColumntaTabla=3;
     filtro.setRowFilter(RowFilter.regexFilter(busqueda.getText(),ColumntaTabla));
    }    
     public void Filtroo1(){
     int ColumntaTabla=2;
     filtro.setRowFilter(RowFilter.regexFilter(busqueda1.getText(),ColumntaTabla));
    }    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        pnlMenu = new javax.swing.JPanel();
        rcs = new javax.swing.JScrollPane();
        tabladatos4 = new javax.swing.JTable();
        jButton1 = new javax.swing.JButton();
        jLabel5 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        loading = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        pago = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        tabladatos = new javax.swing.JTable();
        busqueda = new javax.swing.JTextField();
        busqueda1 = new javax.swing.JTextField();
        dat2 = new javax.swing.JLabel();
        dat1 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        prov = new javax.swing.JRadioButton();
        cli = new javax.swing.JRadioButton();
        jLabel15 = new javax.swing.JLabel();
        dat3 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tabladatos1 = new javax.swing.JTable();
        jLabel1 = new javax.swing.JLabel();
        tot = new javax.swing.JLabel();
        jScrollPane4 = new javax.swing.JScrollPane();
        tabladatos2 = new javax.swing.JTable();
        jLabel4 = new javax.swing.JLabel();
        jButton7 = new javax.swing.JButton();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        pen = new javax.swing.JLabel();
        pendiente = new javax.swing.JLabel();
        abono = new javax.swing.JLabel();
        abon = new javax.swing.JLabel();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jMenu3 = new javax.swing.JMenu();
        jMenu2 = new javax.swing.JMenu();
        jMenu4 = new javax.swing.JMenu();
        r_pr = new javax.swing.JMenuItem();
        r_em = new javax.swing.JMenuItem();
        r_fa = new javax.swing.JMenuItem();
        r_pro = new javax.swing.JMenuItem();
        r_cli = new javax.swing.JMenuItem();
        jMenu6 = new javax.swing.JMenu();
        b_pro = new javax.swing.JMenuItem();
        b_emp = new javax.swing.JMenuItem();
        b_fac = new javax.swing.JMenuItem();
        b_cli = new javax.swing.JMenuItem();
        b_ventas = new javax.swing.JMenuItem();
        modificacion = new javax.swing.JMenu();
        m_pro = new javax.swing.JMenuItem();
        m_emp = new javax.swing.JMenuItem();
        m_cli = new javax.swing.JMenuItem();
        m_prov = new javax.swing.JMenuItem();
        devolucion = new javax.swing.JMenu();
        dev = new javax.swing.JMenuItem();
        jMenu7 = new javax.swing.JMenu();
        ven = new javax.swing.JMenuItem();
        venE = new javax.swing.JMenuItem();
        ea = new javax.swing.JMenu();
        rep = new javax.swing.JMenuItem();
        jMenu5 = new javax.swing.JMenu();
        r_pago = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setUndecorated(true);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        pnlMenu.setBackground(new java.awt.Color(255, 255, 255));
        pnlMenu.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)), new javax.swing.border.MatteBorder(null)));
        pnlMenu.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        tabladatos4.setFont(new java.awt.Font("Times New Roman", 3, 18)); // NOI18N
        tabladatos4.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "codigo_prod", "Nombre", "Descripción", "Prec_Unit", "Cantidad", "Monto"
            }
        ));
        tabladatos4.setGridColor(new java.awt.Color(255, 255, 255));
        rcs.setViewportView(tabladatos4);

        pnlMenu.add(rcs, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 50, 670, 260));

        jButton1.setBackground(new java.awt.Color(0, 255, 204));
        jButton1.setText("Listo");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        pnlMenu.add(jButton1, new org.netbeans.lib.awtextra.AbsoluteConstraints(660, 320, 60, 30));

        jLabel5.setFont(new java.awt.Font("Tahoma", 3, 24)); // NOI18N
        jLabel5.setText("PRODUCTOS COMPRADOS");
        pnlMenu.add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(210, 10, 340, 30));

        getContentPane().add(pnlMenu, new org.netbeans.lib.awtextra.AbsoluteConstraints(-800, 140, 720, 350));

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setMinimumSize(new java.awt.Dimension(500, 355));
        jPanel1.setLayout(null);

        loading.setIcon(new javax.swing.ImageIcon(getClass().getResource("/main/loading_1.gif"))); // NOI18N
        jPanel1.add(loading);
        loading.setBounds(820, 40, 360, 60);
        jPanel1.add(jLabel2);
        jLabel2.setBounds(32, 52, 0, 30);

        pago.setText("REALIZAR PAGO");
        pago.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                pagoActionPerformed(evt);
            }
        });
        jPanel1.add(pago);
        pago.setBounds(1110, 600, 130, 30);

        tabladatos.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        tabladatos.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Codigo", "Nombre", "Apellido", "TELEFONO", "DIRECCIÓN"
            }
        ));
        tabladatos.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        tabladatos.setSelectionBackground(new java.awt.Color(51, 204, 255));
        tabladatos.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tabladatosMouseClicked(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                tabladatosMousePressed(evt);
            }
        });
        tabladatos.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tabladatosKeyPressed(evt);
            }
        });
        jScrollPane1.setViewportView(tabladatos);

        jPanel1.add(jScrollPane1);
        jScrollPane1.setBounds(10, 180, 830, 210);

        busqueda.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        busqueda.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        busqueda.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                busquedaActionPerformed(evt);
            }
        });
        busqueda.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                busquedaKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                busquedaKeyTyped(evt);
            }
        });
        jPanel1.add(busqueda);
        busqueda.setBounds(470, 20, 170, 30);

        busqueda1.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        busqueda1.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        busqueda1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                busqueda1ActionPerformed(evt);
            }
        });
        busqueda1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                busqueda1KeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                busqueda1KeyTyped(evt);
            }
        });
        jPanel1.add(busqueda1);
        busqueda1.setBounds(470, 90, 170, 30);

        dat2.setBackground(new java.awt.Color(0, 0, 0));
        dat2.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        dat2.setForeground(new java.awt.Color(0, 51, 51));
        jPanel1.add(dat2);
        dat2.setBounds(360, 90, 100, 30);

        dat1.setBackground(new java.awt.Color(0, 0, 0));
        dat1.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        dat1.setForeground(new java.awt.Color(0, 51, 51));
        jPanel1.add(dat1);
        dat1.setBounds(340, 20, 120, 30);

        jLabel16.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/PAGO1.png"))); // NOI18N
        jPanel1.add(jLabel16);
        jLabel16.setBounds(70, 90, 60, 61);

        prov.setBackground(new java.awt.Color(255, 255, 255));
        buttonGroup1.add(prov);
        prov.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        prov.setText("PROVEEDOR");
        prov.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                provMouseClicked(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                provMousePressed(evt);
            }
        });
        jPanel1.add(prov);
        prov.setBounds(130, 120, 150, 25);

        cli.setBackground(new java.awt.Color(255, 255, 255));
        buttonGroup1.add(cli);
        cli.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        cli.setText("CLIENTE");
        cli.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                cliMouseClicked(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                cliMousePressed(evt);
            }
        });
        jPanel1.add(cli);
        cli.setBounds(130, 60, 150, 25);

        jLabel15.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/PAG2.png"))); // NOI18N
        jPanel1.add(jLabel15);
        jLabel15.setBounds(80, 30, 75, 60);

        dat3.setBackground(new java.awt.Color(0, 0, 0));
        dat3.setFont(new java.awt.Font("Times New Roman", 3, 18)); // NOI18N
        dat3.setForeground(new java.awt.Color(0, 51, 51));
        jPanel1.add(dat3);
        dat3.setBounds(230, 100, 160, 30);

        tabladatos1.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        tabladatos1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null}
            },
            new String [] {
                "Pagos", "Pendiente", "Fecha"
            }
        ));
        tabladatos1.setSelectionBackground(new java.awt.Color(51, 204, 255));
        jScrollPane2.setViewportView(tabladatos1);

        jPanel1.add(jScrollPane2);
        jScrollPane2.setBounds(860, 180, 360, 210);

        jLabel1.setFont(new java.awt.Font("Times New Roman", 1, 24)); // NOI18N
        jLabel1.setText("Detalles");
        jPanel1.add(jLabel1);
        jLabel1.setBounds(30, 400, 270, 20);

        tot.setFont(new java.awt.Font("Times New Roman", 1, 24)); // NOI18N
        tot.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255)));
        jPanel1.add(tot);
        tot.setBounds(770, 20, 130, 30);

        tabladatos2.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        tabladatos2.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tabladatos2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                tabladatos2MousePressed(evt);
            }
        });
        tabladatos2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tabladatos2KeyPressed(evt);
            }
        });
        jScrollPane4.setViewportView(tabladatos2);

        jPanel1.add(jScrollPane4);
        jScrollPane4.setBounds(10, 430, 1220, 160);

        jLabel4.setFont(new java.awt.Font("Times New Roman", 1, 24)); // NOI18N
        jLabel4.setText("DETALLE DE PAGOS");
        jPanel1.add(jLabel4);
        jLabel4.setBounds(910, 140, 270, 30);

        jButton7.setFont(new java.awt.Font("Tahoma", 3, 24)); // NOI18N
        jButton7.setForeground(new java.awt.Color(255, 255, 255));
        jButton7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/salir.png"))); // NOI18N
        jButton7.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jButton7.setIconTextGap(1);
        jButton7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton7ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton7);
        jButton7.setBounds(1150, 10, 50, 40);

        jLabel6.setBackground(new java.awt.Color(255, 255, 255));
        jLabel6.setFont(new java.awt.Font("Tahoma", 3, 14)); // NOI18N
        jLabel6.setText("SALIR");
        jPanel1.add(jLabel6);
        jLabel6.setBounds(1150, 50, 44, 17);

        jLabel7.setFont(new java.awt.Font("Times New Roman", 1, 24)); // NOI18N
        jLabel7.setText("Total Q.");
        jPanel1.add(jLabel7);
        jLabel7.setBounds(680, 20, 90, 29);

        jLabel8.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        jLabel8.setText("Pagos Pendientes");
        jPanel1.add(jLabel8);
        jLabel8.setBounds(260, 150, 270, 30);

        jLabel3.setBackground(new java.awt.Color(0, 255, 255));
        jLabel3.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        jLabel3.setText("VER PRODUCTOS");
        jLabel3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel3MouseClicked(evt);
            }
        });
        jPanel1.add(jLabel3);
        jLabel3.setBounds(640, 390, 200, 40);

        getContentPane().add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 1240, 670));

        pen.setFont(new java.awt.Font("Tahoma", 3, 24)); // NOI18N
        pen.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        getContentPane().add(pen, new org.netbeans.lib.awtextra.AbsoluteConstraints(1110, 120, 100, 40));

        pendiente.setBackground(new java.awt.Color(0, 0, 0));
        pendiente.setFont(new java.awt.Font("Times New Roman", 3, 18)); // NOI18N
        pendiente.setForeground(new java.awt.Color(0, 51, 51));
        pendiente.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        getContentPane().add(pendiente, new org.netbeans.lib.awtextra.AbsoluteConstraints(980, 130, 120, 30));

        abono.setBackground(new java.awt.Color(0, 0, 0));
        abono.setFont(new java.awt.Font("Times New Roman", 3, 18)); // NOI18N
        abono.setForeground(new java.awt.Color(0, 51, 51));
        abono.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        getContentPane().add(abono, new org.netbeans.lib.awtextra.AbsoluteConstraints(980, 60, 120, 30));

        abon.setFont(new java.awt.Font("Tahoma", 3, 24)); // NOI18N
        abon.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        getContentPane().add(abon, new org.netbeans.lib.awtextra.AbsoluteConstraints(1110, 50, 100, 40));

        jMenuBar1.setBorder(null);
        jMenuBar1.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jMenuBar1.setFont(new java.awt.Font("Segoe UI", 2, 12)); // NOI18N

        jMenu1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/can.png"))); // NOI18N
        jMenu1.setText("LOGIN");

        jMenu3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Fondo.png"))); // NOI18N
        jMenu3.setText("LOGIN");
        jMenu3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jMenu3MouseClicked(evt);
            }
        });
        jMenu1.add(jMenu3);

        jMenuBar1.add(jMenu1);
        jMenuBar1.add(jMenu2);

        jMenu4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/add (1).png"))); // NOI18N
        jMenu4.setText(" REGISTRO");

        r_pr.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_R, java.awt.event.InputEvent.CTRL_DOWN_MASK));
        r_pr.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/ingreso.png"))); // NOI18N
        r_pr.setText("PRODUCTO");
        r_pr.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                r_prActionPerformed(evt);
            }
        });
        jMenu4.add(r_pr);

        r_em.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_E, java.awt.event.InputEvent.CTRL_DOWN_MASK));
        r_em.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/emple.png"))); // NOI18N
        r_em.setText("EMPLEADOS");
        r_em.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                r_emActionPerformed(evt);
            }
        });
        jMenu4.add(r_em);

        r_fa.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F, java.awt.event.InputEvent.CTRL_DOWN_MASK));
        r_fa.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/REGISTRO FACTURA.png"))); // NOI18N
        r_fa.setText("FACTURA");
        r_fa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                r_faActionPerformed(evt);
            }
        });
        jMenu4.add(r_fa);

        r_pro.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_P, java.awt.event.InputEvent.CTRL_DOWN_MASK));
        r_pro.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/proveNue.png"))); // NOI18N
        r_pro.setText("PROVEEDOR");
        r_pro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                r_proActionPerformed(evt);
            }
        });
        jMenu4.add(r_pro);

        r_cli.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_C, java.awt.event.InputEvent.CTRL_DOWN_MASK));
        r_cli.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/cliente.png"))); // NOI18N
        r_cli.setText("CLIENTE");
        r_cli.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                r_cliActionPerformed(evt);
            }
        });
        jMenu4.add(r_cli);

        jMenuBar1.add(jMenu4);

        jMenu6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Busqueda (1).png"))); // NOI18N
        jMenu6.setText("BUSQUEDA");

        b_pro.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_P, java.awt.event.InputEvent.ALT_DOWN_MASK));
        b_pro.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/prodddd.png"))); // NOI18N
        b_pro.setText("PRODUCTOS");
        b_pro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                b_proActionPerformed(evt);
            }
        });
        jMenu6.add(b_pro);

        b_emp.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_E, java.awt.event.InputEvent.ALT_DOWN_MASK));
        b_emp.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/busPer .png"))); // NOI18N
        b_emp.setText("EMPLEADOS");
        b_emp.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                b_empActionPerformed(evt);
            }
        });
        jMenu6.add(b_emp);

        b_fac.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F, java.awt.event.InputEvent.ALT_DOWN_MASK));
        b_fac.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/busqueda_factura.png"))); // NOI18N
        b_fac.setText("FACTURA");
        b_fac.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                b_facActionPerformed(evt);
            }
        });
        jMenu6.add(b_fac);

        b_cli.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_C, java.awt.event.InputEvent.ALT_DOWN_MASK));
        b_cli.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/bus_em.png"))); // NOI18N
        b_cli.setText("CLIENTE");
        b_cli.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                b_cliActionPerformed(evt);
            }
        });
        jMenu6.add(b_cli);

        b_ventas.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_Z, java.awt.event.InputEvent.ALT_DOWN_MASK));
        b_ventas.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Busqueda1.png"))); // NOI18N
        b_ventas.setText("VENTAS");
        b_ventas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                b_ventasActionPerformed(evt);
            }
        });
        jMenu6.add(b_ventas);

        jMenuBar1.add(jMenu6);

        modificacion.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/modificacion.png"))); // NOI18N
        modificacion.setText("MODIFICAR");

        m_pro.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_K, java.awt.event.InputEvent.ALT_DOWN_MASK));
        m_pro.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/PRODUCTO.png"))); // NOI18N
        m_pro.setText("PRODUCTO");
        m_pro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                m_proActionPerformed(evt);
            }
        });
        modificacion.add(m_pro);

        m_emp.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_E, java.awt.event.InputEvent.SHIFT_DOWN_MASK));
        m_emp.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/empleado.png"))); // NOI18N
        m_emp.setText("EMPLEADO");
        m_emp.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                m_empActionPerformed(evt);
            }
        });
        modificacion.add(m_emp);

        m_cli.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_C, java.awt.event.InputEvent.SHIFT_DOWN_MASK));
        m_cli.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/clientee.png"))); // NOI18N
        m_cli.setText("CLIENTE");
        m_cli.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                m_cliActionPerformed(evt);
            }
        });
        modificacion.add(m_cli);

        m_prov.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_P, java.awt.event.InputEvent.SHIFT_DOWN_MASK));
        m_prov.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/proveedores.png"))); // NOI18N
        m_prov.setText("PROVEEDORES");
        m_prov.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                m_provActionPerformed(evt);
            }
        });
        modificacion.add(m_prov);

        jMenuBar1.add(modificacion);

        devolucion.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/DEV.png"))); // NOI18N
        devolucion.setText("DEVOULUCION");

        dev.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_D, java.awt.event.InputEvent.CTRL_DOWN_MASK));
        dev.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/dev1.png"))); // NOI18N
        dev.setText("DEVOLUCION");
        dev.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                devActionPerformed(evt);
            }
        });
        devolucion.add(dev);

        jMenuBar1.add(devolucion);

        jMenu7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/png-transparent-shopping-cart-graphy-cart-supermarket-vehicle-shopping-bags-trolleys (1) (1).png"))); // NOI18N
        jMenu7.setText("VENTA");

        ven.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_V, java.awt.event.InputEvent.CTRL_DOWN_MASK));
        ven.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/venta a credito.png"))); // NOI18N
        ven.setText("VENTA  A CREDITO");
        ven.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                venActionPerformed(evt);
            }
        });
        jMenu7.add(ven);

        venE.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_B, java.awt.event.InputEvent.CTRL_DOWN_MASK));
        venE.setIcon(new javax.swing.ImageIcon(getClass().getResource("/main/venta efectivo.png"))); // NOI18N
        venE.setText("VENTA EN EFECTIVO");
        venE.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                venEActionPerformed(evt);
            }
        });
        jMenu7.add(venE);

        jMenuBar1.add(jMenu7);

        ea.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/REPORTE.png"))); // NOI18N
        ea.setText("REPORTE");

        rep.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_R, java.awt.event.InputEvent.ALT_DOWN_MASK));
        rep.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/reportess.png"))); // NOI18N
        rep.setText(" REPORTES");
        rep.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                repActionPerformed(evt);
            }
        });
        ea.add(rep);

        jMenuBar1.add(ea);

        jMenu5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Pago_C_P.png"))); // NOI18N
        jMenu5.setText("PAGOS");

        r_pago.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Pago_C_P (2).png"))); // NOI18N
        r_pago.setText("REALIZAR PAGOS");
        r_pago.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                r_pagoActionPerformed(evt);
            }
        });
        jMenu5.add(r_pago);

        jMenuBar1.add(jMenu5);

        setJMenuBar(jMenuBar1);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jMenu3MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jMenu3MouseClicked
        Login l = new Login();
        l.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_jMenu3MouseClicked

    private void r_prActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_r_prActionPerformed
        R_PROD a = new R_PROD();
        a.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_r_prActionPerformed

    private void r_emActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_r_emActionPerformed
        R_EMPL a = new R_EMPL();
        a.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_r_emActionPerformed

    private void r_faActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_r_faActionPerformed
        R_FACTURA a = new R_FACTURA();
        a.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_r_faActionPerformed

    private void r_proActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_r_proActionPerformed
        R_PROVEE a = new R_PROVEE();
        a.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_r_proActionPerformed

    private void r_cliActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_r_cliActionPerformed

        R_CLI A = new R_CLI();
        A.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_r_cliActionPerformed

    private void b_proActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_b_proActionPerformed
        Busqueda_Productos a = new Busqueda_Productos();
        a.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_b_proActionPerformed

    private void b_empActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_b_empActionPerformed
        Busqueda_Empleados a = new Busqueda_Empleados();
        a.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_b_empActionPerformed

    private void b_facActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_b_facActionPerformed
        Busqueda_Facturas a = new Busqueda_Facturas();
        a.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_b_facActionPerformed

    private void b_cliActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_b_cliActionPerformed
        Busqueda_Clientes a = new Busqueda_Clientes();
        a.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_b_cliActionPerformed

    private void b_ventasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_b_ventasActionPerformed
        Busqueda_Venta P = new Busqueda_Venta();
        this.setVisible(false);
        P.setVisible(true);
    }//GEN-LAST:event_b_ventasActionPerformed

    private void m_proActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_m_proActionPerformed
        m_prod a = new m_prod();
        a.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_m_proActionPerformed

    private void m_empActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_m_empActionPerformed
        m_empl a = new m_empl();
        a.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_m_empActionPerformed

    private void m_cliActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_m_cliActionPerformed
        m_cliente a = new m_cliente();
        a.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_m_cliActionPerformed

    private void m_provActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_m_provActionPerformed
        m_prov a = new m_prov();
        a.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_m_provActionPerformed

    private void devActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_devActionPerformed
        Devolucion l = new Devolucion();
        l.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_devActionPerformed

    private void venActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_venActionPerformed
        Ventas_Cred a = new Ventas_Cred();
        a.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_venActionPerformed

    private void venEActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_venEActionPerformed
        Ventas a = new Ventas();
        a.setVisible(true);
        this.setVisible(false);

    }//GEN-LAST:event_venEActionPerformed

    private void repActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_repActionPerformed
        Reportes r = new Reportes();
        r.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_repActionPerformed

    private void r_pagoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_r_pagoActionPerformed
        PAGO P = new PAGO();
        this.setVisible(false);
        P.setVisible(true);
    }//GEN-LAST:event_r_pagoActionPerformed

    private void jButton7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton7ActionPerformed
        System.exit(0);
    }//GEN-LAST:event_jButton7ActionPerformed

    private void tabladatos2KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tabladatos2KeyPressed

    }//GEN-LAST:event_tabladatos2KeyPressed

    private void tabladatos2MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tabladatos2MousePressed
         Thread thread = new Thread() {
            public void run() {
 try {
            if (cli.isSelected()) {
                int seleccion = tabladatos2.rowAtPoint(evt.getPoint());
                tot.setText(String.valueOf(tabladatos2.getValueAt(seleccion, 6)));
                abon.setText(String.valueOf(tabladatos2.getValueAt(seleccion, 7)));
                pen.setText(String.valueOf(tabladatos2.getValueAt(seleccion, 8)));
                Mostrar_Pagos();
            } else if (prov.isSelected()) {
                int seleccion = tabladatos2.rowAtPoint(evt.getPoint());
                tot.setText(String.valueOf(tabladatos2.getValueAt(seleccion, 2)));
                abon.setText(String.valueOf(tabladatos2.getValueAt(seleccion, 3)));
                pen.setText(String.valueOf(tabladatos2.getValueAt(seleccion, 4)));
                Mostrar_Pagos_Factura();
            }
        } catch (Exception e) {
        }
            }
        };

        thread.start();
       

    }//GEN-LAST:event_tabladatos2MousePressed

    private void cliMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_cliMousePressed
Thread thread = new Thread() {
            public void run() {
        DefaultTableModel modelo1 = new DefaultTableModel();
        modelo1.addColumn("Codigo");
        modelo1.addColumn("Vendedor");
        modelo1.addColumn("Fecha");
        modelo1.addColumn("Tipo_Venta");
        modelo1.addColumn("Nombre_Cli");
        modelo1.addColumn("Apellido_Cli");
        modelo1.addColumn("Total");
        modelo1.addColumn("Abono");
        modelo1.addColumn("Pendiente");

        tabladatos.setModel(modelo1);
        busqueda1.setEnabled(true);
        busqueda.setEnabled(true);
        dat1.setText("NOMBRE");
        dat2.setText("FECHA");        // TODO add your handling code here:
        pendiente.setText("PENDIENTE");
        abono.setText("ABONO");

        Cons_Factura();
         }
        };

        thread.start();
    }//GEN-LAST:event_cliMousePressed

    private void cliMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_cliMouseClicked

    }//GEN-LAST:event_cliMouseClicked

    private void provMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_provMousePressed
Thread thread = new Thread() {
            public void run() {
        DefaultTableModel modelo1 = new DefaultTableModel();
        //agrego columnas de la tabla
        modelo1.addColumn("Codigo");
        modelo1.addColumn("Vendedor");
        modelo1.addColumn("Fecha");
        modelo1.addColumn("Nombre_Cli");
        modelo1.addColumn("Combu.Adom");
        modelo1.addColumn("Descuento");
        modelo1.addColumn("Total");
        modelo1.addColumn("Estado");
        tabladatos.setModel(modelo1);
        busqueda1.setEnabled(true);
        busqueda.setEnabled(true);
        pendiente.setText("PENDIENTE");
        abono.setText("ABONO");
        dat1.setText("PROVEEDOR");
        dat2.setText("--------");
        busqueda1.setEnabled(false);
        // TODO add your handling code here:
        Mostrar_Factura();
          }
        };

        thread.start();
    }//GEN-LAST:event_provMousePressed

    private void provMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_provMouseClicked

    }//GEN-LAST:event_provMouseClicked

    private void busqueda1KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_busqueda1KeyReleased

       
    }//GEN-LAST:event_busqueda1KeyReleased

    private void busqueda1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_busqueda1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_busqueda1ActionPerformed

    private void busquedaKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_busquedaKeyTyped

busqueda.addKeyListener(new KeyAdapter(){
    public void keyReleased(final KeyEvent e){
    String cadena=(busqueda.getText().toUpperCase());
    busqueda.setText(cadena);
    Filtroo();}
   
    
   });
   filtro=new TableRowSorter(tabladatos.getModel());
   tabladatos.setRowSorter(filtro);

    }//GEN-LAST:event_busquedaKeyTyped

    private void busquedaKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_busquedaKeyReleased
      

    }//GEN-LAST:event_busquedaKeyReleased

    private void tabladatosKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tabladatosKeyPressed

    }//GEN-LAST:event_tabladatosKeyPressed

    private void tabladatosMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tabladatosMousePressed
  Thread thread = new Thread() {
            public void run() {
                  tot.setText("");
 pago.setEnabled(true);

        int seleccion = tabladatos.rowAtPoint(evt.getPoint());
        if (prov.isSelected()) {
            id_ven_fac = Integer.parseInt(String.valueOf(tabladatos.getValueAt(seleccion, 0)));
            id_Factura = String.valueOf(tabladatos.getValueAt(seleccion, 1));  // TODO add your handling code here:

            Factura_Pagos(id_ven_fac);
        } else if (cli.isSelected()) {
            id_ven_fac = Integer.parseInt(String.valueOf(tabladatos.getValueAt(seleccion, 0)));
            venta_C(id_ven_fac);
        }
            }
        };

        thread.start();
       

    }//GEN-LAST:event_tabladatosMousePressed

    private void tabladatosMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tabladatosMouseClicked

    }//GEN-LAST:event_tabladatosMouseClicked

    private void pagoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_pagoActionPerformed
Thread thread = new Thread() {
            public void run() {
        int cuentaFilasSeleccionadas = tabladatos2.getSelectedRowCount();
        if (cuentaFilasSeleccionadas == 0) {
            Icon m = new ImageIcon(getClass().getResource("/img/cancelar.png"));
            JOptionPane.showMessageDialog(null,
                    "<html><h1 style=\"color:red;\">NO HAY FILA SELECCIONADA EN LA TABLA DETALLE</h1></html>", "Error", JOptionPane.INFORMATION_MESSAGE, m);
        } else {
            if (cli.isSelected()) {
                DefaultTableModel modelo1 = new DefaultTableModel();
                DefaultTableModel modelo2 = new DefaultTableModel();
                DefaultTableModel modelo3 = new DefaultTableModel();
                modelo1.addColumn("Codigo");
                modelo1.addColumn("Vendedor");
                modelo1.addColumn("Fecha");
                modelo1.addColumn("Tipo_Venta");
                modelo1.addColumn("Nombre_Cli");
                modelo1.addColumn("Apellido_Cli");
                modelo1.addColumn("Total");
                modelo1.addColumn("Abono");
                modelo1.addColumn("Pendiente");
                Modificar_Pagos();

                borrar(modelo1);
                borrar(modelo2);
                borrar(modelo3);
                tabladatos.setModel(modelo1);
                tabladatos2.setModel(modelo2);
                tabladatos1.setModel(modelo3);
                Cons_Factura();
                // venta_C();
                busqueda.setText("");
                busqueda1.setText("");
                abon.setText("");
                pen.setText("");
            } else if (prov.isSelected()) {
                DefaultTableModel modelo1 = new DefaultTableModel();

                DefaultTableModel modelo2 = new DefaultTableModel();
                DefaultTableModel modelo3 = new DefaultTableModel();
                //agrego columnas de la tabla
                modelo1.addColumn("Codigo");
                modelo1.addColumn("Numero_Factura");
                modelo1.addColumn("Fecha de Ingreso");
                modelo1.addColumn("Proveedor");
                modelo1.addColumn("Total");
                modelo1.addColumn("Abono");
                modelo1.addColumn("Pendiente");
                modelo1.addColumn("Receptor Factura");
                modelo1.addColumn("Vendedor");
                Modificar_Pagos_Factura();
                borrar(modelo1);
                borrar(modelo2);
                borrar(modelo3);
                tabladatos.setModel(modelo1);
                tabladatos2.setModel(modelo2);
                tabladatos1.setModel(modelo3);
                tabladatos.setModel(modelo1);
                Mostrar_Factura();
                busqueda.setText("");
                busqueda1.setText("");
                abon.setText("");
                pen.setText("");
            }

        }
  }
        };

        thread.start();
    }//GEN-LAST:event_pagoActionPerformed
    public void mostrar_detalle_venta() {

        String[] datos = new String[9];
          Conexion_mysql con;
  con=new Conexion_mysql(loading);
  java.sql.Connection cn=con.getConnection();
        String readMetaData = "CALL  detalle_venta(?,?) ";
        try {
            cn.setAutoCommit(false); // This line must be written just after the 

            CallableStatement cs = cn.prepareCall(readMetaData);
            cs.setInt(1, id_ven_fac);

            cs.registerOutParameter(2, Types.REF_CURSOR);
            cs.execute();
            ResultSet rs = (ResultSet) cs.getObject(2);
            if (rs != null) {
                while (rs.next()) {
                    datos[0] = rs.getString(2).toUpperCase();
                    datos[1] = rs.getString(3).toUpperCase();
                    datos[2] = rs.getString(4).toUpperCase();
                    datos[3] = rs.getString(5).toUpperCase();
                    datos[4] = rs.getString(6);
                    datos[5] = rs.getString(7);
                    datos[6] = Float.toString(rs.getFloat(6) * rs.getInt(7));
                    modelo2.addRow(datos);

                }//end while
            }//end if
            cs.close();
            rs.close();
        } catch (SQLException ex) {
            Logger.getLogger(R_FACTURA.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed

        //  Animacion.Animacion.mover_izquierda(140,-790, 2, 2, pnlMenu); 

    }//GEN-LAST:event_jButton1ActionPerformed

    private void jLabel3MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel3MouseClicked
        if (cli.isSelected()) {

            //  Animacion.Animacion.mover_izquierda(140,130, 2, 1, pnlMenu);
            borrar(modelo2);
            mostrar_detalle_venta();
        } else if (prov.isSelected()) {
            //  Animacion.Animacion.mover_izquierda(140,130, 2, 1, pnlMenu);
            mostrar_detalle();
        }

    }//GEN-LAST:event_jLabel3MouseClicked

    private void busquedaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_busquedaActionPerformed

    }//GEN-LAST:event_busquedaActionPerformed

    private void busqueda1KeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_busqueda1KeyTyped

busqueda1.addKeyListener(new KeyAdapter(){
    public void keyReleased(final KeyEvent e){
    String cadena=(busqueda1.getText().toUpperCase());
    busqueda1.setText(cadena);
    Filtroo1();}
   
    
   });
   filtro=new TableRowSorter(tabladatos.getModel());
   tabladatos.setRowSorter(filtro);    }//GEN-LAST:event_busqueda1KeyTyped

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Windows".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(PAGO.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(PAGO.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(PAGO.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(PAGO.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new PAGO().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel abon;
    private javax.swing.JLabel abono;
    private javax.swing.JMenuItem b_cli;
    private javax.swing.JMenuItem b_emp;
    private javax.swing.JMenuItem b_fac;
    private javax.swing.JMenuItem b_pro;
    private javax.swing.JMenuItem b_ventas;
    private javax.swing.JTextField busqueda;
    private javax.swing.JTextField busqueda1;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JRadioButton cli;
    private javax.swing.JLabel dat1;
    private javax.swing.JLabel dat2;
    private javax.swing.JLabel dat3;
    private javax.swing.JMenuItem dev;
    private javax.swing.JMenu devolucion;
    private javax.swing.JMenu ea;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton7;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenu jMenu3;
    private javax.swing.JMenu jMenu4;
    private javax.swing.JMenu jMenu5;
    private javax.swing.JMenu jMenu6;
    private javax.swing.JMenu jMenu7;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JLabel loading;
    private javax.swing.JMenuItem m_cli;
    private javax.swing.JMenuItem m_emp;
    private javax.swing.JMenuItem m_pro;
    private javax.swing.JMenuItem m_prov;
    private javax.swing.JMenu modificacion;
    private javax.swing.JButton pago;
    private javax.swing.JLabel pen;
    private javax.swing.JLabel pendiente;
    private javax.swing.JPanel pnlMenu;
    private javax.swing.JRadioButton prov;
    private javax.swing.JMenuItem r_cli;
    private javax.swing.JMenuItem r_em;
    private javax.swing.JMenuItem r_fa;
    private javax.swing.JMenuItem r_pago;
    private javax.swing.JMenuItem r_pr;
    private javax.swing.JMenuItem r_pro;
    private javax.swing.JScrollPane rcs;
    private javax.swing.JMenuItem rep;
    private javax.swing.JTable tabladatos;
    private javax.swing.JTable tabladatos1;
    private javax.swing.JTable tabladatos2;
    private javax.swing.JTable tabladatos4;
    private javax.swing.JLabel tot;
    private javax.swing.JMenuItem ven;
    private javax.swing.JMenuItem venE;
    // End of variables declaration//GEN-END:variables
}
