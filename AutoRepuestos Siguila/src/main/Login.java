/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import clases.TextPrompt;
import clases.Conexion_postgres;
import clases.Activacion;
import clases.Conexion_mysql;
import clases.variables;
import java.awt.event.KeyEvent;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import org.apache.commons.codec.digest.DigestUtils;


/**
 *
 * @author Leo
 */
public class Login extends javax.swing.JFrame {
    variables v=new variables();
    Conexion_postgres cn=new  Conexion_postgres ();
String usuario,pas,usuario_post,pas_post,Rol;
    public Login() {
        initComponents();
        loading.setVisible(false);
        this.setLocationRelativeTo(null);
        TextPrompt p=new TextPrompt("Ingrese usuario",us);
            TextPrompt q=new TextPrompt("Ingrese Contraseña",pass);
             
    }

    public void  Consulta_Empleado(){   
    Conexion_mysql con;
    con=new Conexion_mysql(loading);
  java.sql.Connection cn=con.getConnection();
  
    String readMetaData = "CALL PRC_Cons_Empleado(?,?)";
    Activacion ac=new Activacion();
     try {
         cn.setAutoCommit(false); // This line must be written just after the 

         CallableStatement cs= cn.prepareCall(readMetaData);
         cs.setString(1, usuario);
         cs.setString(2, pas);
       //  cs.registerOutParameter(3, Types.REF_CURSOR);
//cs.execute();
//ResultSet rs = (ResultSet) cs.getObject(3);
ResultSet rs = (ResultSet) cs.executeQuery();
if (rs != null) {
    while (rs.next()) {       
          ac.codigo_log=rs.getInt(1);
          usuario_post=rs.getString(2);
        pas_post=rs.getString(3);
        Rol=rs.getString(4).toUpperCase();
        ac.nom_log=rs.getString(5).toUpperCase();
                }//end while
            }//end if
 System.out.print("\nDatos "+usuario_post+"\n"+pas_post+"\n"+Rol);
 v.setTipoEmpleado(Rol);
     } catch (SQLException ex) {
         Logger.getLogger(R_FACTURA.class.getName()).log(Level.SEVERE, null, ex);
     }


}//end if
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jButton1 = new javax.swing.JButton();
        loading = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        pass = new rscomponentshade.RSPassFieldShade();
        us = new rscomponentshade.RSTextFieldShade();
        jLabel5 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setUndecorated(true);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jButton1.setText("Menu");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton1, new org.netbeans.lib.awtextra.AbsoluteConstraints(510, 320, 70, 40));

        loading.setIcon(new javax.swing.ImageIcon(getClass().getResource("/main/loading_1.gif"))); // NOI18N
        jPanel1.add(loading, new org.netbeans.lib.awtextra.AbsoluteConstraints(280, 40, 320, 90));

        jLabel1.setFont(new java.awt.Font("Times New Roman", 3, 36)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("Usuario");
        jPanel1.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(260, 110, -1, -1));

        jLabel2.setFont(new java.awt.Font("Times New Roman", 3, 48)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setText("LOGIN");
        jPanel1.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 0, 180, -1));

        jLabel6.setFont(new java.awt.Font("Times New Roman", 3, 36)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(255, 255, 255));
        jLabel6.setText("Contraseña");
        jPanel1.add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(260, 200, -1, -1));

        jLabel3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/log2.png"))); // NOI18N
        jPanel1.add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 50, 240, 270));

        pass.setPlaceholder("");
        pass.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                passKeyReleased(evt);
            }
        });
        jPanel1.add(pass, new org.netbeans.lib.awtextra.AbsoluteConstraints(260, 240, 240, 50));

        us.setPlaceholder("");
        us.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                usKeyReleased(evt);
            }
        });
        jPanel1.add(us, new org.netbeans.lib.awtextra.AbsoluteConstraints(260, 150, 240, 50));

        jLabel5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/3D_Black_Abstract-High_quality_HD_Wallpaper_1366x768.jpg"))); // NOI18N
        jPanel1.add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 600, 380));

        getContentPane().add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, -1, -1));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void usKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_usKeyReleased
      if(evt.getKeyCode() == KeyEvent.VK_ENTER){
            pass.requestFocus();
        }
    }//GEN-LAST:event_usKeyReleased

    private void passKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_passKeyReleased

        if(evt.getKeyCode() == KeyEvent.VK_ENTER){
            String valorPass = new String(pass.getPassword());
            pas=DigestUtils.md5Hex(valorPass);
            usuario=us.getText();       
     Consulta_Empleado();
     if(pas.equals(pas_post) &&usuario.equals(usuario_post)&&Rol.equals("ADMINISTRADOR") ){
                Activacion b=new Activacion();
                b.ver=1;
                this.setVisible(false);
                Menu_PRIN a=new Menu_PRIN();
                a.setVisible(true);
                System.out.print(b.ver);
            } else if(pas.equals(pas_post) &&usuario.equals(usuario_post)&&Rol.equals("VENTA")){
                Activacion b=new Activacion();
                b.ver=2;
                this.setVisible(false);
                Menu_PRIN a=new Menu_PRIN();
                a.setVisible(true);

            }else{
                JOptionPane.showMessageDialog(null,"Usuario o Contraseña Incorrecta","Error al ingresar",JOptionPane.ERROR_MESSAGE);
            }

        }
    }//GEN-LAST:event_passKeyReleased

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
Menu_PRIN m=new Menu_PRIN();
m.setVisible(true);

this.setVisible(false);
    }//GEN-LAST:event_jButton1ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Login.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Login.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Login.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Login.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Login().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JLabel loading;
    private rscomponentshade.RSPassFieldShade pass;
    private rscomponentshade.RSTextFieldShade us;
    // End of variables declaration//GEN-END:variables
}
