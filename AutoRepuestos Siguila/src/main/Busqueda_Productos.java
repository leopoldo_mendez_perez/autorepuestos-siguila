/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;
import clases.TextPrompt;
import clases.Conexion_postgres;
import clases.Activacion;
import clases.Conexion_mysql;
import java.awt.Image;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Method;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.RowFilter;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;

/**
 *
 * @author YUVILSA
 */
public class Busqueda_Productos extends javax.swing.JFrame {

    String ruta = null;
    Conexion_postgres cn = new Conexion_postgres();
    int codigoRecivido;
    private TableRowSorter filtro;

    DefaultTableModel modelo = new DefaultTableModel();
    String Nombre_E, Descripcion_E, Marca_E;
    //Filtro por nombre Producto

    public Busqueda_Productos() {
        initComponents();


        loading.setVisible(false);
        modelo.addColumn("Id_Prod");
        modelo.addColumn("Nombre_Prod");
        modelo.addColumn("Descripcion_Prod");
        modelo.addColumn("Marca");
        modelo.addColumn("Precio_Com");
        modelo.addColumn("Precio_Ven");
        modelo.addColumn("Estado_Prod");
        modelo.addColumn("Existencia");
        tabladatos.setModel(modelo);
        //TEXTO EN JTEXTFIELD
        TextPrompt p = new TextPrompt("Ingrese Nombre Producto", busqueda);
        TextPrompt q = new TextPrompt("Ingrese Descripción", busqueda1);
        TextPrompt r = new TextPrompt("Ingrese Marca", busqueda2);
//TABLA TRANSPARENTE
//segunda tabla
tabladatos.setOpaque(false);
jScrollPane2.setOpaque(false);
jScrollPane2.getViewport().setOpaque(false);
        this.setLocationRelativeTo(null);
        //ACTIVACION
        r_pr.setEnabled(false);
        r_pro.setEnabled(false);
        r_em.setEnabled(false);
        r_fa.setEnabled(false);
        r_cli.setEnabled(false);
        b_pro.setEnabled(false);
        b_emp.setEnabled(false);
        b_fac.setEnabled(false);
        b_cli.setEnabled(false);
        ven.setEnabled(false);
        venE.setEnabled(false);
        rep.setEnabled(false);
        dev.setEnabled(false);
        m_pro.setEnabled(false);
        m_emp.setEnabled(false);
        m_cli.setEnabled(false);
        m_prov.setEnabled(false);
        b_ventas.setEnabled(false);
        r_pago.setEnabled(false);
        Activacion b = new Activacion();
        if (b.ver == 1) {
            b.entrante(r_pr, r_pro, r_em, r_fa, r_cli, b_pro, b_emp, b_fac, b_cli, modificacion, devolucion, ven, ea, dev, m_pro,
                    m_emp, m_cli, m_prov, rep, venE, b_ventas, r_pago);
        }
        if (b.ver == 2) {
            b.en(ven, venE, r_fa, b_pro, r_pr, r_pro, r_cli, r_pago, dev, b_cli, b_fac);
        }
        consulta_prod_registro_fac();

    }

    public void borrar(DefaultTableModel tabla) {
        while (tabla.getRowCount() > 0) {
            tabla.removeRow(0);
        }
    }

    public void consulta_prod_registro_fac() {

        String[] datos = new String[9];
        Conexion_mysql con;
        con = new Conexion_mysql(loading);
        java.sql.Connection cn = con.getConnection();
        String readMetaData = "CALL PRC_Cons_Prod_Reg_Fac() ";
        try {
            cn.setAutoCommit(false); // This line must be written just after the 

            CallableStatement cs = cn.prepareCall(readMetaData);
            //  cs.registerOutParameter(4, Types.REF_CURSOR);
//cs.execute();
//ResultSet rs = (ResultSet) cs.getObject(4);
            ResultSet rs = (ResultSet) cs.executeQuery();

            if (rs != null) {
                while (rs.next()) {
                    datos[0] = rs.getString(1).toUpperCase();
                    datos[1] = rs.getString(2).toUpperCase();
                    System.out.println("Nombre Producto" + rs.getString(2).toUpperCase() + "\n");

                    datos[2] = rs.getString(3).toUpperCase();
                    datos[3] = rs.getString(4).toUpperCase();
                    datos[4] = rs.getString(5).toUpperCase();
                    datos[5] = rs.getString(6).toUpperCase();
                    datos[6] = rs.getString(7).toUpperCase();
                    datos[7] = rs.getString(8).toUpperCase();
//PROCESO PARA CONSULTAR IMAGEN
//TERMINACION DE PROCESO PARA CONSULTAR IMAGEN
                    modelo.addRow(datos);

                }//end while
            }//end if
        } catch (SQLException ex) {
            Logger.getLogger(R_FACTURA.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void Consulta_Imagen() {
        //Variables para la imagen
        InputStream is;
        ImageIcon foto;
        Conexion_mysql con;
        con = new Conexion_mysql(loading);
        java.sql.Connection cn = con.getConnection();
        String readMetaData = "CALL PRC_Con_Imag_Re_Fac(?) ";
        try {
            cn.setAutoCommit(false); // This line must be written just after the 

            CallableStatement cs = cn.prepareCall(readMetaData);
            cs.setInt(1, codigoRecivido);

            // cs.registerOutParameter(2, Types.REF_CURSOR);
//cs.execute();
//ResultSet rs = (ResultSet) cs.getObject(2);
            ResultSet rs = cs.executeQuery();

            if (rs != null) {
                while (rs.next()) {

//PROCESO PARA CONSULTAR IMAGEN
                    is = rs.getBinaryStream(1);
                    try {
                        BufferedImage bi = ImageIO.read(is);
                        foto = new ImageIcon(bi);
                        Image img = foto.getImage();

                        Image newimg = img.getScaledInstance(200, 200,
                                java.awt.Image.SCALE_SMOOTH);
                        ImageIcon newicon = new ImageIcon(newimg);
                        rec_img.setIcon(newicon);//enviarlo a un jlabel    
                    } catch (IOException ex) {
                        Logger.getLogger(R_FACTURA.class.getName()).log(Level.SEVERE, null, ex);
                    }

//TERMINACION DE PROCESO PARA CONSULTAR IMAGEN
                }//end while
            }//end if
        } catch (SQLException ex) {
            Logger.getLogger(R_FACTURA.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
    //filtro por nombre de producto

    void filtro_nombre_Producto() {
        int ColumntaTabla = 1;
        filtro.setRowFilter(RowFilter.regexFilter(busqueda.getText(), ColumntaTabla));
    }
    //filtro por nombre de producto

    void filtro_Descripción_Producto() {
        int ColumntaTabla = 2;
        filtro.setRowFilter(RowFilter.regexFilter(busqueda1.getText(), ColumntaTabla));
    }
    //filtro por nombre de producto

    void filtro_Marca_Producto() {
        int ColumntaTabla = 3;
        filtro.setRowFilter(RowFilter.regexFilter(busqueda2.getText(), ColumntaTabla));
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        rSLabelHora1 = new rojeru_san.componentes.RSLabelHora();
        rSLabelFecha1 = new rojeru_san.componentes.RSLabelFecha();
        loading = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        busqueda = new javax.swing.JTextField();
        busqueda1 = new javax.swing.JTextField();
        busqueda2 = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        rec_img = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tabladatos = new javax.swing.JTable();
        jButton6 = new javax.swing.JButton();
        jLabel5 = new javax.swing.JLabel();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jMenu3 = new javax.swing.JMenu();
        jMenu2 = new javax.swing.JMenu();
        jMenu4 = new javax.swing.JMenu();
        r_pr = new javax.swing.JMenuItem();
        r_em = new javax.swing.JMenuItem();
        r_fa = new javax.swing.JMenuItem();
        r_pro = new javax.swing.JMenuItem();
        r_cli = new javax.swing.JMenuItem();
        jMenu6 = new javax.swing.JMenu();
        b_pro = new javax.swing.JMenuItem();
        b_emp = new javax.swing.JMenuItem();
        b_fac = new javax.swing.JMenuItem();
        b_cli = new javax.swing.JMenuItem();
        b_ventas = new javax.swing.JMenuItem();
        modificacion = new javax.swing.JMenu();
        m_pro = new javax.swing.JMenuItem();
        m_emp = new javax.swing.JMenuItem();
        m_cli = new javax.swing.JMenuItem();
        m_prov = new javax.swing.JMenuItem();
        devolucion = new javax.swing.JMenu();
        dev = new javax.swing.JMenuItem();
        jMenu7 = new javax.swing.JMenu();
        ven = new javax.swing.JMenuItem();
        venE = new javax.swing.JMenuItem();
        ea = new javax.swing.JMenu();
        rep = new javax.swing.JMenuItem();
        jMenu5 = new javax.swing.JMenu();
        r_pago = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("BUSCAR PRODUCTOS");
        setUndecorated(true);

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setMinimumSize(new java.awt.Dimension(500, 355));
        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        rSLabelHora1.setFont(new java.awt.Font("Times New Roman", 1, 24)); // NOI18N
        jPanel1.add(rSLabelHora1, new org.netbeans.lib.awtextra.AbsoluteConstraints(960, 10, 200, 30));

        rSLabelFecha1.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        jPanel1.add(rSLabelFecha1, new org.netbeans.lib.awtextra.AbsoluteConstraints(950, 40, -1, -1));

        loading.setIcon(new javax.swing.ImageIcon(getClass().getResource("/main/loading_1.gif"))); // NOI18N
        jPanel1.add(loading, new org.netbeans.lib.awtextra.AbsoluteConstraints(450, -30, 250, 170));
        jPanel1.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(32, 52, -1, 30));

        busqueda.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        busqueda.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                busquedaActionPerformed(evt);
            }
        });
        busqueda.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                busquedaKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                busquedaKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                busquedaKeyTyped(evt);
            }
        });
        jPanel1.add(busqueda, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 150, 290, 40));

        busqueda1.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        busqueda1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                busqueda1KeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                busqueda1KeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                busqueda1KeyTyped(evt);
            }
        });
        jPanel1.add(busqueda1, new org.netbeans.lib.awtextra.AbsoluteConstraints(330, 150, 290, 40));

        busqueda2.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        busqueda2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                busqueda2KeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                busqueda2KeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                busqueda2KeyTyped(evt);
            }
        });
        jPanel1.add(busqueda2, new org.netbeans.lib.awtextra.AbsoluteConstraints(630, 150, 290, 40));

        jLabel10.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/NUEVA LUPA.png"))); // NOI18N
        jPanel1.add(jLabel10, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 150, 50, 40));

        rec_img.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanel1.add(rec_img, new org.netbeans.lib.awtextra.AbsoluteConstraints(1040, 200, 200, 200));

        tabladatos.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        tabladatos.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tabladatos.setGridColor(new java.awt.Color(255, 255, 255));
        tabladatos.setSelectionBackground(new java.awt.Color(51, 204, 255));
        tabladatos.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tabladatosMouseClicked(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                tabladatosMousePressed(evt);
            }
        });
        jScrollPane2.setViewportView(tabladatos);

        jPanel1.add(jScrollPane2, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 200, 1030, 200));

        jButton6.setFont(new java.awt.Font("Tahoma", 3, 24)); // NOI18N
        jButton6.setForeground(new java.awt.Color(255, 255, 255));
        jButton6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/salir.png"))); // NOI18N
        jButton6.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jButton6.setIconTextGap(1);
        jButton6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton6ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton6, new org.netbeans.lib.awtextra.AbsoluteConstraints(1180, 0, -1, 40));

        jLabel5.setBackground(new java.awt.Color(255, 255, 255));
        jLabel5.setFont(new java.awt.Font("Tahoma", 3, 14)); // NOI18N
        jLabel5.setText("SALIR");
        jPanel1.add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(1180, 50, -1, -1));

        jMenuBar1.setBorder(null);
        jMenuBar1.setForeground(new java.awt.Color(255, 255, 255));
        jMenuBar1.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jMenuBar1.setFont(new java.awt.Font("Segoe UI", 2, 12)); // NOI18N
        jMenuBar1.setOpaque(true);

        jMenu1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/can.png"))); // NOI18N
        jMenu1.setText("LOGIN");

        jMenu3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Fondo.png"))); // NOI18N
        jMenu3.setText("LOGIN");
        jMenu3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jMenu3MouseClicked(evt);
            }
        });
        jMenu1.add(jMenu3);

        jMenuBar1.add(jMenu1);
        jMenuBar1.add(jMenu2);

        jMenu4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/add (1).png"))); // NOI18N
        jMenu4.setText(" REGISTRO");

        r_pr.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_R, java.awt.event.InputEvent.CTRL_DOWN_MASK));
        r_pr.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/ingreso.png"))); // NOI18N
        r_pr.setText("PRODUCTO");
        r_pr.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                r_prActionPerformed(evt);
            }
        });
        jMenu4.add(r_pr);

        r_em.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_E, java.awt.event.InputEvent.CTRL_DOWN_MASK));
        r_em.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/emple.png"))); // NOI18N
        r_em.setText("EMPLEADOS");
        r_em.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                r_emActionPerformed(evt);
            }
        });
        jMenu4.add(r_em);

        r_fa.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F, java.awt.event.InputEvent.CTRL_DOWN_MASK));
        r_fa.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/REGISTRO FACTURA.png"))); // NOI18N
        r_fa.setText("FACTURA");
        r_fa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                r_faActionPerformed(evt);
            }
        });
        jMenu4.add(r_fa);

        r_pro.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_P, java.awt.event.InputEvent.CTRL_DOWN_MASK));
        r_pro.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/proveNue.png"))); // NOI18N
        r_pro.setText("PROVEEDOR");
        r_pro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                r_proActionPerformed(evt);
            }
        });
        jMenu4.add(r_pro);

        r_cli.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_C, java.awt.event.InputEvent.CTRL_DOWN_MASK));
        r_cli.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/cliente.png"))); // NOI18N
        r_cli.setText("CLIENTE");
        r_cli.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                r_cliActionPerformed(evt);
            }
        });
        jMenu4.add(r_cli);

        jMenuBar1.add(jMenu4);

        jMenu6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Busqueda (1).png"))); // NOI18N
        jMenu6.setText("BUSQUEDA");

        b_pro.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_P, java.awt.event.InputEvent.ALT_DOWN_MASK));
        b_pro.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/prodddd.png"))); // NOI18N
        b_pro.setText("PRODUCTOS");
        b_pro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                b_proActionPerformed(evt);
            }
        });
        jMenu6.add(b_pro);

        b_emp.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_E, java.awt.event.InputEvent.ALT_DOWN_MASK));
        b_emp.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/busPer .png"))); // NOI18N
        b_emp.setText("EMPLEADOS");
        b_emp.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                b_empActionPerformed(evt);
            }
        });
        jMenu6.add(b_emp);

        b_fac.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F, java.awt.event.InputEvent.ALT_DOWN_MASK));
        b_fac.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/busqueda_factura.png"))); // NOI18N
        b_fac.setText("FACTURA");
        b_fac.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                b_facActionPerformed(evt);
            }
        });
        jMenu6.add(b_fac);

        b_cli.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_C, java.awt.event.InputEvent.ALT_DOWN_MASK));
        b_cli.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/bus_em.png"))); // NOI18N
        b_cli.setText("CLIENTE");
        b_cli.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                b_cliActionPerformed(evt);
            }
        });
        jMenu6.add(b_cli);

        b_ventas.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_Z, java.awt.event.InputEvent.ALT_DOWN_MASK));
        b_ventas.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Busqueda1.png"))); // NOI18N
        b_ventas.setText("VENTAS");
        b_ventas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                b_ventasActionPerformed(evt);
            }
        });
        jMenu6.add(b_ventas);

        jMenuBar1.add(jMenu6);

        modificacion.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/modificacion.png"))); // NOI18N
        modificacion.setText("MODIFICAR");

        m_pro.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_K, java.awt.event.InputEvent.ALT_DOWN_MASK));
        m_pro.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/PRODUCTO.png"))); // NOI18N
        m_pro.setText("PRODUCTO");
        m_pro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                m_proActionPerformed(evt);
            }
        });
        modificacion.add(m_pro);

        m_emp.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_E, java.awt.event.InputEvent.SHIFT_DOWN_MASK));
        m_emp.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/empleado.png"))); // NOI18N
        m_emp.setText("EMPLEADO");
        m_emp.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                m_empActionPerformed(evt);
            }
        });
        modificacion.add(m_emp);

        m_cli.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_C, java.awt.event.InputEvent.SHIFT_DOWN_MASK));
        m_cli.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/clientee.png"))); // NOI18N
        m_cli.setText("CLIENTE");
        m_cli.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                m_cliActionPerformed(evt);
            }
        });
        modificacion.add(m_cli);

        m_prov.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_P, java.awt.event.InputEvent.SHIFT_DOWN_MASK));
        m_prov.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/proveedores.png"))); // NOI18N
        m_prov.setText("PROVEEDORES");
        m_prov.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                m_provActionPerformed(evt);
            }
        });
        modificacion.add(m_prov);

        jMenuBar1.add(modificacion);

        devolucion.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/DEV.png"))); // NOI18N
        devolucion.setText("DEVOULUCION");

        dev.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_D, java.awt.event.InputEvent.CTRL_DOWN_MASK));
        dev.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/dev1.png"))); // NOI18N
        dev.setText("DEVOLUCION");
        dev.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                devActionPerformed(evt);
            }
        });
        devolucion.add(dev);

        jMenuBar1.add(devolucion);

        jMenu7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/png-transparent-shopping-cart-graphy-cart-supermarket-vehicle-shopping-bags-trolleys (1) (1).png"))); // NOI18N
        jMenu7.setText("VENTA");

        ven.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_V, java.awt.event.InputEvent.CTRL_DOWN_MASK));
        ven.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/venta a credito.png"))); // NOI18N
        ven.setText("VENTA  A CREDITO");
        ven.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                venActionPerformed(evt);
            }
        });
        jMenu7.add(ven);

        venE.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_B, java.awt.event.InputEvent.CTRL_DOWN_MASK));
        venE.setIcon(new javax.swing.ImageIcon(getClass().getResource("/main/venta efectivo.png"))); // NOI18N
        venE.setText("VENTA EN EFECTIVO");
        venE.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                venEActionPerformed(evt);
            }
        });
        jMenu7.add(venE);

        jMenuBar1.add(jMenu7);

        ea.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/REPORTE.png"))); // NOI18N
        ea.setText("REPORTE");

        rep.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_R, java.awt.event.InputEvent.ALT_DOWN_MASK));
        rep.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/reportess.png"))); // NOI18N
        rep.setText(" REPORTES");
        rep.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                repActionPerformed(evt);
            }
        });
        ea.add(rep);

        jMenuBar1.add(ea);

        jMenu5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Pago_C_P.png"))); // NOI18N
        jMenu5.setText("PAGOS");

        r_pago.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Pago_C_P (2).png"))); // NOI18N
        r_pago.setText("REALIZAR PAGOS");
        r_pago.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                r_pagoActionPerformed(evt);
            }
        });
        jMenu5.add(r_pago);

        jMenuBar1.add(jMenu5);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 1245, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 448, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void busquedaKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_busquedaKeyPressed

    }//GEN-LAST:event_busquedaKeyPressed

    private void busquedaKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_busquedaKeyReleased

    }//GEN-LAST:event_busquedaKeyReleased

    private void busqueda1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_busqueda1KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_busqueda1KeyPressed

    private void busqueda1KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_busqueda1KeyReleased

    }//GEN-LAST:event_busqueda1KeyReleased

    private void busqueda2KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_busqueda2KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_busqueda2KeyPressed

    private void busqueda2KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_busqueda2KeyReleased

    }//GEN-LAST:event_busqueda2KeyReleased

    private void tabladatosMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tabladatosMouseClicked

    }//GEN-LAST:event_tabladatosMouseClicked

    private void tabladatosMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tabladatosMousePressed
        Thread thread = new Thread() {
            public void run() {

                int seleccionn = tabladatos.rowAtPoint(evt.getPoint());
                codigoRecivido = Integer.parseInt(String.valueOf(tabladatos.getValueAt(seleccionn, 0)));
                Consulta_Imagen();

            }
        };

        thread.start();

    }//GEN-LAST:event_tabladatosMousePressed

    private void jMenu3MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jMenu3MouseClicked
        Login l = new Login();
        l.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_jMenu3MouseClicked

    private void r_prActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_r_prActionPerformed
        R_PROD a = new R_PROD();
        a.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_r_prActionPerformed

    private void r_emActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_r_emActionPerformed
        R_EMPL a = new R_EMPL();
        a.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_r_emActionPerformed

    private void r_faActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_r_faActionPerformed
        R_FACTURA a = new R_FACTURA();
        a.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_r_faActionPerformed

    private void r_proActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_r_proActionPerformed
        R_PROVEE a = new R_PROVEE();
        a.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_r_proActionPerformed

    private void r_cliActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_r_cliActionPerformed

        R_CLI A = new R_CLI();
        A.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_r_cliActionPerformed

    private void b_proActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_b_proActionPerformed
        Busqueda_Productos a = new Busqueda_Productos();
        a.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_b_proActionPerformed

    private void b_empActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_b_empActionPerformed
        Busqueda_Empleados a = new Busqueda_Empleados();
        a.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_b_empActionPerformed

    private void b_facActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_b_facActionPerformed
        Busqueda_Facturas a = new Busqueda_Facturas();
        a.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_b_facActionPerformed

    private void b_cliActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_b_cliActionPerformed
        Busqueda_Clientes a = new Busqueda_Clientes();
        a.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_b_cliActionPerformed

    private void b_ventasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_b_ventasActionPerformed
        Busqueda_Venta P = new Busqueda_Venta();
        this.setVisible(false);
        P.setVisible(true);
    }//GEN-LAST:event_b_ventasActionPerformed

    private void m_proActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_m_proActionPerformed
        m_prod a = new m_prod();
        a.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_m_proActionPerformed

    private void m_empActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_m_empActionPerformed
        m_empl a = new m_empl();
        a.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_m_empActionPerformed

    private void m_cliActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_m_cliActionPerformed
        m_cliente a = new m_cliente();
        a.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_m_cliActionPerformed

    private void m_provActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_m_provActionPerformed
        m_prov a = new m_prov();
        a.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_m_provActionPerformed

    private void devActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_devActionPerformed
        Devolucion l = new Devolucion();
        l.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_devActionPerformed

    private void venActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_venActionPerformed
        Ventas_Cred a = new Ventas_Cred();
        a.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_venActionPerformed

    private void venEActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_venEActionPerformed
        Ventas a = new Ventas();
        a.setVisible(true);
        this.setVisible(false);

    }//GEN-LAST:event_venEActionPerformed

    private void repActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_repActionPerformed
        Reportes r = new Reportes();
        r.setVisible(true);
    }//GEN-LAST:event_repActionPerformed

    private void r_pagoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_r_pagoActionPerformed
        PAGO P = new PAGO();
        this.setVisible(false);
        P.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_r_pagoActionPerformed

    private void jButton6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton6ActionPerformed
        System.exit(0);
    }//GEN-LAST:event_jButton6ActionPerformed

    private void busquedaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_busquedaActionPerformed
        busqueda.setText("");
        busqueda1.setText("");
        busqueda2.setText("");
    }//GEN-LAST:event_busquedaActionPerformed

    private void busquedaKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_busquedaKeyTyped
        //borrar(modelo1);
        busqueda.addKeyListener(new KeyAdapter() {
            public void keyReleased(final KeyEvent e) {
                String cadena = (busqueda.getText().toUpperCase());
                busqueda.setText(cadena);
                filtro_nombre_Producto();
            }

        });
        filtro = new TableRowSorter(tabladatos.getModel());
        tabladatos.setRowSorter(filtro);

    }//GEN-LAST:event_busquedaKeyTyped

    private void busqueda1KeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_busqueda1KeyTyped
        busqueda1.addKeyListener(new KeyAdapter() {
            public void keyReleased(final KeyEvent e) {
                String cadena = (busqueda1.getText().toUpperCase());
                busqueda1.setText(cadena);
                filtro_Descripción_Producto();
            }

        });
        filtro = new TableRowSorter(tabladatos.getModel());
        tabladatos.setRowSorter(filtro);
    }//GEN-LAST:event_busqueda1KeyTyped

    private void busqueda2KeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_busqueda2KeyTyped
        busqueda2.addKeyListener(new KeyAdapter() {
            public void keyReleased(final KeyEvent e) {
                String cadena = (busqueda2.getText().toUpperCase());
                busqueda2.setText(cadena);
                filtro_Marca_Producto();
            }

        });
        filtro = new TableRowSorter(tabladatos.getModel());
        tabladatos.setRowSorter(filtro);
    }//GEN-LAST:event_busqueda2KeyTyped

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Busqueda_Productos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Busqueda_Productos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Busqueda_Productos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Busqueda_Productos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Busqueda_Productos().setVisible(true);

            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenuItem b_cli;
    private javax.swing.JMenuItem b_emp;
    private javax.swing.JMenuItem b_fac;
    private javax.swing.JMenuItem b_pro;
    private javax.swing.JMenuItem b_ventas;
    private javax.swing.JTextField busqueda;
    private javax.swing.JTextField busqueda1;
    private javax.swing.JTextField busqueda2;
    private javax.swing.JMenuItem dev;
    private javax.swing.JMenu devolucion;
    private javax.swing.JMenu ea;
    private javax.swing.JButton jButton6;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenu jMenu3;
    private javax.swing.JMenu jMenu4;
    private javax.swing.JMenu jMenu5;
    private javax.swing.JMenu jMenu6;
    private javax.swing.JMenu jMenu7;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JLabel loading;
    private javax.swing.JMenuItem m_cli;
    private javax.swing.JMenuItem m_emp;
    private javax.swing.JMenuItem m_pro;
    private javax.swing.JMenuItem m_prov;
    private javax.swing.JMenu modificacion;
    private rojeru_san.componentes.RSLabelFecha rSLabelFecha1;
    private rojeru_san.componentes.RSLabelHora rSLabelHora1;
    private javax.swing.JMenuItem r_cli;
    private javax.swing.JMenuItem r_em;
    private javax.swing.JMenuItem r_fa;
    private javax.swing.JMenuItem r_pago;
    private javax.swing.JMenuItem r_pr;
    private javax.swing.JMenuItem r_pro;
    private javax.swing.JLabel rec_img;
    private javax.swing.JMenuItem rep;
    private javax.swing.JTable tabladatos;
    private javax.swing.JMenuItem ven;
    private javax.swing.JMenuItem venE;
    // End of variables declaration//GEN-END:variables
}
