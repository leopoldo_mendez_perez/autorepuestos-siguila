/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package clases;

/**
 *
 * @author QA
 */
public class NewMain {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
    }

    private static void anulacionDocumento(gt.com.tekra.desa.apicertificacion._8080.certificacion.wsdl.AutenticacionAnulacion autenticacion, java.lang.String documento, javax.xml.ws.Holder<java.lang.String> resultadoAnulacion, javax.xml.ws.Holder<java.lang.String> anulacionCertificada, javax.xml.ws.Holder<java.lang.String> representacionGrafica, javax.xml.ws.Holder<java.lang.String> codigoQR, javax.xml.ws.Holder<java.lang.String> nitCertificador, javax.xml.ws.Holder<java.lang.String> nombreCertificador, javax.xml.ws.Holder<java.lang.String> numeroAutorizacion, javax.xml.ws.Holder<java.math.BigInteger> numeroDocumento, javax.xml.ws.Holder<java.lang.String> serieDocumento, javax.xml.ws.Holder<java.lang.String> fechaHoraCertificacion) {
        gt.com.tekra.desa.apicertificacion._8080.certificacion.wsdl.KTFCertificador_Service service = new gt.com.tekra.desa.apicertificacion._8080.certificacion.wsdl.KTFCertificador_Service();
        gt.com.tekra.desa.apicertificacion._8080.certificacion.wsdl.KTFCertificador port = service.getKTFCertificadorSOAP();
        port.anulacionDocumento(autenticacion, documento, resultadoAnulacion, anulacionCertificada, representacionGrafica, codigoQR, nitCertificador, nombreCertificador, numeroAutorizacion, numeroDocumento, serieDocumento, fechaHoraCertificacion);
    }

    private static void certificacionDocumento(gt.com.tekra.desa.apicertificacion._8080.certificacion.wsdl.AutenticacionCertificacion autenticacion, java.lang.String documento, javax.xml.ws.Holder<java.lang.String> resultadoCertificacion, javax.xml.ws.Holder<java.lang.String> documentoCertificado, javax.xml.ws.Holder<java.lang.String> representacionGrafica, javax.xml.ws.Holder<java.lang.String> codigoQR, javax.xml.ws.Holder<java.lang.String> nitCertificador, javax.xml.ws.Holder<java.lang.String> nombreCertificador, javax.xml.ws.Holder<java.lang.String> numeroAutorizacion, javax.xml.ws.Holder<java.math.BigInteger> numeroDocumento, javax.xml.ws.Holder<java.lang.String> serieDocumento, javax.xml.ws.Holder<java.lang.String> fechaHoraCertificacion, javax.xml.ws.Holder<java.lang.String> nombreReceptor) {
        gt.com.tekra.desa.apicertificacion._8080.certificacion.wsdl.KTFCertificador_Service service = new gt.com.tekra.desa.apicertificacion._8080.certificacion.wsdl.KTFCertificador_Service();
        gt.com.tekra.desa.apicertificacion._8080.certificacion.wsdl.KTFCertificador port = service.getKTFCertificadorSOAP();
        port.certificacionDocumento(autenticacion, documento, resultadoCertificacion, documentoCertificado, representacionGrafica, codigoQR, nitCertificador, nombreCertificador, numeroAutorizacion, numeroDocumento, serieDocumento, fechaHoraCertificacion, nombreReceptor);
    }
    
}
