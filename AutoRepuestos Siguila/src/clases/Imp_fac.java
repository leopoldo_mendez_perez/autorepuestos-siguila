
package clases;


public class Imp_fac {
    private static int id_venta;
    private static String nombre;
    private static String empleado;
    private static String Total;
    private static String Descuento;
     private static String Pendiente;
      private static String Abono;
    private static String tot_con_desc;

    public static String getTot_con_desc() {
        return tot_con_desc;
    }

    public static void setTot_con_desc(String tot_con_desc) {
        Imp_fac.tot_con_desc = tot_con_desc;
    }
    public static String getPendiente() {
        return Pendiente;
    }

    public static void setPendiente(String Pendiente) {
        Imp_fac.Pendiente = Pendiente;
    }

    public static String getAbono() {
        return Abono;
    }

    public static void setAbono(String Abono) {
        Imp_fac.Abono = Abono;
    }

    public static String getDescuento() {
        return Descuento;
    }

    public static void setDescuento(String Descuento) {
        Imp_fac.Descuento = Descuento;
    }
    public static int getId_venta() {
        return id_venta;
    }

    public static void setId_venta(int id_venta) {
        Imp_fac.id_venta = id_venta;
    }

    public static String getNombre() {
        return nombre;
    }

    public static void setNombre(String nombre) {
        Imp_fac.nombre = nombre;
    }

    public static String getEmpleado() {
        return empleado;
    }

    public static void setEmpleado(String empleado) {
        Imp_fac.empleado = empleado;
    }

    public static String getTotal() {
        return Total;
    }

    public static void setTotal(String Total) {
        Imp_fac.Total = Total;
    }

}
