
package gt.com.tekra.desa.apicertificacion._8080.certificacion.wsdl;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the gt.com.tekra.desa.apicertificacion._8080.certificacion.wsdl package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: gt.com.tekra.desa.apicertificacion._8080.certificacion.wsdl
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link CertificacionDocumento }
     * 
     */
    public CertificacionDocumento createCertificacionDocumento() {
        return new CertificacionDocumento();
    }

    /**
     * Create an instance of {@link AutenticacionCertificacion }
     * 
     */
    public AutenticacionCertificacion createAutenticacionCertificacion() {
        return new AutenticacionCertificacion();
    }

    /**
     * Create an instance of {@link CertificacionDocumentoResponse }
     * 
     */
    public CertificacionDocumentoResponse createCertificacionDocumentoResponse() {
        return new CertificacionDocumentoResponse();
    }

    /**
     * Create an instance of {@link AnulacionDocumento }
     * 
     */
    public AnulacionDocumento createAnulacionDocumento() {
        return new AnulacionDocumento();
    }

    /**
     * Create an instance of {@link AutenticacionAnulacion }
     * 
     */
    public AutenticacionAnulacion createAutenticacionAnulacion() {
        return new AutenticacionAnulacion();
    }

    /**
     * Create an instance of {@link AnulacionDocumentoResponse }
     * 
     */
    public AnulacionDocumentoResponse createAnulacionDocumentoResponse() {
        return new AnulacionDocumentoResponse();
    }

}
