
package gt.com.tekra.desa.apicertificacion._8080.certificacion.wsdl;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ResultadoAnulacion" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="AnulacionCertificada" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="RepresentacionGrafica" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="CodigoQR" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="NITCertificador" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="NombreCertificador" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="NumeroAutorizacion" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="NumeroDocumento" type="{http://www.w3.org/2001/XMLSchema}integer"/&gt;
 *         &lt;element name="SerieDocumento" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="FechaHoraCertificacion" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "resultadoAnulacion",
    "anulacionCertificada",
    "representacionGrafica",
    "codigoQR",
    "nitCertificador",
    "nombreCertificador",
    "numeroAutorizacion",
    "numeroDocumento",
    "serieDocumento",
    "fechaHoraCertificacion"
})
@XmlRootElement(name = "AnulacionDocumentoResponse")
public class AnulacionDocumentoResponse {

    @XmlElement(name = "ResultadoAnulacion", required = true)
    protected String resultadoAnulacion;
    @XmlElement(name = "AnulacionCertificada", required = true)
    protected String anulacionCertificada;
    @XmlElement(name = "RepresentacionGrafica", required = true)
    protected String representacionGrafica;
    @XmlElement(name = "CodigoQR", required = true)
    protected String codigoQR;
    @XmlElement(name = "NITCertificador", required = true)
    protected String nitCertificador;
    @XmlElement(name = "NombreCertificador", required = true)
    protected String nombreCertificador;
    @XmlElement(name = "NumeroAutorizacion", required = true)
    protected String numeroAutorizacion;
    @XmlElement(name = "NumeroDocumento", required = true)
    protected BigInteger numeroDocumento;
    @XmlElement(name = "SerieDocumento", required = true)
    protected String serieDocumento;
    @XmlElement(name = "FechaHoraCertificacion", required = true)
    protected String fechaHoraCertificacion;

    /**
     * Obtiene el valor de la propiedad resultadoAnulacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResultadoAnulacion() {
        return resultadoAnulacion;
    }

    /**
     * Define el valor de la propiedad resultadoAnulacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResultadoAnulacion(String value) {
        this.resultadoAnulacion = value;
    }

    /**
     * Obtiene el valor de la propiedad anulacionCertificada.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAnulacionCertificada() {
        return anulacionCertificada;
    }

    /**
     * Define el valor de la propiedad anulacionCertificada.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAnulacionCertificada(String value) {
        this.anulacionCertificada = value;
    }

    /**
     * Obtiene el valor de la propiedad representacionGrafica.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRepresentacionGrafica() {
        return representacionGrafica;
    }

    /**
     * Define el valor de la propiedad representacionGrafica.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRepresentacionGrafica(String value) {
        this.representacionGrafica = value;
    }

    /**
     * Obtiene el valor de la propiedad codigoQR.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoQR() {
        return codigoQR;
    }

    /**
     * Define el valor de la propiedad codigoQR.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoQR(String value) {
        this.codigoQR = value;
    }

    /**
     * Obtiene el valor de la propiedad nitCertificador.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNITCertificador() {
        return nitCertificador;
    }

    /**
     * Define el valor de la propiedad nitCertificador.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNITCertificador(String value) {
        this.nitCertificador = value;
    }

    /**
     * Obtiene el valor de la propiedad nombreCertificador.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNombreCertificador() {
        return nombreCertificador;
    }

    /**
     * Define el valor de la propiedad nombreCertificador.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNombreCertificador(String value) {
        this.nombreCertificador = value;
    }

    /**
     * Obtiene el valor de la propiedad numeroAutorizacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumeroAutorizacion() {
        return numeroAutorizacion;
    }

    /**
     * Define el valor de la propiedad numeroAutorizacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumeroAutorizacion(String value) {
        this.numeroAutorizacion = value;
    }

    /**
     * Obtiene el valor de la propiedad numeroDocumento.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getNumeroDocumento() {
        return numeroDocumento;
    }

    /**
     * Define el valor de la propiedad numeroDocumento.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setNumeroDocumento(BigInteger value) {
        this.numeroDocumento = value;
    }

    /**
     * Obtiene el valor de la propiedad serieDocumento.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSerieDocumento() {
        return serieDocumento;
    }

    /**
     * Define el valor de la propiedad serieDocumento.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSerieDocumento(String value) {
        this.serieDocumento = value;
    }

    /**
     * Obtiene el valor de la propiedad fechaHoraCertificacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechaHoraCertificacion() {
        return fechaHoraCertificacion;
    }

    /**
     * Define el valor de la propiedad fechaHoraCertificacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechaHoraCertificacion(String value) {
        this.fechaHoraCertificacion = value;
    }

}
