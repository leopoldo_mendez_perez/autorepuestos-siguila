
package gt.com.tekra.desa.apicertificacion._8080.certificacion.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para AutenticacionAnulacion complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="AutenticacionAnulacion"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="pn_usuario"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;minLength value="1"/&gt;
 *               &lt;maxLength value="100"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="pn_clave"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;minLength value="1"/&gt;
 *               &lt;maxLength value="500"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="pn_cliente" type="{http://www.w3.org/2001/XMLSchema}long"/&gt;
 *         &lt;element name="pn_contrato" type="{http://www.w3.org/2001/XMLSchema}long"/&gt;
 *         &lt;element name="pn_firmar_emisor"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;minLength value="1"/&gt;
 *               &lt;maxLength value="2"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="pn_retornar_pdf" minOccurs="0"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;minLength value="1"/&gt;
 *               &lt;maxLength value="2"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AutenticacionAnulacion", propOrder = {
    "pnUsuario",
    "pnClave",
    "pnCliente",
    "pnContrato",
    "pnFirmarEmisor",
    "pnRetornarPdf"
})
public class AutenticacionAnulacion {

    @XmlElement(name = "pn_usuario", required = true)
    protected String pnUsuario;
    @XmlElement(name = "pn_clave", required = true)
    protected String pnClave;
    @XmlElement(name = "pn_cliente")
    protected long pnCliente;
    @XmlElement(name = "pn_contrato")
    protected long pnContrato;
    @XmlElement(name = "pn_firmar_emisor", required = true)
    protected String pnFirmarEmisor;
    @XmlElement(name = "pn_retornar_pdf")
    protected String pnRetornarPdf;

    /**
     * Obtiene el valor de la propiedad pnUsuario.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPnUsuario() {
        return pnUsuario;
    }

    /**
     * Define el valor de la propiedad pnUsuario.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPnUsuario(String value) {
        this.pnUsuario = value;
    }

    /**
     * Obtiene el valor de la propiedad pnClave.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPnClave() {
        return pnClave;
    }

    /**
     * Define el valor de la propiedad pnClave.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPnClave(String value) {
        this.pnClave = value;
    }

    /**
     * Obtiene el valor de la propiedad pnCliente.
     * 
     */
    public long getPnCliente() {
        return pnCliente;
    }

    /**
     * Define el valor de la propiedad pnCliente.
     * 
     */
    public void setPnCliente(long value) {
        this.pnCliente = value;
    }

    /**
     * Obtiene el valor de la propiedad pnContrato.
     * 
     */
    public long getPnContrato() {
        return pnContrato;
    }

    /**
     * Define el valor de la propiedad pnContrato.
     * 
     */
    public void setPnContrato(long value) {
        this.pnContrato = value;
    }

    /**
     * Obtiene el valor de la propiedad pnFirmarEmisor.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPnFirmarEmisor() {
        return pnFirmarEmisor;
    }

    /**
     * Define el valor de la propiedad pnFirmarEmisor.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPnFirmarEmisor(String value) {
        this.pnFirmarEmisor = value;
    }

    /**
     * Obtiene el valor de la propiedad pnRetornarPdf.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPnRetornarPdf() {
        return pnRetornarPdf;
    }

    /**
     * Define el valor de la propiedad pnRetornarPdf.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPnRetornarPdf(String value) {
        this.pnRetornarPdf = value;
    }

}
